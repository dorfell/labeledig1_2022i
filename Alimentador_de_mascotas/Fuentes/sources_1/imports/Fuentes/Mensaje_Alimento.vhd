library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--Genera un mensaje para enviarlo al usuario, pensado para la fifo

entity Mensaje_Alimento is

port(
    clk, rst:  in std_logic;
    start:  in std_logic;  --Empieza la carga del mensaje
    data_user: out std_logic_vector(7 downto 0); --Se utiliza para el mensaje a tx
    wr_uart : out std_logic --Libera el mensaje al terminar de cargar la fifo
--    rst_fifo : out std_logic   
);
    end Mensaje_alimento;

architecture Behavioral of Mensaje_Alimento is

type states is (inicio,s0,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,s14,s15,parada);
signal present_state, next_state: states;

begin

--===================
-- Present State
--===================
process(clk, rst)
    begin
    if rst = '1' then
        present_state <= inicio;
    elsif (clk'event and clk='1') then
        present_state <= next_state;
    else
        present_state <= present_state;
    end if;
end process;


--===================
-- Next state logic
--===================

--Escribe las letras de Comida Servida cada ciclo de reloj y se lo pasa a la fifo

process(present_state, start, clk)
    begin
    case present_state is
 
    when inicio =>
        if (start = '1') then
--           rst_fifo <= '1';
           next_state <= s0;
        else
           next_state <= inicio;
--           rst_fifo <= '0';  
        end if;

    when s0 =>
--        rst_fifo <= '0';    
        next_state <= s1;

    when s1 =>
        next_state <= s2;

    when s2 =>
        next_state <= s3;

    when s3 =>
        next_state <= s4;

    when s4 =>
        next_state <= s5;

    when s5 =>
        next_state <= s6;

    when s6 =>
        next_state <= s7;

    when s7 =>
        next_state <= s8;
       
    when s8 =>
        next_state <= s9;
       
    when s9 =>
        next_state <= s10;
       
    when s10 =>
        next_state <= s11;
       
    when s11 =>
        next_state <= s12;
       
    when s12 =>
        next_state <= s13;
       
    when s13 =>
        next_state <= s14;                    

    when s14 =>
        next_state <= s15;  
  
    when s15 =>
        next_state <= parada;    

    when parada =>          
        next_state <= inicio;
       
                           
    end case;
end process;

--===================
-- Output logic
--===================
process(present_state)
  begin
  case present_state is
 
    when inicio =>
        wr_uart <= '0';  
        data_user <= "00000000";
 
    when s0 =>
        wr_uart <= '1';      
        data_user <= "01000011"; --C

    when s1 =>
        wr_uart <= '1';      
        data_user <= "01001111"; --O

    when s2 =>
       wr_uart <= '1';      
       data_user <= "01001101"; --M

    when s3 =>
       wr_uart <= '1';      
       data_user <= "01001001"; --I

    when s4 =>
       wr_uart <= '1';      
       data_user <= "01000100"; --D

    when s5 =>
       wr_uart <= '1';      
       data_user <= "01000001"; --A
       
    when s6 =>
       wr_uart <= '1';    
       data_user <= "00100000"; --      

    when s7 =>
       wr_uart <= '1';      
       data_user <= "01010011"; --S
       
    when s8 =>
       wr_uart <= '1';      
       data_user <= "01000101"; --E
       
    when s9 =>
       wr_uart <= '1';      
       data_user <= "01010010"; --R
       
    when s10 =>
       wr_uart <= '1';      
       data_user <= "01010110"; --V
       
    when s11 =>
       wr_uart <= '1';      
       data_user <= "01001001"; --I
       
    when s12 =>
       wr_uart <= '1';      
       data_user <= "01000100"; --D
       
    when s13 =>
       wr_uart <= '1';  
       data_user <= "01000001"; --A

    when s14 =>
       wr_uart <= '1';  
       data_user <= "00100000"; -- 
       
    when s15 =>
       wr_uart <= '1';  
       data_user <= "00100000"; --        

    when parada =>
       wr_uart <= '0';  
       data_user <= "00000000";

  end case;
end process;
   

end Behavioral;