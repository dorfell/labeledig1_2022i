
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Completo is
Port ( clk : in std_logic;
       rst, start, stop_motor : in std_logic;
       state: out std_logic_vector(3 downto 0)
      );
end Completo;

architecture Behavioral of Completo is
signal en: std_logic;

component Divisor is
port ( clk,rst :  in std_logic;
         pulse : out std_logic);
end component;

component FSM is
port( clk, rst, en:  in std_logic;
        start, stop_motor        :  in std_logic;
        state         : out std_logic_vector(3 downto 0));
end component;



begin
FSM1 : FSM port map(clk => clk, rst => rst, start => start, state => state, en => en, stop_motor =>stop_motor);
Div: Divisor port map(clk => clk, rst => rst, pulse => en);


end Behavioral;
