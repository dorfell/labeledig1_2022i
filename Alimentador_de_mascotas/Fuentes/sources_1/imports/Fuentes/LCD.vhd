library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity LCD is
    
    Port (
    CLOCK : in STD_LOGIC; --reloj de 100MHz
    start : in std_logic;
    stop_lcd : in std_logic;
    rs : in STD_LOGIC; --boton de reinicio a BTNC
    LCD_RS : out STD_LOGIC; --del LCD (JD1)
    LCD_RW : out STD_LOGIC; --read/write del LCD (JD2)
    LCD_E : out STD_LOGIC; --enable del LCD (JD3)
    DATA : out STD_LOGIC_VECTOR (7 downto 0) --bus de datos de la LCD (JC1-4,7-10)
    );

end LCD;


architecture LCD of LCD is

-- FSM states
type STATE_TYPE is (idle, RST, ST0, ST1, FSET, EMSET, DO, CLD, RETH, SDDRAMA, WRITE1, WRITE2,
WRITE3, WRITE4, WRITE5, WRITE6, WRITE7, WRITE8, WRITE9, WRITE10,
WRITE11, WRITE12, WRITE13, WRITE14, stop, clear);

-- se�ales
signal State,Next_State : STATE_TYPE;
--signal CONT1 : STD_LOGIC_VECTOR(24 downto 0) := X"0000000"; -- 33.554.432 = 0.33554432 s MAX
signal CONT1 : STD_LOGIC_VECTOR(23 downto 0) := X"000000"; -- 16,777,216 = 0.33554432 s MAX
signal CONT2 : STD_LOGIC_VECTOR(5 downto 0) :="000000"; -- 64 = 0.64 us
signal RESET : STD_LOGIC :='0';
signal READY : STD_LOGIC :='0';

--constantes ASCII (la "M_" antes de la letra es para may�scula)
-- ASCII: hexadecimal(x"") decimal
constant M_C: STD_LOGIC_VECTOR(7 downto 0) := x"43"; -- C 67
constant M_O: STD_LOGIC_VECTOR(7 downto 0) := x"4F"; -- O 79
constant M_M: STD_LOGIC_VECTOR(7 downto 0) := x"4D"; -- M 77
constant M_I: STD_LOGIC_VECTOR(7 downto 0) := x"49"; -- I 73
constant M_D: STD_LOGIC_VECTOR(7 downto 0) := x"44"; -- D 68
constant M_A: STD_LOGIC_VECTOR(7 downto 0) := x"41"; -- A 65

constant space: STD_LOGIC_VECTOR(7 downto 0) := x"20"; -- [space] 32

constant M_S: STD_LOGIC_VECTOR(7 downto 0) := x"53"; -- S 83
constant M_E: STD_LOGIC_VECTOR(7 downto 0) := x"45"; -- E 69
constant M_R: STD_LOGIC_VECTOR(7 downto 0) := x"52"; -- R 82
constant M_V: STD_LOGIC_VECTOR(7 downto 0) := x"56"; -- V 86

--constantes de tiempos
constant T1: STD_LOGIC_VECTOR(23 downto 0) := x"001FFE"; -- espera de 81.9us = 8.l90 
-- @ write > 37us (3700 = E74)

-------------------

begin

-------------------------------------------------------------------
--Contador de Retardos CONT1--

process(CLOCK,RESET)

begin
    if RESET = '1' then
        CONT1 <= (others => '0');
    elsif (CLOCK'event and CLOCK='1') then
        CONT1 <= CONT1 + 1;
    end if;
end process;

-------------------------------------------------------------------
--Contador para Secuencias CONT2--

process(CLOCK,READY)

begin

if (CLOCK = '1' and CLOCK'event) then
    if READY = '1' then 
        CONT2 <= CONT2 + 1;
    else CONT2 <= "000000";
    end if;
end if;

end process;

-------------------------------------------------------------------
--Actualizaci�n de estados--

process (CLOCK, Next_State)

begin

    if (CLOCK = '1' and CLOCK'event) then 
        State <= Next_State;
    end if;

end process;

-------------------------------------------------------------------
--FSM--

process(CONT1,CONT2,State,CLOCK,rs,start)
    begin
    if rs = '1' THEN 
        Next_State <= RST;
    elsif (CLOCK = '0' and CLOCK'event) then
        case State is
        
            when idle =>
                RESET <= '1';
                LCD_RS <= '0';
                LCD_RW <= '0';
                LCD_E <= '1';
                DATA <= X"01";
                if start = '1' then
                    Next_state <= RST;
                else
                    Next_state <= idle;
                end if;    
        
            when RST => -- Estado de reset
            
                    LCD_RS <= '0';
                    LCD_RW <= '0';
                    LCD_E <= '0';
                    DATA <= X"00";
                    Next_State <= ST0;
                   
----------------- Estado 0 -----------------

            when ST0 => --Primer estado de espera por 25ms

                        --(20 ms = x 1E 8480 = 2.000.000)(15 ms = x 16 E360 = 1.500.000)

                if CONT1 = X"02625A0" then -- 2.500.000 = 25ms
                    READY <= '1';
                    DATA <= X"38"; -- FUNCTION SET 8BITS, 2 LINE, 5X7
                    Next_State <= ST0;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';

                elsif CONT2 = "0011011" then
                    
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= ST1;

                else

                    Next_State <= ST0;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

----------------- Estado 1 -----------------

            when ST1 => --Segundo estado de espera por 100us (10.000 = x 2710)
                
                if CONT1 = X"0006BD0" then -- 27.600 = 276us

                    READY <= '1';
                    DATA <= X"38"; -- FUNCTION SET
                    Next_State <= ST1;

               elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= FSET;

                else

                    Next_State <= ST1;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

----------------- Estado FSET -----------------

            when FSET => --FUNCTION SET 0x38 (8bits, 2 lineas, 5x7dots)

                if CONT1 = X"0009C40" then --espera por 40.000 = 40us

                    READY <= '1';
                    DATA <= X"38"; --001DL-N-F-XX
                    Next_State <= FSET;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= EMSET;

                else

                    Next_State <= FSET;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

----------------- Estado EMSET -----------------

            when EMSET => --ENTRY MODE SET 0x06 (1 right-moving cursor and address increment)

                if CONT1 = X"0009C40" then --estado de espera por 40us

                    READY <= '1';
                    DATA <= X"06"; --000001-I/D-SH
                    Next_State <= EMSET;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= DO;

                else

                    Next_State <= EMSET;

                end if;

            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

----------------- Estado D0 -----------------

            when DO => --DISPLAY ON/OFF 0x0C (DISPLAY-CURSOR-BLINKING on-off)

                if CONT1 = X"0009C40" then -- estado de espera por 40us

                    READY <= '1';
                    DATA <= X"0C"; --00001-D-C-B,display on, cursor on
                    Next_State <= DO;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= CLD;

                else

                    Next_State <= DO;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

----------------- Estado CLD -----------------

            when CLD => --CLEAR DISPLAY 0x01
            
                if CONT1 = X"0009C40" then -- estado de espera por 40us

                    READY <= '1';
                    DATA <= X"01"; --00000001
                    Next_State <= CLD;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= RETH;

                else

                    Next_State <= CLD;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

------ return cursor home
----------------- Estado RETH -----------------

            when RETH => --RETURN CURSOR HOME
                if CONT1 = X"0009C40" then -- estado de espera por 40us

                    READY <= '1';
                    DATA <= X"02"; --0000001X
                    Next_State <= RETH;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= SDDRAMA;

                else

                    Next_State <= RETH;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

------ ------
----------------- Estado SDDRAMA -----------------

            when SDDRAMA => --SET DD RAM ADDRESS posici�n del display del rengl�n 1 columna 4

                if CONT1 = X"00280A0" then -- estado de espera por 1.64ms = 164.000

                    READY <= '1';
                    DATA <= X"80"; --1-AC6-AC0, 80(R=1,C=1) 84(R=1,C=5)
                    Next_State <= SDDRAMA;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE1;

                else

                    Next_State <= SDDRAMA;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

---------------------------------------------------------------------------------------
--DATOS--DATOS--DATOS--DATOS--DATOS--DATOS--DATOS--DATOS--DATOS--DATOS--DATOS--
---------------------------------------------------------------------------------------
            when WRITE1 => --Write Data in DD RAM (S 53)
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <=    '1';
                    LCD_RS <= '1';
                    DATA <= M_C;
                    Next_State <= WRITE1;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';

                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE2;

                else

                    Next_State <= WRITE1;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
                    
            when WRITE2 => --Write Data in DD RAM (i 69, � A1)
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_O;
                    Next_State <= WRITE2;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State  <=  WRITE3;

                else

                    Next_State <= WRITE2;

                end if;

            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE3 => --Write Data in DD RAM (m 6D)
    
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_M;
                    Next_State <= WRITE3;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E<='1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE4;

                else

                    Next_State <= WRITE3;

                end if;
            
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE4 => --Write Data in DD RAM (b 62)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_I;
                    Next_State <= WRITE4;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE5;
                    
                else

                    Next_State<=WRITE4;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE5 => --Write Data in DD RAM (o 6F)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_D;
                    Next_State <= WRITE5;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';

                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE6;

                else

                    Next_State <= WRITE5;

                end if;
            
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE6 => --Write Data in DD RAM (l 6C)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_A;
                    Next_State <= WRITE6;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE7;

                else

                    Next_State <= WRITE6;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE7 => --Write Data in DD RAM (o 6F)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space;
                    Next_State <= WRITE7;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';

                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE8;

                else

                    Next_State <= WRITE7;

                end if;
                
                    RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
                    
            when WRITE8 => --Write Data in DD RAM (s 73)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_S; 
                    Next_State <= WRITE8;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
    
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE9;

                else

                    Next_State <= WRITE8;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE9 => --Write Data in DD RAM (space 20)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_E; 
                    Next_State <= WRITE9;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE10;

                else

                Next_State<=WRITE9;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE10 => --Write Data in DD RAM (A 41)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_R;
                    Next_State <= WRITE10;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E<='1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE11;

                else

                    Next_State <=   WRITE10;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE11 => --Write Data in DD RAM (S 53)
                
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_V;
                    Next_State <= WRITE11;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
            
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE12;

                else

                    Next_State <= WRITE11;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE12 => --Write Data in DD RAM (C 43)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_I;
                    Next_State <= WRITE12;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE13;

                else

                    Next_State <= WRITE12;

                end if;
                
                RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE13 => --Write Data in DD RAM (I 49)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_D; 
                    Next_State <= WRITE13;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                
                elsif CONT2 = "0011011" then
                    
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE14;

                else

                    Next_State <= WRITE13;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE14 => --Write Data in DD RAM (I 49)
                
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_A;
                    Next_State <= WRITE14;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= stop;

                else

                    Next_State <= WRITE14;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when stop => --esperar con el mensaje
                                
                if (stop_lcd = '1') then
                    next_state <= clear;
                else
                    next_state <= stop;     
                end if;                                     

            when clear => --CLEAR DISPLAY 0x01
            
                if CONT1 = X"0009C40" then -- estado de espera por 40us

                    READY <= '1';
                    DATA <= X"01"; --00000001
                    Next_State <= clear;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= idle;

                else

                    Next_State <= clear;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

        end case;
    end if;
end process; --FIN DEL PROCESO DE LA M�QUINA DE ESTADOS

end;
