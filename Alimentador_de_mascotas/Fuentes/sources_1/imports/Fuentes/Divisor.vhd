library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;


entity Divisor is
	port ( clk,rst :  in std_logic;
          pulse : out std_logic);
end Divisor;

architecture Behavioral of Divisor is

signal dd, qq : std_logic_vector(28 downto 0):=(others=>'0');
signal ovf    : std_logic;
begin


mux_add  : 
       dd <= qq + 1  when ovf='0' else
            (others=>'0'); 

-- ? [Hz]
ovf_com  :				 --001c270
    ovf <= '1' when qq = '0'&x"001c270" else --Frecuencia
        '0';

-- Flip-Flop
process (clk, rst)
begin  
   if rst = '1' then
      qq <= (others => '0');
   elsif (clk'event and clk = '1') then
      qq <= dd;
   else
      qq <= qq;
   end if;
end process;
		
-- output
    pulse <= '1' when qq > '0'&x"001c26f" else -- Ancho del pulso
        '0';
        
end Behavioral;
