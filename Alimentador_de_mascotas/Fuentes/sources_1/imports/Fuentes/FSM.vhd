library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity FSM is
	port(
			clk, rst, en:  in std_logic;
			start:  in std_logic;
			stop_motor: in std_logic;
			state: out std_logic_vector(3 downto 0));
end FSM;

architecture Behavioral of FSM is

type states is (inicio,s0,s1,s2,s3,s4,s5,s6,s7);
signal present_state, next_state: states;

begin

--===================
-- Present State
--===================
process(clk, rst)
  begin
  if rst = '1' then
    present_state <= inicio;
  elsif (clk'event and clk='1') then
    if en='1' then
	   present_state <= next_state;
    else		
	   present_state <= present_state;
	 end if;
  end if;
end process;
		
--===================
-- Next state logic
--===================
process(present_state, start, clk,stop_motor)
  begin 
  
  case present_state is
  
  when inicio =>
	   if (start = '1') then
		   next_state <= s0;   
       else
           next_state <= inicio;           
	   end if;

--Se elimino dir, solo se necesita una direccion
    
    when s0 =>
        next_state <= s1;

    when s1 =>
        next_state <= s2;
	
    when s2 =>
        next_state <= s3;
	
    when s3 =>
        next_state <= s4;		

    when s4 =>
        next_state <= s5;
      
    when s5 =>
        next_state <= s6;

    when s6 =>
        next_state <= s7;             

-- Realiza 512 ciclos que son 5 segundos, entonces el motor para si no hay start
    when s7 => 
        if (stop_motor = '1') then
            next_state <= inicio;
        else
            next_state <= s0;     
        end if;
			
  end case;
end process;
	
--===================	
-- Output logic
--===================
process(present_state)
  begin
  case present_state is
  
    when inicio =>
	   state <= "0000";	   
	  
    when s0 =>
	   state <= "1000";

    when s1 =>
	   state <= "1100";

    when s2 =>
	   state <= "0100";

    when s3 =>
	   state <= "0110";

    when s4 =>
	   state <= "0010";

    when s5 =>
	   state <= "0011";

    when s6 =>
	   state <= "0001";

    when s7 =>
	   state <= "1001";
						
  end case;
end process;

		  
end Behavioral;
