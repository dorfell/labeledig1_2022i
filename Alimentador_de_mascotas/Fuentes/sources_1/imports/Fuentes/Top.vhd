library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity Top is

    Port ( clk : in std_logic;
           rst, start: in std_logic;
           rd_uart: in std_logic;  -- Se puede dejar abierto
           rx: in std_logic;   -- Se puede dejar abierto
           col: in std_logic_vector(3 downto 0);           
--           tx_full, rx_empty: out std_logic;   -- Se puede dejar abierto
--           r_data: out std_logic_vector(7 downto 0);   -- Se puede dejar abierto
           tx: out std_logic;
           state: out std_logic_vector(3 downto 0);
           pulse: out std_logic;
           filas: out std_logic_vector(3 downto 0);
           LCD_RS : out STD_LOGIC; --del LCD (JD1)
           LCD_RW : out STD_LOGIC; --read/write del LCD (JD2)
           LCD_E : out STD_LOGIC; --enable del LCD (JD3)
           DATA : out STD_LOGIC_VECTOR (7 downto 0); --bus de datos de la LCD (JC1-4,7-10)
           segmentos: out std_logic_vector(6 downto 0);
           anodo: out std_logic_vector(7 downto 0)       
          );
end Top;

architecture Behavioral of Top is

signal fifo_start : std_logic;
signal write_alimento: std_logic;
signal msg_alimento: std_logic_vector(7 downto 0); 
signal stop: std_logic;
signal str: std_logic; --Se ha cambiado la entrada de boton a teclado 

component UART is
	generic(
		DBIT: integer := 8;     -- # data bits
		SB_TICK: integer := 16; -- # ticks de parada
										-- 32/48/64 para 1/1.5/2 bits
		DVSR: integer := 652;   -- divisor de ticks
										-- = clk/(16*baudrate)
		DVSR_BIT: integer := 10; -- # bits de DVSR
		FIFO_W: integer := 4    -- # bits de direccion del fifo
										-- # palabras en FIFO = 2^FIFO_W
	);
	port(
		clk, rst: in std_logic;
		rd_uart, wr_uart: in std_logic; -- Para el FIFO
		rx: in std_logic;
		w_data: in std_logic_vector(DBIT-1 downto 0);
		tx_full, rx_empty: out std_logic;
		r_data: out std_logic_vector(DBIT-1 downto 0);
		tx: out std_logic
	);
end component;

component LCD is
	port(
        CLOCK : in STD_LOGIC; --reloj de 100MHz
        start : in std_logic;
        stop_lcd : in std_logic;
        rs : in STD_LOGIC; --boton de reinicio a BTNC
        LCD_RS : out STD_LOGIC; --del LCD (JD1)
        LCD_RW : out STD_LOGIC; --read/write del LCD (JD2)
        LCD_E : out STD_LOGIC; --enable del LCD (JD3)
        DATA : out STD_LOGIC_VECTOR (7 downto 0) --bus de datos de la LCD (JC1-4,7-10)
	);
end component;

component Teclado is
	port(
       Reloj: in std_logic;
       col: in std_logic_vector(3 downto 0);
       filas: out std_logic_vector(3 downto 0);
       Segmentos: out std_logic_vector(6 downto 0);   
       start: out std_logic
	);
end component;

component PWM_Bocina is
	port(
			clk, start, rst:  in std_logic;  --Empieza la carga del mensaje
            stop_motor, pulse : out std_logic
	);
end component;

component Retraso_Fifo is
	port(
			clk, start, rst:  in std_logic;  --Empieza la carga del mensaje
            pulse : out std_logic
	);
end component;

component Mensaje_Alimento is
	port(
			clk, rst:  in std_logic;
			start        :  in std_logic;  --Empieza la carga del mensaje
			data_user: out std_logic_vector(7 downto 0); --Se utiliza para el mensaje a tx
			wr_uart : out std_logic --Libera el mensaje al terminar de cargar la fifo
	);
end component;

component Completo is
    Port ( clk : in std_logic;
           rst, start, stop_motor : in std_logic;
           state: out std_logic_vector(3 downto 0)
          );
end component;

begin

    lcd_16x1 : LCD PORT MAP(
        CLOCK => clk,
        start => start,
        rs => rst,
        stop_lcd => stop,
        LCD_RS => LCD_RS,
        LCD_RW => LCD_RW,
        LCD_E => LCD_E,
        DATA => DATA
    );

    teclado_matricial : Teclado PORT MAP(
        reloj => clk,
        col => col,
        filas => filas,
        Segmentos => segmentos,
        start => open --start, signal
    );

    bocina : PWM_Bocina PORT MAP(
        clk => clk,
        rst => rst,
        start => start,
        pulse => pulse,
        stop_motor => stop
    );

    retraso : Retraso_Fifo PORT MAP(
        clk => clk,
        rst => rst,
        start => start,
        pulse => fifo_start
    );

    mensaje : Mensaje_Alimento PORT MAP(
        clk => clk,
        rst => rst,
        start => fifo_start, --Inicia entrega de alimento
        data_user => msg_alimento, --Vector que entrega los caracteres
        wr_uart => write_alimento
    );

    uart_bluetooth : UART PORT MAP(
            clk => clk, 
            rst => rst,
            rd_uart => rd_uart, 
            wr_uart => write_alimento, 
            rx => rx, 
            tx => tx,
            w_data => msg_alimento, --Recibe los caracteres
            tx_full => open,
            rx_empty => open,
            r_data => open
        );

    
    motor : Completo PORT MAP(
            clk => clk, 
            rst => rst,
            start => start,
            state => state,
            stop_motor => stop
        );

anodo <= "11111110";

end Behavioral;
