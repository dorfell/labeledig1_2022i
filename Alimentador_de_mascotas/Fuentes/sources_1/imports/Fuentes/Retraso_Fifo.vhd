library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;


entity Retraso_Fifo is
	port ( clk, start, rst :  in std_logic;
          pulse : out std_logic);
end Retraso_Fifo;

architecture Behavioral of Retraso_Fifo is

signal dd, qq : std_logic_vector(27 downto 0):=(others=>'0');
signal ovf    : std_logic;
begin


mux_add  : 
       dd <= qq + 1  when ovf='0' else
            (others=>'0'); 

-- ? [Hz]
ovf_com  :				 --1 segundo 5f5e100
    ovf <= '1' when qq = x"5f5e100" else --Frecuencia
        '0';

-- Flip-Flop
process (clk, start,rst,qq)
begin  
   if rst = '1' then
      qq <= (others => '0');
   elsif (start = '1' or qq > x"0") then 
       if (clk'event and clk = '1') then
          qq <= dd;
       else
          qq <= qq;
       end if;      
   else
      qq <= qq;
   end if;
end process;
		
-- output                  --1 segundo 5f5e0ff
    pulse <= '1' when qq = x"5f5e0ff" else -- Ancho del pulso
        '0';
        
end Behavioral;
