library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Teclado is
   Port (
   Reloj: in std_logic;
   col: in std_logic_vector(3 downto 0);
   filas: out std_logic_vector(3 downto 0);
   Segmentos: out std_logic_vector(6 downto 0);   
   start: out std_logic
    );
end Teclado;

architecture Behavioral of Teclado is

COMPONENT TECLADO_M is

GENERIC(
	  FREQ_CLK : INTEGER := 100_000_000         --FRECUENCIA DE LA TARJETA
);


PORT(
	CLK 		  : IN  STD_LOGIC; 						  --RELOJ FPGA
	COLUMNAS   : IN  STD_LOGIC_VECTOR(3 DOWNTO 0); --PUERTO CONECTADO A LAS COLUMNAS DEL TECLADO
	FILAS 	  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --PUERTO CONECTADO A LA FILAS DEL TECLADO
	BOTON_PRES : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --PUERTO QUE INDICA LA TECLA QUE SE PRESION�
	IND		  : OUT STD_LOGIC							  --BANDERA QUE INDICA CUANDO SE PRESION� UNA TECLA (S�LO DURA UN CICLO DE RELOJ)
);

end component TECLADO_M;

signal boton_pres : std_logic_vector (3 downto 0) := (others=> '0');
signal ind:  std_logic :='0';
signal segm: std_logic_vector (6 downto 0) := "1111110";
signal str: std_logic := '0';

begin 
libreria : TECLADO_M Generic map (FREQ_CLK => 100000000)
  port map(Reloj, col, filas, boton_pres, ind);
  
  Proceso_Teclado: process (Reloj, ind, boton_pres, str, segm) begin
      if rising_edge (Reloj) then
          if ind='1' and boton_pres = x"0" then segm <= "0000001";
          elsif ind='1' and boton_pres = x"1" then segm <= "1001111";
          elsif ind='1' and boton_pres = x"2" then segm <= "0010010";
          elsif ind='1' and boton_pres = x"3" then segm <= "0000110";
          elsif ind='1' and boton_pres = x"4" then segm <= "1001100";
          elsif ind='1' and boton_pres = x"5" then segm <= "0100100";
          elsif ind='1' and boton_pres = x"6" then segm <= "0100000";
          elsif ind='1' and boton_pres = x"7" then segm <= "0001111";
          elsif ind='1' and boton_pres = x"8" then segm <= "0000000";
          elsif ind='1' and boton_pres = x"9" then segm <= "0000100";
          elsif ind='1' and boton_pres = x"A" then segm <= "0001000";
          elsif ind='1' and boton_pres = x"B" then segm <= "1100000";
          elsif ind='1' and boton_pres = x"C" then segm <= "0110001";
          elsif ind='1' and boton_pres = x"D" then segm <= "1000010";
          elsif ind='1' and boton_pres = x"E" then segm <= "0110000";
          elsif ind='1' and boton_pres = x"F" then segm <= "0111000";
          else segm <= segm;
          
          end if;
       end if;

    if (segm = "0111000" and ind = '1') then
        str <= '1';
    else
        str <= '0';    
    end if;       
           
    start <= str;       
    Segmentos <= segm;
       
  end process;
end Behavioral;
