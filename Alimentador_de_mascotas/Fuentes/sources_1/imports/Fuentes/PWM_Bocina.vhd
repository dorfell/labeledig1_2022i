library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity PWM_Bocina is
	port ( clk,rst,start :  in std_logic;
	       stop_motor,  pulse: out std_logic);
end PWM_Bocina;

architecture Behavioral of PWM_Bocina is

signal dd_0, qq_0 : std_logic_vector(19 downto 0):=(others=>'0');
signal dd_1, qq_1: std_logic_vector(31 downto 0):=(others=>'0');
signal ovf_0, ovf_1: std_logic;
signal stop: std_logic;
signal pulse_bocina: std_logic;

begin

--Para el de 440 Hz
mux_add_0  : 
    dd_0 <= qq_0 + 1  when ovf_0='0' else
        (others=>'0');

--Para que la bocina funcione un tiempo       
mux_add_1  : 
    dd_1 <= qq_1 + 1  when ovf_1='0' else
        (others=>'0');          

-- 440 [Hz] 377c8
ovf_com_0  :				
    ovf_0 <= '1' when qq_0 = x"377c8" else -- 50 [Hz]
        '0';

-- 5s 1dcd6500
ovf_com_1  :				
    ovf_1 <= '1' when qq_1 = x"1dcd6500" else -- 50 [Hz]
        '0';

-- Flip-Flop
process (clk, rst,start,qq_1,qq_0)
begin  
   if rst = '1' then
      qq_0 <= (others => '0');
      qq_1 <= (others => '0');      
   elsif (start = '1' or qq_1 > x"0") then 
        if (clk'event and clk = '1') then
            qq_0 <= dd_0;
            qq_1 <= dd_1;
        else
            qq_0 <= qq_0;
            qq_1 <= qq_1;
        end if;        
   else
        qq_0 <= qq_0;
        qq_1 <= qq_1; 
    end if;
end process;

-- output 90%
pulse_bocina <= '1' when qq_0 < x"31f01"  else
    '0';
    
stop <= '1' when qq_1 = x"0"  else
    '0';    
    
pulse <= pulse_bocina;
stop_motor <= stop;    

end Behavioral;
