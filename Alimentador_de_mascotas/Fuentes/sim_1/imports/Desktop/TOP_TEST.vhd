library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TOP_TEST is
end TOP_TEST;

architecture Behavioral of TOP_TEST is

        component Top is
        Port ( clk : in std_logic;
               rst, start: in std_logic;
               rd_uart: in std_logic;  -- Se puede dejar abierto
               rx: in std_logic;   -- Se puede dejar abierto
               col: in std_logic_vector(3 downto 0);           
--               tx_full, rx_empty: out std_logic;   -- Se puede dejar abierto
--               r_data: out std_logic_vector(7 downto 0);   -- Se puede dejar abierto
               segmentos: out std_logic_vector(6 downto 0);
               tx: out std_logic;
               state: out std_logic_vector(3 downto 0);
               pulse: out std_logic;
               filas: out std_logic_vector(3 downto 0);
               LCD_RS : out STD_LOGIC; --del LCD (JD1)
               LCD_RW : out STD_LOGIC; --read/write del LCD (JD2)
               LCD_E : out STD_LOGIC; --enable del LCD (JD3)
               DATA : out STD_LOGIC_VECTOR (7 downto 0) --bus de datos de la LCD (JC1-4,7-10)  
              );
        end component Top;
        
        --Inputs
         signal clk : std_logic := '1';
         signal rst: std_logic  := '0';
         signal start: std_logic  := '0';
         signal rd_uart: std_logic := '0'; 
         signal rx: std_logic := '1';
         signal col: std_logic_vector(3 downto 0) := (others =>'0');
        
        --Outputs
         signal tx_full: std_logic; 
         signal rx_empty: std_logic;
         signal r_data: std_logic_vector(7 downto 0);
         signal tx: std_logic;
         signal state: std_logic_vector(3 downto 0);         
         signal pulse: std_logic;
         signal filas: std_logic_vector(3 downto 0);
         signal LCD_RS : STD_LOGIC; --del LCD (JD1)
         signal LCD_RW : STD_LOGIC; --read/write del LCD (JD2)
         signal LCD_E : STD_LOGIC; --enable del LCD (JD3)
         signal DATA : STD_LOGIC_VECTOR (7 downto 0); --bus de datos de la LCD (JC1-4,7-10)
         signal segmentos: std_logic_vector(6 downto 0);                
            

begin

    clock:
    clk <=  '1' after 5 ns when clk = '0' else
            '0' after 5 ns when clk = '1';

   -- Instantiate the Unit Under Test(UUT)
    uut: Top PORT MAP(
        clk => clk,
        rst => rst,
        start => start,   
        rd_uart => rd_uart,
        rx => rx,
        col => col,
--        tx_full => tx_full,
--        rx_empty => rx_empty,
--        r_data => r_data,
        tx => tx,
        state => state,
        pulse => pulse,
        filas => filas,
        segmentos => segmentos     
        );

    -- Stimulus process
    stim_proc: process
 
    begin
        wait for 10 ns;
            rst <= '1';
        wait for 1000 ns;    
            rst <= '0';
            start <= '1';
            col <= "1111";
        wait for 10000 ns;
            start <= '0';           
        wait for 1 ms;
            col <= "0000";                                                                                                      
        wait;                      
    end process;

end Behavioral;
