----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.06.2022 08:40:53
-- Design Name: 
-- Module Name: Buzzer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Buzzer is
    Port (
    clk, rst: in std_logic;
    Sonido : out std_logic;
    ena : in std_logic;
    start : in std_logic
     );
     
end Buzzer;

architecture Behavioral of Buzzer is
type states is (s0,s1,s2,s3,s4);
signal present_state, next_state: states;

begin

--===================
-- Present State
--===================
process(clk, rst)
  begin
  if rst = '1' then
    present_state <= s0;
  elsif (clk'event and clk='1') then
    if ena='1' then
	   present_state <= next_state;
    else		
	   present_state <= present_state;
	 end if;
  end if;
end process;

		
--===================
-- Next state logic
--===================
process(present_state, start)
  begin 
  case present_state is 
    when s0 =>
	   if (start = '1') then
		   next_state <=  s1;
		else 
		   next_state <=  s0;
		end if;

    when s1  =>
	      next_state <=  s2;

    when s2  =>
	      next_state <=  s3;

    when s3  =>
	      next_state <=  s4;

    when s4  =>
	      next_state <=  s0;
			
  end case;
end process;
	
--===================	
-- Output logic
--===================
process(present_state)
  begin
  case present_state is
    when s0  =>
	   Sonido <= '0';

    when s1  =>
	   Sonido <= '1';

    when s2  =>
	   Sonido <= '1';

    when s3  =>
	   Sonido <= '1';

    when s4  =>
	   Sonido <= '1';

  end case;
end process;
	
end Behavioral;
