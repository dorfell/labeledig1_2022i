library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity baud_gen is
     generic(  --Son variables que se pueden alterar segun necesidades de funcionamiento
        M: integer := 652;   --Genera un tick cada M ciclos de reloj = (Velocidad reloj/(Baud rate * Ticks))
        MBITS: integer := 10  --el tama�o del vector necesario para contar hasta M, MBITS = Log2(M)
              );
     port (
        clk, rst: in std_logic;           
        baud_ticks: out std_logic );
end baud_gen;

architecture Behavioral of baud_gen is

signal r_reg, r_next: unsigned( MBITS-1 downto 0); --Crea el vector para contar hasta M 

begin

--register 
    process (clk, rst)
    begin
        if (rst='1') then
           r_reg <=(others=>'0');
        elsif (clk'event and clk='1') then
           r_reg<=r_next;
        end if;
    end process;
    
    --logic
    r_next <= (others=> '0') when r_reg = (M-1) else r_reg + 1;   --Borra el registro (overflow) cuando cuenta M ciclos de reloj    
    --out  
    
    baud_ticks <='1' when r_reg = (M-1) else '0'; -- Genera una se�al igual al reloj cuando a contado M ciclos de reloj
    

end Behavioral;