library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity uart_tx is

    generic( --Son variables que se pueden alterar segun necesidades de funcionamiento
        DBIT: integer:= 8;   -- Numero bits de datos
        SB_TICK: integer:=16 -- Ticks de bits de parada
    );
    
    Port (
        clk, rst : in std_logic;
        tx_start : in std_logic;
        s_tick: in std_logic;
        din: in std_logic_vector(7 downto 0);         
        tx_done_tick: out std_logic;
        tx : out std_logic
    );
    
end uart_tx;

architecture Behavioral of uart_tx is
    type state_type is (idle, start, data, stop); --FSM con sus cuatro estados
    signal state_reg, state_next: state_type; --Estado actual y siguiente
    signal s_reg, s_next: unsigned(3 downto 0); --Contador 0 a 15 para los ticks
    signal n_reg, n_next: unsigned(2 downto 0); --Contador para data bits, por definicion puede ser de 6 a 8
    signal b_reg, b_next: std_logic_vector(7 downto 0); --Almacenar informacion 
    signal tx_reg, tx_next: std_logic;

begin

    --FSMD estados y registro de datos
    process(clk,rst) --Se requiere el proceso porque rx depende del reloj
    begin
        if rst = '1' then --Se vuelve a inactivo y se borran los registros, rst asincrono
            state_reg <= idle;
            s_reg <= (others=> '0');
            n_reg <= (others=> '0');
            b_reg <= (others=> '0');
            tx_reg <= '1';
        elsif(clk'event and clk = '1') then --En flanco ascendente, se pasa al siguiente estado
            state_reg <= state_next;
            s_reg <= s_next;
            n_reg <= n_next;
            b_reg <= b_next;
            tx_reg <= tx_next;
        end if;
    end process;
    
    -- Funcionamiento de los estados al ir al siguiente
    process(state_reg, s_reg, n_reg, b_reg, s_tick, tx_reg, tx_start, din)
    begin
        state_next <= state_reg;
        s_next <= s_reg;    
        n_next <= n_reg;
        b_next <= b_reg ;
        tx_next <= tx_reg;
        tx_done_tick <= '0';
        case state_reg is --Revisa el estado del registro de estados
            when idle =>  --Estado inactivo
                tx_next <= '1'; --Genera 1 en tx
                if tx_start ='1' then --Se le informa al dispositivo que inicie a transmitir
                    state_next <= start; --Pasa al estado de bit de inicio
                    s_next <= (others=>'0'); -- Se arranca el contador de ticks  
                    b_next <= din; -- Se almacena el paquete de datos en din
                end if;
            when start => --Estado inicio
                tx_next <= '0'; --Genera 0 en tx, indicando inicio del paquete
                if (s_tick = '1') then --Depende del baud tick
                    if s_reg = 15 then --Termina de contar en el mismo intervalo del baud_gen                   
                        state_next <= data; 
                        s_next <= (others => '0'); --Borra contador de ticks
                        n_next <= (others => '0'); --Inicia contador de data bits
                    else
                        s_next <= s_reg + 1; --Suma 1 al contador de bits
                    end if;
                end if;
            when data => --Estado de datos
                tx_next <= b_reg(0); --Obtiene el LSB 
                if (s_tick = '1') then
                    if s_reg = 15 then --Llega a la mitad del siguiente dato
                        s_next <= (others => '0'); --Borra registro ticks    
                        b_next <= '0' & b_reg(7 downto 1); --Agrega 0 a la izquierda y los datos de las posiciones 7 a 1 se mueven a la derecha, el segundo bit menos significativo se convierte en el LSB de este nuevo vector
                        if n_reg = (DBIT-1) then -- Si el registro a contado los bits de datos
                            state_next <= stop; --Se pasa a estado de parada
                        else
                            n_next <= n_reg + 1; --Se le suma 1 al registro
                        end if;
                    else
                        s_next <= s_reg + 1;
                    end if;
                end if;            
             when stop =>
                tx_next <= '1'; --los datos de bits han terminado
                if (s_tick = '1') then 
                    if s_reg = (SB_TICK-1) then
                        state_next <= idle; --Se vuelve a inactivo
                        tx_done_tick <= '1'; --Se informa que ha terminado
                    else
                        s_next <= s_reg+1;
                    end if;
                end if;
          end case;                    
    end process;
    
    -- Outputs
    tx <= tx_reg; --Los valores que se van asignando a tx_reg son transmitidos
     
end Behavioral;
