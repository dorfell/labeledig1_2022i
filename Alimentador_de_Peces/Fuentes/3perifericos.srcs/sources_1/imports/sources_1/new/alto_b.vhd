----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.06.2022 19:23:12
-- Design Name: 
-- Module Name: alto_b - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Top is

    Port ( clk : in std_logic;
           rst,start: in std_logic;
--           rd_uart: in std_logic;  -- Se puede dejar abierto
--           rx: in std_logic;   -- Se puede dejar abierto         
--           tx_full, rx_empty: out std_logic;   -- Se puede dejar abierto
--           r_data: out std_logic_vector(7 downto 0);   -- Se puede dejar abierto
           tx: out std_logic     
          );
end Top;

architecture Behavioral of Top is

signal fifo_start : std_logic;
signal write_alimento: std_logic;
signal msg_alimento: std_logic_vector(7 downto 0); 
signal stop: std_logic;
signal str: std_logic; --Se ha cambiado la entrada de boton a teclado 

component UART is
	generic(
		DBIT: integer := 8;     -- # data bits
		SB_TICK: integer := 16; -- # ticks de parada
										-- 32/48/64 para 1/1.5/2 bits
		DVSR: integer := 652;   -- divisor de ticks
										-- = clk/(16*baudrate)
		DVSR_BIT: integer := 10; -- # bits de DVSR
		FIFO_W: integer := 4    -- # bits de direccion del fifo
										-- # palabras en FIFO = 2^FIFO_W
	);
	port(
		clk, rst: in std_logic;
		wr_uart: in std_logic; -- Para el FIFO
		w_data: in std_logic_vector(DBIT-1 downto 0);
		tx_full, rx_empty: out std_logic;
		r_data: out std_logic_vector(DBIT-1 downto 0);
		tx: out std_logic
	);
end component;



component Retraso_Fifo is
	port(
			clk, start, rst:  in std_logic;  --Empieza la carga del mensaje
            pulse : out std_logic
	);
end component;

component Mensaje_Alimento is
	port(
			clk, rst:  in std_logic;
			start        :  in std_logic;  --Empieza la carga del mensaje
			data_user: out std_logic_vector(7 downto 0); --Se utiliza para el mensaje a tx
			wr_uart : out std_logic --Libera el mensaje al terminar de cargar la fifo
	);
end component;


begin


    retraso : Retraso_Fifo PORT MAP(
        clk => clk,
        rst => rst,
        start => start,
        pulse => fifo_start
    );

    mensaje : Mensaje_Alimento PORT MAP(
        clk => clk,
        rst => rst,
        start => fifo_start, --Inicia entrega de alimento
        data_user => msg_alimento, --Vector que entrega los caracteres
        wr_uart => write_alimento
    );

    uart_bluetooth : UART PORT MAP(
            clk => clk, 
            rst => rst,
            wr_uart => write_alimento, 
            tx => tx,
            w_data => msg_alimento, --Recibe los caracteres
            tx_full => open,
            rx_empty => open,
            r_data => open
        );

   
end Behavioral;