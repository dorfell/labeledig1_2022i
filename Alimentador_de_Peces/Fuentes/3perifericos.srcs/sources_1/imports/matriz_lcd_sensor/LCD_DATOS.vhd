----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.06.2022 20:30:36
-- Design Name: 
-- Module Name: LCD_DATOS - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LCD_DATOS is
    Port ( 
    CLOCK : in STD_LOGIC; --reloj de 100MHz
    REINI : in STD_LOGIC; --boton de reinicio a BTNC
    ENABLE : in STD_LOGIC; --Enable para 1 s de funcionamiento
    LCD_RS : out STD_LOGIC; --del LCD (JD1)
    LCD_RW : out STD_LOGIC; --read/write del LCD (JD2)
    LCD_E : out STD_LOGIC; --enable del LCD (JD3)
    TEMP : in std_logic_vector(12 downto 0); --Datos de temperatura 13 bit signo - 0-12 bits datos de temp
    DATA : out STD_LOGIC_VECTOR (7 downto 0); --bus de datos de la LCD (JC1-4,7-10)
    Control : in STD_LOGIC_VECTOR (1 downto 0) --Opci�n escogida en el men� de usuraio
    );
end LCD_DATOS;



architecture Behavioral of LCD_DATOS is

-- FSM states
type STATE_TYPE is (
RST, ST0, ST1, FSET, EMSET, DO, CLD, RETH, SDDRAMA,
CLD2, CLD3, CLD4, RETH2, RETH3, RETH4,  
SDDRAMA2, SDDRAMA3, SDDRAMA4, SDDRAMA5, SDDRAMA6, SDDRAMA7, SDDRAMA8, SDDRAMA9,
WRITE1, WRITE2, WRITE3, WRITE4, WRITE5, WRITE6, WRITE7, WRITE8, WRITE9, WRITE10,
WRITE11, WRITE12, WRITE13, WRITE14, WRITE15, WRITE16, WRITE17, WRITE18, WRITE19, WRITE20,
WRITE21, WRITE22, WRITE23, WRITE24, WRITE25, WRITE26, WRITE27, WRITE28, WRITE29, WRITE30,
WRITE31, WRITE32, WRITE33, WRITE34, WRITE35, WRITE36, WRITE37, WRITE38, WRITE39, WRITE40,
WRITE41, WRITE42, WRITE43, WRITE44, WRITE45, WRITE46, WRITE47, WRITE48, WRITE49, WRITE50,
WRITE51, WRITE52, WRITE53, WRITE54, WRITE55, WRITE56, WRITE57, WRITE58, WRITE59, WRITE60,
WRITE61, WRITE62, WRITE63, WRITE64, WRITE65, WRITE66, WRITE67, WRITE68, WRITE69, WRITE70,
WRITE71, WRITE72, WRITE73, WRITE74, WRITE75, WRITE76, WRITE77, WRITE78, WRITE79, WRITE80,
WRITE81, WRITE82, WRITE83, WRITE84, WRITE85, WRITE86, WRITE87, WRITE88, WRITE89, WRITE90,
WRITE91, WRITE92, WRITE93, WRITE94, WRITE95, WRITE96, WRITE97, WRITE98,
Delay0, Delay1, Delay2, Delay3, Delay4, Delay5, Delay6, Delay7, Delay8);

-- se�ales
signal State,Next_State : STATE_TYPE;
signal CONT1 : STD_LOGIC_VECTOR(23 downto 0) := X"000000"; -- 16,777,216 = 0.33554432 s MAX
signal CONT2 : STD_LOGIC_VECTOR(5 downto 0) :="000000"; -- 64 = 0.64 us

signal RESET : STD_LOGIC :='0';
signal READY : STD_LOGIC :='0';

signal dd, CONT3 : std_logic_vector(3 downto 0):=(others=>'0');
signal ovf: std_logic;

----------------------------------------------------------------------------------------------------------------
-- ASCII: hexadecimal(x"") decimal
----constantes ASCII (la "M_" antes de la letra es para may�scula)
constant space: STD_LOGIC_VECTOR(7 downto 0) := x"20"; -- [space] 32
constant admirC: STD_LOGIC_VECTOR(7 downto 0) := x"21"; -- ! 33
constant porcient: STD_LOGIC_VECTOR(7 downto 0) := x"25"; -- % 37
constant asterisc: STD_LOGIC_VECTOR(7 downto 0) := x"2A"; -- * 42
constant menos: STD_LOGIC_VECTOR(7 downto 0) := x"2D"; -- - 45
constant punto: STD_LOGIC_VECTOR(7 downto 0) := x"2E"; -- . 46
constant cero: STD_LOGIC_VECTOR(7 downto 0) := x"30"; -- n�mero 0 48
constant uno: STD_LOGIC_VECTOR(7 downto 0) := x"31"; -- n�mero 1 49
constant dos: STD_LOGIC_VECTOR(7 downto 0) := x"32"; -- n�mero 2 50
constant tres: STD_LOGIC_VECTOR(7 downto 0) := x"33"; -- n�mero 3 51
constant cuatro: STD_LOGIC_VECTOR(7 downto 0) := x"34"; -- n�mero 4 52
constant cinco: STD_LOGIC_VECTOR(7 downto 0) := x"35"; -- n�mero 5 53
constant seis: STD_LOGIC_VECTOR(7 downto 0) := x"36"; -- n�mero 6 54
constant siete: STD_LOGIC_VECTOR(7 downto 0) := x"37"; -- n�mero 7 55
constant ocho: STD_LOGIC_VECTOR(7 downto 0) := x"38"; -- n�mero 8 56
constant nueve: STD_LOGIC_VECTOR(7 downto 0) := x"39"; -- n�mero 9 57
constant dosptos: STD_LOGIC_VECTOR(7 downto 0) := x"3A"; -- s�mbolo : 58
constant igual: STD_LOGIC_VECTOR(7 downto 0) := x"3D"; -- s�mbolo = 61
constant interrog: STD_LOGIC_VECTOR(7 downto 0) := x"3F"; -- s�mbolo ? 63

constant M_A: STD_LOGIC_VECTOR(7 downto 0) := x"41"; -- A 65
constant M_B: STD_LOGIC_VECTOR(7 downto 0) := x"42"; -- B 66
constant M_C: STD_LOGIC_VECTOR(7 downto 0) := x"43"; -- C 67
constant M_L: STD_LOGIC_VECTOR(7 downto 0) := x"4C"; -- L 76
constant M_T: STD_LOGIC_VECTOR(7 downto 0) := x"54"; -- T 84
constant M_M: STD_LOGIC_VECTOR(7 downto 0) := x"4D"; -- M 77
constant a: STD_LOGIC_VECTOR(7 downto 0) := x"61"; -- a 97
constant b: STD_LOGIC_VECTOR(7 downto 0) := x"62"; -- b 98
constant c: STD_LOGIC_VECTOR(7 downto 0) := x"63"; -- c 99
constant d: STD_LOGIC_VECTOR(7 downto 0) := x"64"; -- d 100
constant e: STD_LOGIC_VECTOR(7 downto 0) := x"65"; -- e 101
constant f: STD_LOGIC_VECTOR(7 downto 0) := x"66"; -- f 102
constant g: STD_LOGIC_VECTOR(7 downto 0) := x"67"; -- g 103
constant h: STD_LOGIC_VECTOR(7 downto 0) := x"68"; -- h 104
constant i: STD_LOGIC_VECTOR(7 downto 0) := x"69"; -- i 105
constant j: STD_LOGIC_VECTOR(7 downto 0) := x"6A"; -- j 106
constant k: STD_LOGIC_VECTOR(7 downto 0) := x"6B"; -- k 107
constant l: STD_LOGIC_VECTOR(7 downto 0) := x"6C"; -- l 108
constant m: STD_LOGIC_VECTOR(7 downto 0) := x"6D"; -- m 109
constant n: STD_LOGIC_VECTOR(7 downto 0) := x"6E"; -- n 110
constant o: STD_LOGIC_VECTOR(7 downto 0) := x"6F"; -- o 111
constant p: STD_LOGIC_VECTOR(7 downto 0) := x"70"; -- p 112
constant q: STD_LOGIC_VECTOR(7 downto 0) := x"71"; -- q 113
constant r: STD_LOGIC_VECTOR(7 downto 0) := x"72"; -- r 114
constant s: STD_LOGIC_VECTOR(7 downto 0) := x"73"; -- s 115
constant t: STD_LOGIC_VECTOR(7 downto 0) := x"74"; -- t 116
constant u: STD_LOGIC_VECTOR(7 downto 0) := x"75"; -- u 117
constant v: STD_LOGIC_VECTOR(7 downto 0) := x"76"; -- v 118
constant w: STD_LOGIC_VECTOR(7 downto 0) := x"77"; -- w 119
constant x: STD_LOGIC_VECTOR(7 downto 0) := x"78"; -- x 120
constant y: STD_LOGIC_VECTOR(7 downto 0) := x"79"; -- y 121
constant z: STD_LOGIC_VECTOR(7 downto 0) := x"7A"; -- z 122
constant llaveA: STD_LOGIC_VECTOR(7 downto 0) := x"7B"; -- { 123
constant borrar: STD_LOGIC_VECTOR(7 downto 0) := x"7F"; -- delete 127
constant grade: STD_LOGIC_VECTOR(7 downto 0) := x"DF"; -- � 248
constant alfa: STD_LOGIC_VECTOR(7 downto 0) := x"E0"; -- ? 224
constant sig_menos : STD_LOGIC_VECTOR(7 downto 0) := x"B0"; -- -

signal temp1 : std_logic_vector(3 downto 0) := TEMP(11) & TEMP(10) & TEMP(9) & TEMP(8);
signal temp2 : std_logic_vector(3 downto 0) := TEMP(7) & TEMP(6) & TEMP(5) & TEMP(4);
signal temp3 : std_logic_vector(3 downto 0) := TEMP(3) & TEMP(2) & TEMP(1) & TEMP(0);   

signal temp1_sg: STD_LOGIC_VECTOR(7 downto 0);
signal temp2_sg: STD_LOGIC_VECTOR(7 downto 0);
signal temp3_sg: STD_LOGIC_VECTOR(7 downto 0);
signal sig_temp_sg: STD_LOGIC_VECTOR(7 downto 0) ;


--constantes de tiempos
constant T1: STD_LOGIC_VECTOR(23 downto 0) := x"001FFE"; -- espera de 81.9us = 8.l90 
-- @ write > 37us (3700 = E74)


-------------------

begin

mux_add  : 
       dd <= CONT3 + 1  when ovf='0' else
            (others=>'0'); 

-- ? [Hz]
ovf_com  :				
		 ovf <= '1' when CONT3 = "1111" else 
              '0';

-------------------------------------------------------------------
--Contador de Retardos CONT1--

process(CLOCK,RESET)

begin
    if RESET = '1' then
        CONT1 <= (others => '0');
    elsif (CLOCK'event and CLOCK='1') then
        CONT1 <= CONT1 + 1;
    end if;
end process;

-------------------------------------------------------------------
--Contador para Secuencias CONT2--

process(CLOCK,READY)

begin
  
    if (CLOCK = '1' and CLOCK'event) then
        if READY = '1' then 
            CONT2 <= CONT2 + 1;
        else CONT2 <= "000000";
        end if;
    end if;
    
end process;

-------------------------------------------------------------------
--Contador para Secuencias CONT3 Enable--

process(CLOCK,RESET,ENABLE)

begin

   if RESET = '1' then
      CONT3 <= (others => '0');
   elsif (CLOCK'event and CLOCK = '1') then
    if ENABLE = '1' then
        CONT3 <= dd;
    else
      CONT3 <= CONT3;
    end if;
   end if;

end process;

-------------------------------------------------------------------
--Actualizaci�n de estados--

process (CLOCK, Next_State)

begin
   
    if (CLOCK = '1' and CLOCK'event) then 
        State <= Next_State;
    end if;

end process;

-------------------------------------------------------------------
--FSM--

process(CONT1,CONT2,State,CLOCK,REINI)
    begin
    if REINI = '1' THEN 
        Next_State <= RST;
        READY <= '0';
        RESET <= '0';
    elsif (CLOCK = '0' and CLOCK'event) then
        case State is
            when RST => -- Estado de reset
            
                if CONT1 = X"0000000"then --0s
                    LCD_RS <= '0';
                    LCD_RW <= '0';
                    LCD_E <= '0';
                    DATA <= X"00";
                    Next_State <= ST0;

                else

                    Next_State <= ST0;

                end if;

----------------- Estado 0 -----------------

            when ST0 => --Primer estado de espera por 25ms

                        --(20 ms = x 1E 8480 = 2.000.000)(15 ms = x 16 E360 = 1.500.000)

                if CONT1 = X"02625A0" then -- 2.500.000 = 25ms
                    READY <= '1';
                    DATA <= X"38"; -- FUNCTION SET 8BITS, 2 LINE, 5X7
                    Next_State <= ST0;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';

                elsif CONT2 = "0011011" then
                    
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= ST1;

                else

                    Next_State <= ST0;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

----------------- Estado 1 -----------------

            when ST1 => --Segundo estado de espera por 100us (10.000 = x 2710)
                
                if CONT1 = X"0006BD0" then -- 27.600 = 276us

                    READY <= '1';
                    DATA <= X"38"; -- FUNCTION SET
                    Next_State <= ST1;

               elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= FSET;

                else

                    Next_State <= ST1;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

----------------- Estado FSET -----------------

            when FSET => --FUNCTION SET 0x38 (8bits, 2 lineas, 5x7dots)

                if CONT1 = X"0009C40" then --espera por 40.000 = 40us

                    READY <= '1';
                    DATA <= X"38"; --001DL-N-F-XX
                    Next_State <= FSET;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= EMSET;

                else

                    Next_State <= FSET;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

----------------- Estado EMSET -----------------

            when EMSET => --ENTRY MODE SET 0x06 (1 right-moving cursor and address increment)

                if CONT1 = X"0009C40" then --estado de espera por 40us

                    READY <= '1';
                    DATA <= X"06"; --000001-I/D-SH
                    Next_State <= EMSET;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= DO;

                else

                    Next_State <= EMSET;

                end if;

            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

----------------- Estado D0 -----------------

            when DO => --DISPLAY ON/OFF 0x0C (DISPLAY-CURSOR-BLINKING on-off)

                if CONT1 = X"0009C40" then -- estado de espera por 40us

                    READY <= '1';
                    DATA <= X"0C"; --00001-D-C-B,display on, cursor on
                    Next_State <= DO;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= CLD;

                else

                    Next_State <= DO;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

----------------- Estado CLD -----------------

            when CLD => --CLEAR DISPLAY 0x01
            
                if CONT1 = X"0009C40" then -- estado de espera por 40us

                    READY <= '1';
                    DATA <= X"01"; --00000001
                    Next_State <= CLD;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= RETH;

                else

                    Next_State <= CLD;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

------ return cursor home
----------------- Estado RETH -----------------

            when RETH => --RETURN CURSOR HOME
                if CONT1 = X"0009C40" then -- estado de espera por 40us

                    READY <= '1';
                    DATA <= X"02"; --0000001X
                    Next_State <= RETH;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= SDDRAMA;

                else

                    Next_State <= RETH;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

------ ------
----------------- Estado SDDRAMA -----------------

            when SDDRAMA => --SET DD RAM ADDRESS posici�n del display del rengl�n 1 columna 4

                if CONT1 = X"00280A0" then -- estado de espera por 1.64ms = 164.000

                    READY <= '1';
                    DATA <= X"80"; --1-AC6-AC0, 80(R=1,C=1) 84(R=1,C=5)
                    Next_State <= SDDRAMA;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE1;

                else

                    Next_State <= SDDRAMA;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

---------------------------------------------------------------------------------------
--DATOS--DATOS--DATOS--DATOS--DATOS--DATOS--DATOS--DATOS--DATOS--DATOS--DATOS--
---------------------------------------------------------------------------------------
 
 ---Bienvenido alimentedor de ?----
 
            when WRITE1 => --Write Data in DD RAM (B)
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <=    '1';
                    LCD_RS <= '1';
                    DATA <= M_B; 
                    Next_State <= WRITE1;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';

                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE2;

                else

                    Next_State <= WRITE1;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
                    
            when WRITE2 => --Write Data in DD RAM (i)
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= i; 
                    Next_State <= WRITE2;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State  <=  WRITE3;

                else

                    Next_State <= WRITE2;

                end if;

            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE3 => --Write Data in DD RAM (e)
    
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= e; 
                    Next_State <= WRITE3;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E<='1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE4;

                else

                    Next_State <= WRITE3;

                end if;
            
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE4 => --Write Data in DD RAM (n)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= n; 
                    Next_State <= WRITE4;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE5;
                    
                else

                    Next_State<=WRITE4;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE5 => --Write Data in DD RAM (v)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= v; 
                    Next_State <= WRITE5;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';

                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE6;

                else

                    Next_State <= WRITE5;

                end if;
            
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE6 => --Write Data in DD RAM (e)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= e; 
                    Next_State <= WRITE6;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE7;

                else

                    Next_State <= WRITE6;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE7 => --Write Data in DD RAM (n)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= n; 
                    Next_State <= WRITE7;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';

                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE8;

                else

                    Next_State <= WRITE7;

                end if;
                
                    RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
                    
            when WRITE8 => --Write Data in DD RAM (i)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= i;
                    Next_State <= WRITE8;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
    
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE9;

                else

                    Next_State <= WRITE8;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE9 => --Write Data in DD RAM (d)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= d;
                    Next_State <= WRITE9;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE10;

                else

                Next_State<=WRITE9;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE10 => --Write Data in DD RAM (o)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= o;
                    Next_State <= WRITE10;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E<='1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= SDDRAMA2;

                else

                    Next_State <=   WRITE10;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
------ ------
            when SDDRAMA2 => --SET DD RAM ADDRESS posici�n del display rengl�n 2 columna 1

                if CONT1 = X"014050" then -- estado de espera por 1.64ms

                    READY <= '1';
                    LCD_RS <= '0';
                    DATA <= X"C0"; --1-AC6 - AC0, C0(R=2,C=1)
                    Next_State <= SDDRAMA2;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE11; --para brincar al segundo rengl�n

                else

                    Next_State <= SDDRAMA2;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

------ ------
            
            when WRITE11 => --Write Data in DD RAM (A)
                
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= A;
                    Next_State <= WRITE11;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
            
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE12;

                else

                    Next_State <= WRITE11;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE12 => --Write Data in DD RAM (l)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= l;
                    Next_State <= WRITE12;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE13;

                else

                    Next_State <= WRITE12;

                end if;
                
                RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE13 => --Write Data in DD RAM (i)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= i; --DATA<=X"49";
                    Next_State <= WRITE13;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                
                elsif CONT2 = "0011011" then
                    
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE14;

                else

                    Next_State <= WRITE13;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE14 => --Write Data in DD RAM (m)
                
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= m;
                    Next_State <= WRITE14;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE15;

                else

                    Next_State <= WRITE14;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE15 => --Write Data in DD RAM (e)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= e;
                    Next_State <= WRITE15;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then    
                    
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE16;

                else

                    Next_State <= WRITE15;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE16 => --Write Data in DD RAM (n)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= n; --DATA<=X"53";
                    Next_State <= WRITE16;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';

                elsif CONT2 = "0011011" then
                    
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE17;

                else

                    Next_State <= WRITE16;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE17 => --Write Data in DD RAM (t)
                
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= t;

                    Next_State <= WRITE17;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE18;

                else

                    Next_State <= WRITE17;

                end if;

            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE18 => --Write Data in DD RAM (a)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= a; 
                    Next_State <= WRITE18;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE19;

                else

                    Next_State <= WRITE18;

                end if;
            
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE19 => --Write Data in DD RAM (d)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= d;
                    Next_State <= WRITE19;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE20;
    
                else

                    Next_State <= WRITE19;

                end if;

            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE20 => --Write Data in DD RAM (o)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= o; --DATA<=X"53";
                    Next_State <= WRITE20;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE21;

                else

                    Next_State <= WRITE20;

                end if;

            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE21 => --Write Data in DD RAM (r)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= r; --DATA<=X"57";
                    Next_State <= WRITE21;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE22;

                else

                    Next_State <= WRITE21;

                end if;
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE22 => --Write Data in DD RAM (space)
                
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space; 
                    Next_State <= WRITE22;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE23;

                else

                    Next_State <= WRITE22;

                end if;
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE23 => --Write Data in DD RAM (d)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= d;
                    Next_State <= WRITE23;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE24;

                else

                    Next_State <= WRITE23;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE24 => --Write Data in DD RAM (e)
                
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= e;
                    Next_State <= WRITE24;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE25;

                else

                    Next_State <= WRITE24;

                end if;

            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE25 => --Write Data in DD RAM (space)
                
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space;
                    Next_State <= WRITE25;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE26;

                else

                    Next_State <= WRITE25;

                end if;

            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE26 => --Write Data in DD RAM (alfa)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= alfa;
                    Next_State <= WRITE26;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= Delay0;

                else
                        
                    Next_State <= WRITE26;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when Delay0 => --Da espera de 5 segundos
            
                if CONT3 = "0101" then
                    
                    Next_State <= CLD2;
                
                else
                    
                    Next_State <= Delay0;
                    
                end if;
                
            RESET <= CONT3(0)and CONT3(3); -- CONT1 = 0

----- Limpiar la LCD ----

            when CLD2 => --CLEAR DISPLAY 0x01
            
                if CONT1 = X"0009C40" then -- estado de espera por 40us

                    READY <= '1';
                    LCD_RS <= '0';
                    DATA <= X"01"; --00000001
                    Next_State <= CLD2;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= RETH2;

                else

                    Next_State <= CLD2;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
----- Volver el cursor a Home ----

            when RETH2 => --RETURN CURSOR HOME
                if CONT1 = X"0009C40" then -- estado de espera por 40us

                    READY <= '1';
                    LCD_RS <= '0';
                    DATA <= X"02"; --0000001X
                    Next_State <= RETH2;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= SDDRAMA3;

                else

                    Next_State <= RETH2;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0            
            
---- Posicionarese en la columna 1 fila 1 ------

            when SDDRAMA3 => --SET DD RAM ADDRESS posici�n del display del rengl�n 1 columna 4

                if CONT1 = X"00280A0" then -- estado de espera por 1.64ms = 164.000

                    READY <= '1';
                    LCD_RS <= '0';
                    DATA <= X"80"; --1-AC6-AC0, 80(R=1,C=1) 84(R=1,C=5)
                    Next_State <= SDDRAMA3;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE27;

                else

                    Next_State <= SDDRAMA3;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0           

---- Mostrar men� ----

            when WRITE27 => --Write Data in DD RAM (M)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_M;
                    Next_State <= WRITE27;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE28;

                else
                        
                    Next_State <= WRITE27;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE28 => --Write Data in DD RAM (e)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= e;
                    Next_State <= WRITE28;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE29;

                else
                        
                    Next_State <= WRITE28;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE29 => --Write Data in DD RAM (n)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= n;
                    Next_State <= WRITE29;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE30;

                else
                        
                    Next_State <= WRITE29;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE30 => --Write Data in DD RAM (u)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= u;
                    Next_State <= WRITE30;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE31;

                else
                        
                    Next_State <= WRITE30;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE31 => --Write Data in DD RAM (space)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space;
                    Next_State <= WRITE31;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE32;

                else
                        
                    Next_State <= WRITE31;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE32 => --Write Data in DD RAM (1)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= uno;
                    Next_State <= WRITE32;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE33;

                else
                        
                    Next_State <= WRITE32;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE33 => --Write Data in DD RAM (:)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= dosptos;
                    Next_State <= WRITE33;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE34;

                else
                        
                    Next_State <= WRITE33;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE34 => --Write Data in DD RAM (T)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_T;
                    Next_State <= WRITE34;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE35;

                else
                        
                    Next_State <= WRITE34;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE35 => --Write Data in DD RAM (e)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= e;
                    Next_State <= WRITE35;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE36;

                else
                        
                    Next_State <= WRITE35;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE36 => --Write Data in DD RAM (m)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= m;
                    Next_State <= WRITE36;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE37;

                else
                        
                    Next_State <= WRITE36;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE37 => --Write Data in DD RAM (p)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= P;
                    Next_State <= WRITE37;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= SDDRAMA4;

                else
                        
                    Next_State <= WRITE37;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when SDDRAMA4 => --SET DD RAM ADDRESS posici�n del display rengl�n 2 columna 1

                if CONT1 = X"014050" then -- estado de espera por 1.64ms

                    READY <= '1';
                    LCD_RS <= '0';
                    DATA <= X"C5"; --1-AC6 - AC0, C0(R=2,C=1)
                    Next_State <= SDDRAMA4;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE38; --para brincar al segundo rengl�n

                else

                    Next_State <= SDDRAMA4;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE38 => --Write Data in DD RAM (2)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= dos;
                    Next_State <= WRITE38;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE39;

                else
                        
                    Next_State <= WRITE38;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE39 => --Write Data in DD RAM (:)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= dosptos;
                    Next_State <= WRITE39;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE40;

                else
                        
                    Next_State <= WRITE39;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE40 => --Write Data in DD RAM (A)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_A;
                    Next_State <= WRITE40;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE41;

                else
                        
                    Next_State <= WRITE40;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE41 => --Write Data in DD RAM (l)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= l;
                    Next_State <= WRITE41;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE42;

                else
                        
                    Next_State <= WRITE41;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE42 => --Write Data in DD RAM (i)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= i;
                    Next_State <= WRITE42;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE43;

                else
                        
                    Next_State <= WRITE42;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE43 => --Write Data in DD RAM (m)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= m;
                    Next_State <= WRITE43;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE44;

                else
                        
                    Next_State <= WRITE43;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE44 => --Write Data in DD RAM (e)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= e;
                    Next_State <= WRITE44;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE45;

                else
                        
                    Next_State <= WRITE44;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE45 => --Write Data in DD RAM (n)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= n;
                    Next_State <= WRITE45;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE46;

                else
                        
                    Next_State <= WRITE45;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE46 => --Write Data in DD RAM (t)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= t;
                    Next_State <= WRITE46;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE47;

                else
                        
                    Next_State <= WRITE46;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE47 => --Write Data in DD RAM (a)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= a;
                    Next_State <= WRITE47;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE48;

                else
                        
                    Next_State <= WRITE47;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE48 => --Write Data in DD RAM (r)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= r;
                    Next_State <= WRITE48;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    
                    if Control = "01" then
                    
                        Next_State <= CLD3;
                     
                    elsif Control = "10" then
                        
                        Next_State <= CLD4;
                        
                    else
                        
                        Next_State <= SDDRAMA5;
                        
                    end if;

                else
                        
                    Next_State <= WRITE48;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when SDDRAMA5 => --SET DD RAM ADDRESS posici�n del display rengl�n 2 columna 11

                if CONT1 = X"014050" then -- estado de espera por 1.64ms

                    READY <= '1';
                    LCD_RS <= '0';
                    DATA <= X"CF"; --1-AC6 - AC0, C0(R=2,C=1)
                    Next_State <= SDDRAMA5;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE48; --para brincar al segundo rengl�n

                else

                    Next_State <= SDDRAMA5;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
----- Pantalla de espera alimentandor opci�n temperatura----

----- Limpiar la LCD ----

            when CLD3 => --CLEAR DISPLAY 0x01
            
                if CONT1 = X"0009C40" then -- estado de espera por 40us

                    READY <= '1';
                    LCD_RS <= '0';
                    DATA <= X"01"; --00000001
                    Next_State <= CLD3;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= RETH3;

                else

                    Next_State <= CLD3;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
----- Volver el cursor a Home ----

            when RETH3 => --RETURN CURSOR HOME
                if CONT1 = X"0009C40" then -- estado de espera por 40us

                    READY <= '1';
                    LCD_RS <= '0';
                    DATA <= X"02"; --0000001X
                    Next_State <= RETH3;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= SDDRAMA6;

                else

                    Next_State <= RETH3;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0            
            
---- Posicionarese en la columna 1 fila 1 ------

            when SDDRAMA6 => --SET DD RAM ADDRESS posici�n del display del rengl�n 1 columna 4

                if CONT1 = X"00280A0" then -- estado de espera por 1.64ms = 164.000

                    READY <= '1';
                    LCD_RS <= '0';
                    DATA <= X"80"; --1-AC6-AC0, 80(R=1,C=1) 84(R=1,C=5)
                    Next_State <= SDDRAMA6;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE49;

                else

                    Next_State <= SDDRAMA6;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0                  

            when WRITE49 => --Write Data in DD RAM (L)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_L;
                    Next_State <= WRITE49;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE50;

                else
                        
                    Next_State <= WRITE49;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE50 => --Write Data in DD RAM (a)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= a;
                    Next_State <= WRITE50;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE51;

                else
                        
                    Next_State <= WRITE50;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE51 => --Write Data in DD RAM (space)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space;
                    Next_State <= WRITE51;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE52;

                else
                        
                    Next_State <= WRITE51;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE52 => --Write Data in DD RAM (t)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= t;
                    Next_State <= WRITE52;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE53;

                else
                        
                    Next_State <= WRITE52;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE53 => --Write Data in DD RAM (e)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= e;
                    Next_State <= WRITE53;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE54;

                else
                        
                    Next_State <= WRITE53;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE54 => --Write Data in DD RAM (m)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= m;
                    Next_State <= WRITE54;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE55;

                else
                        
                    Next_State <= WRITE54;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE55 => --Write Data in DD RAM (p)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= p;
                    Next_State <= WRITE55;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE56;

                else
                        
                    Next_State <= WRITE55;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE56 => --Write Data in DD RAM (e)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= e;
                    Next_State <= WRITE56;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE57;

                else
                        
                    Next_State <= WRITE56;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE57 => --Write Data in DD RAM (r)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= r;
                    Next_State <= WRITE57;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE58;

                else
                        
                    Next_State <= WRITE57;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE58 => --Write Data in DD RAM (a)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= a;
                    Next_State <= WRITE58;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE59;

                else
                        
                    Next_State <= WRITE58;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE59 => --Write Data in DD RAM (t)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= t;
                    Next_State <= WRITE59;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE60;

                else
                        
                    Next_State <= WRITE59;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE60 => --Write Data in DD RAM (u)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= u;
                    Next_State <= WRITE60;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE61;

                else
                        
                    Next_State <= WRITE60;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE61 => --Write Data in DD RAM (r)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= r;
                    Next_State <= WRITE61;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE62;

                else
                        
                    Next_State <= WRITE61;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE62 => --Write Data in DD RAM (a)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= a;
                    Next_State <= WRITE62;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= SDDRAMA7;

                else
                        
                    Next_State <= WRITE62;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when SDDRAMA7 => --SET DD RAM ADDRESS posici�n del display rengl�n 2 columna 1

                if CONT1 = X"014050" then -- estado de espera por 1.64ms

                    READY <= '1';
                    LCD_RS <= '0';
                    DATA <= X"C0"; --1-AC6 - AC0, C0(R=2,C=1)
                    Next_State <= SDDRAMA7;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE63; --para brincar al segundo rengl�n

                else

                    Next_State <= SDDRAMA7;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE63 => --Write Data in DD RAM (e)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= e;
                    Next_State <= WRITE63;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE64;

                else
                        
                    Next_State <= WRITE63;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE64 => --Write Data in DD RAM (s)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= s;
                    Next_State <= WRITE64;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE65;

                else
                        
                    Next_State <= WRITE64;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE65 => --Write Data in DD RAM (space)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space;
                    Next_State <= WRITE65;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE66;

                else
                        
                    Next_State <= WRITE65;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE66 => --Write Data in DD RAM (Temp_signo)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= sig_temp_sg;
                    Next_State <= WRITE66;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE67;

                else
                        
                    Next_State <= WRITE66;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE67 => --Write Data in DD RAM (Temp1_sg)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= temp1_sg;
                    Next_State <= WRITE67;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE68;

                else
                        
                    Next_State <= WRITE67;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE68 => --Write Data in DD RAM (Temp2_sg)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= temp2_sg;
                    Next_State <= WRITE68;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE69;

                else
                        
                    Next_State <= WRITE68;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE69 => --Write Data in DD RAM (.)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= punto;
                    Next_State <= WRITE69;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE70;

                else
                        
                    Next_State <= WRITE69;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE70 => --Write Data in DD RAM (Temp3_sg)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= temp3_sg;
                    Next_State <= WRITE70;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE71;

                else
                        
                    Next_State <= WRITE70;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE71 => --Write Data in DD RAM (�)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= grade;
                    Next_State <= WRITE71;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE72;
                    
                else
                        
                    Next_State <= WRITE71;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE72 => --Write Data in DD RAM (C)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_C;
                    Next_State <= WRITE72;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE73;
                    
                else
                        
                    Next_State <= WRITE72;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE73 => --Write Data in DD RAM (space)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space;
                    Next_State <= WRITE73;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE74;
                    
                else
                        
                    Next_State <= WRITE73;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE74 => --Write Data in DD RAM (space)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space;
                    Next_State <= WRITE74;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= Delay1;
                    
                else
                        
                    Next_State <= WRITE74;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when Delay1 => ---Da una espera de 5 segundos
            
                if CONT3 = "0101" then
                    
                    Next_State <= CLD2;
                                 
                else
                    
                    Next_State <= Delay1;
                    
                end if;
                
            RESET <= CONT3(0)and CONT3(3); -- CONT1 = 0

----- Pantalla de espera alimentandor opci�n alimentar----

----- Limpiar la LCD ----

            when CLD4 => --CLEAR DISPLAY 0x01
            
                if CONT1 = X"0009C40" then -- estado de espera por 40us

                    READY <= '1';
                    LCD_RS <= '0';
                    DATA <= X"01"; --00000001
                    Next_State <= CLD4;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= RETH4;

                else

                    Next_State <= CLD4;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
----- Volver el cursor a Home ----

            when RETH4 => --RETURN CURSOR HOME
                if CONT1 = X"0009C40" then -- estado de espera por 40us

                    READY <= '1';
                    LCD_RS <= '0';
                    DATA <= X"02"; --0000001X
                    Next_State <= RETH4;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= SDDRAMA8;

                else

                    Next_State <= RETH4;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0            
            
---- Posicionarese en la columna 1 fila 1 ------

            when SDDRAMA8 => --SET DD RAM ADDRESS posici�n del display del rengl�n 1 columna 4

                if CONT1 = X"00280A0" then -- estado de espera por 1.64ms = 164.000

                    READY <= '1';
                    LCD_RS <= '0';
                    DATA <= X"80"; --1-AC6-AC0, 80(R=1,C=1) 84(R=1,C=5)
                    Next_State <= SDDRAMA8;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE75;

                else

                    Next_State <= SDDRAMA8;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0                  

            when WRITE75 => --Write Data in DD RAM (A)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_A;
                    Next_State <= WRITE75;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE76;

                else
                        
                    Next_State <= WRITE75;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE76 => --Write Data in DD RAM (l)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= l;
                    Next_State <= WRITE76;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE77;

                else
                        
                    Next_State <= WRITE76;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE77 => --Write Data in DD RAM (i)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= i;
                    Next_State <= WRITE77;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE78;

                else
                        
                    Next_State <= WRITE77;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE78 => --Write Data in DD RAM (m)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= m;
                    Next_State <= WRITE78;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE79;

                else
                        
                    Next_State <= WRITE78;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE79 => --Write Data in DD RAM (e)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= e;
                    Next_State <= WRITE79;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE80;

                else
                        
                    Next_State <= WRITE79;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE80 => --Write Data in DD RAM (n)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= n;
                    Next_State <= WRITE80;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE81;

                else
                        
                    Next_State <= WRITE80;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE81 => --Write Data in DD RAM (t)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= t;
                    Next_State <= WRITE81;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE82;

                else
                        
                    Next_State <= WRITE81;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE82 => --Write Data in DD RAM (a)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= a;
                    Next_State <= WRITE82;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE83;

                else
                        
                    Next_State <= WRITE82;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE83 => --Write Data in DD RAM (n)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= n;
                    Next_State <= WRITE83;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE84;

                else
                        
                    Next_State <= WRITE83;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE84 => --Write Data in DD RAM (d)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= d;
                    Next_State <= WRITE84;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE85;

                else
                        
                    Next_State <= WRITE84;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE85 => --Write Data in DD RAM (o)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= o;
                    Next_State <= WRITE85;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE86;

                else
                        
                    Next_State <= WRITE85;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE86 => --Write Data in DD RAM (space)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space;
                    Next_State <= WRITE86;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE87;

                else
                        
                    Next_State <= WRITE86;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE87 => --Write Data in DD RAM (alfa)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= alfa;
                    Next_State <= WRITE87;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= SDDRAMA9;

                else
                        
                    Next_State <= WRITE87;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when SDDRAMA9 => --SET DD RAM ADDRESS posici�n del display rengl�n 2 columna 1

                if CONT1 = X"014050" then -- estado de espera por 1.64ms

                    READY <= '1';
                    LCD_RS <= '0';
                    DATA <= X"C0"; --1-AC6 - AC0, C0(R=2,C=1)
                    Next_State <= SDDRAMA9;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE88; --para brincar al segundo rengl�n

                else

                    Next_State <= SDDRAMA9;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE88 => --Write Data in DD RAM (1)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= uno;
                    Next_State <= WRITE88;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= Delay3;

                else
                        
                    Next_State <= WRITE88;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when Delay3 => ---Da una espera de 1 segundos
            
                if CONT3 = "0001" then
                    
                    Next_State <= WRITE89;
                
                else
                    
                    Next_State <= Delay3;
                    
                end if;
                
            RESET <= CONT3(0); -- CONT1 = 0            
            
            when WRITE89 => --Write Data in DD RAM (.)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= punto;
                    Next_State <= WRITE89;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= Delay4;

                else
                        
                    Next_State <= WRITE89;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when Delay4 => ---Da una espera de 1 segundos
            
                if CONT3 = "0001" then
                    
                    Next_State <= WRITE90;
                
                else
                    
                    Next_State <= Delay4;
                    
                end if;
                
            RESET <= CONT3(0); -- CONT1 = 0            
            
            when WRITE90 => --Write Data in DD RAM (.)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= punto;
                    Next_State <= WRITE90;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= Delay5;

                else
                        
                    Next_State <= WRITE90;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when Delay5 => ---Da una espera de 1 segundos
            
                if CONT3 = "0001" then
                    
                    Next_State <= WRITE91;
                
                else
                    
                    Next_State <= Delay5;
                    
                end if;
                
            RESET <= CONT3(0); -- CONT1 = 0            
            
            when WRITE91 => --Write Data in DD RAM (2)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= dos;
                    Next_State <= WRITE91;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= Delay6;

                else
                        
                    Next_State <= WRITE91;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when Delay6 => ---Da una espera de 1 segundos
            
                if CONT3 = "0001" then
                    
                    Next_State <= WRITE92;
                
                else
                    
                    Next_State <= Delay6;
                    
                end if;
                
            RESET <= CONT3(0); -- CONT1 = 0            
            
            when WRITE92 => --Write Data in DD RAM (.)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= punto;
                    Next_State <= WRITE92;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= Delay7;

                else
                        
                    Next_State <= WRITE92;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when Delay7 => ---Da una espera de 1 segundos
            
                if CONT3 = "0001" then
                    
                    Next_State <= WRITE93;
                
                else
                    
                    Next_State <= Delay7;
                    
                end if;
                
            RESET <= CONT3(0); -- CONT1 = 0            
            
            when WRITE93 => --Write Data in DD RAM (.)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= punto;
                    Next_State <= WRITE93;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= Delay8;

                else
                        
                    Next_State <= WRITE93;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when Delay8 => ---Da una espera de 1 segundos
            
                if CONT3 = "0001" then
                    
                    Next_State <= WRITE94;
                
                else
                    
                    Next_State <= Delay8;
                    
                end if;
                
            RESET <= CONT3(0); -- CONT1 = 0            
            
            when WRITE94 => --Write Data in DD RAM (L)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= M_L;
                    Next_State <= WRITE94;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE95;

                else
                        
                    Next_State <= WRITE94;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE95 => --Write Data in DD RAM (i)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= i;
                    Next_State <= WRITE95;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE96;

                else
                        
                    Next_State <= WRITE95;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE96 => --Write Data in DD RAM (s)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= s;
                    Next_State <= WRITE96;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE97;
                    
                else
                        
                    Next_State <= WRITE96;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE97 => --Write Data in DD RAM (t)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= t;
                    Next_State <= WRITE97;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE98;
                    
                else
                        
                    Next_State <= WRITE97;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE98 => --Write Data in DD RAM (o)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= o;
                    Next_State <= WRITE98;
     
                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= Delay2;
                    
                else
                        
                    Next_State <= WRITE98;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when Delay2 => ---Da una espera de 1 segundos
            
                if CONT3 = "0001" then
                    
                    Next_State <= CLD2;
                
                else
                    
                    Next_State <= Delay2;
                    
                end if;
                
            RESET <= CONT3(0); -- CONT1 = 0
            
        end case;
        
    end if;
end process; --FIN DEL PROCESO DE LA M�QUINA DE ESTADOS


process (TEMP(12))
begin
   case TEMP(12) is
      when '1' => sig_temp_sg <= sig_menos;
      when '0' => sig_temp_sg <= space;
      when others => sig_temp_sg <= x;
   end case;
end process;

process (temp1)
begin
   case temp1 is
      when "0000" => temp1_sg <= cero;
      when "0001" => temp1_sg <= uno;
      when "0010" => temp1_sg <= dos;
      when "0011" => temp1_sg <= tres;
      when "0100" => temp1_sg <= cuatro;
      when "0101" => temp1_sg <= cinco;
      when "0110" => temp1_sg <= seis;
      when "0111" => temp1_sg <= siete;
      when "1000" => temp1_sg <= ocho;
      when "1001" => temp1_sg <= nueve;
      when others => temp1_sg <= x;
   end case;
end process;

process (temp2)
begin
   case temp2 is
      when "0000" => temp2_sg <= cero;
      when "0001" => temp2_sg <= uno;
      when "0010" => temp2_sg <= dos;
      when "0011" => temp2_sg <= tres;
      when "0100" => temp2_sg <= cuatro;
      when "0101" => temp2_sg <= cinco;
      when "0110" => temp2_sg <= seis;
      when "0111" => temp2_sg <= siete;
      when "1000" => temp2_sg <= ocho;
      when "1001" => temp2_sg <= nueve;
      when others => temp2_sg <= x;
   end case;
end process;

process (temp3)
begin
   case temp3 is
      when "0000" => temp3_sg <= cero;
      when "0001" => temp3_sg <= uno;
      when "0010" => temp3_sg <= dos;
      when "0011" => temp3_sg <= tres;
      when "0100" => temp3_sg <= cuatro;
      when "0101" => temp3_sg <= cinco;
      when "0110" => temp3_sg <= seis;
      when "0111" => temp3_sg <= siete;
      when "1000" => temp3_sg <= ocho;
      when "1001" => temp3_sg <= nueve;
      when others => temp3_sg <= x;
   end case;
end process;

end;
