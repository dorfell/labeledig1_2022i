
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Completo is
Port ( clk : in std_logic;
       rst, start, dir : in std_logic;
       Sw: in std_logic_vector(2 downto 0);
       state: out std_logic_vector(3 downto 0)
      );
end Completo;

architecture Behavioral of Completo is
signal en: std_logic;

component Divisor is
port ( clk,rst :  in std_logic;
	      Sw : in std_logic_vector (2 downto 0);
         pulse : out std_logic);
end component;

component FSM is
port( clk, rst, en, dir :  in std_logic;
        start        :  in std_logic;
        state         : out std_logic_vector(3 downto 0));
end component;



begin
FSM1 : FSM port map(clk => clk, rst => rst, start => start, dir => dir, state => state, en => en);
Div: Divisor port map(clk => clk, rst => rst, Sw => Sw, pulse => en);


end Behavioral;
