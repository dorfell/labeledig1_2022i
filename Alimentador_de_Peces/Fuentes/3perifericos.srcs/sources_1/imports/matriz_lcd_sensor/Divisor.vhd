library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;


entity Divisor is
	port ( clk,rst :  in std_logic;
	      Sw : in std_logic_vector (2 downto 0);
         pulse : out std_logic);
end Divisor;

architecture Behavioral of Divisor is

component Multiplexor5a1 is
     port(Sw: in STD_LOGIC_VECTOR(2 downto 0);
         D0, D1, D2, D3, D4: in STD_LOGIC_VECTOR(28 downto 0);
         Y: out STD_LOGIC_VECTOR(28 downto 0));
end component;

signal dd, qq : std_logic_vector(28 downto 0):=(others=>'0');
signal ovf    : std_logic;
signal sig_mux1: STD_LOGIC_VECTOR(28 downto 0);
signal sig_mux2: STD_LOGIC_VECTOR(28 downto 0);
begin


mux_add  : 
       dd <= qq + 1  when ovf='0' else
            (others=>'0'); 

-- ? [Hz]
ovf_com  :				
             ovf <= '1' when qq = sig_mux1 else --Frecuencia
              '0';
              
MUL1: Multiplexor5a1 port map ( D0 => '0'&x"04C4B40", D1 => '0'&x"02625A0" , D2 => '0'&x"0196E6B" , D3 => '0'&x"01312D0", D4 => '0'&x"010F447",
                              Y => sig_mux1, Sw => Sw);
                              
MUL2: Multiplexor5a1 port map ( D0 => '0'&x"04C4B3F", D1 => '0'&x"026259F" , D2 => '0'&x"0196E6A" , D3 => '0'&x"01312CF", D4 => '0'&x"010F446",
                              Y => sig_mux2, Sw => Sw);

-- Flip-Flop
process (clk, rst)
begin  
   if rst = '1' then
      qq <= (others => '0');
   elsif (clk'event and clk = '1') then
      qq <= dd;
   else
      qq <= qq;
   end if;
end process;
		
-- output
		 pulse <= '1' when qq > sig_mux2 else -- Ancho del pulso
              '0';
end Behavioral;
