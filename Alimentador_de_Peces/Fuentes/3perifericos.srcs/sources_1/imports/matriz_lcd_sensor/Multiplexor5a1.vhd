library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity Multiplexor5a1 is
     port(Sw: in STD_LOGIC_VECTOR(2 downto 0);
         D0, D1, D2, D3, D4: in STD_LOGIC_VECTOR(28 downto 0);
         Y: out STD_LOGIC_VECTOR(28 downto 0));
end Multiplexor5a1;

architecture Behavioral of Multiplexor5a1 is

begin
    Y  <=  D0 when (Sw = "000") else
           D1 when (Sw = "001") else
           D2 when (Sw = "010") else
           D3 when (Sw = "011") else
           D4 when (Sw = "100") else
           (others => '0'); 

end Behavioral;
