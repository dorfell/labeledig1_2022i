----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.04.2022 13:38:03
-- Design Name: 
-- Module Name: Counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Counter is
    Port (
         clk,rst, ena :  in std_logic;
         count   : out std_logic_vector(3 downto 0) );
end Counter;

architecture Behavioral of Counter is

signal dd, qq : std_logic_vector(3 downto 0):=(others=>'0');
signal ovf: std_logic;

begin

mux_add  : 
       dd <= qq + 1  when ovf='0' else
            (others=>'0'); 

-- ? [Hz]
ovf_com  :				
		 ovf <= '1' when qq = "1001" else 
              '0';

-- Flip-Flop
process (clk, rst, ena)
begin  
   if rst = '1' then
      qq <= (others => '0');
   elsif (clk'event and clk = '1') then
    if ena = '1' then
        qq <= dd;
    else
      qq <= qq;
    end if;
   end if;
end process;
		
-- output
process (qq)
begin
    if qq < x"0001" then
        count <= "0000";
    elsif (qq < "0010") then
        count <= "0001";
    elsif (qq < "0011") then
        count <= "0010";
    elsif (qq < "0100") then
        count <= "0011";
    elsif (qq < "0101") then
        count <= "0100";
    elsif (qq < "0110") then
        count <= "0101";
    elsif (qq < "0111")  then
        count <= "0110";
    elsif (qq < "1000") then
        count <= "0111";
    elsif (qq < "1001") then
        count <= "1000";
    else
        count <= "1001";
    end if;      
end process;
                                                  
end Behavioral;

