----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.06.2022 20:28:19
-- Design Name: 
-- Module Name: Control_Maestro - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Control_Maestro is
 Port (CLOCK : in STD_LOGIC; --reloj de 100MHz );
    REINI : in STD_LOGIC; --boton de reinicio a BTNC
    TMP_SCL : inout STD_LOGIC; 
	TMP_SDA : inout STD_LOGIC;
    LCD_RS : out STD_LOGIC; --del LCD (JD1)
    LCD_RW : out STD_LOGIC; --read/write del LCD (JD2)
    LCD_E : out STD_LOGIC; --enable del LCD (JD3)
    DATA : out STD_LOGIC_VECTOR (7 downto 0); --bus de datos de la LCD (JC1-4,7-10)
    Control : in STD_LOGIC_VECTOR (1 downto 0); --Opci�n escogida en el men� de usuraio
    COLUMNAS: in std_logic_vector(2 downto 0);
    FILAS: out std_logic_vector(3 downto 0);
    tx: out std_logic; --Datos Bluethooth
    Sonido : out std_logic; --Sonnido Buzzer
    state : out std_logic_vector(3 downto 0));
end Control_Maestro;

architecture Behavioral of Control_Maestro is

component Enable is
Port (clk, rst : in std_logic;
       ena: out std_logic );
end component Enable;

component LCD_DATOS is
    Port (CLOCK : in STD_LOGIC; --reloj de 100MHz
    REINI : in STD_LOGIC; --boton de reinicio a BTNC   
    ENABLE : in STD_LOGIC; --Enable para 1 s de funcionamiento
    LCD_RS : out STD_LOGIC; --del LCD (JD1)
    LCD_RW : out STD_LOGIC; --read/write del LCD (JD2)
    LCD_E : out STD_LOGIC; --enable del LCD (JD3)
    TEMP : in std_logic_vector(12 downto 0); --Datos de temperatura 13 bit signo - 0-12 bits datos de temp
    DATA : out STD_LOGIC_VECTOR (7 downto 0); --bus de datos de la LCD (JC1-4,7-10)
    Control : in STD_LOGIC_VECTOR (1 downto 0) --Opci�n escogida en el men� de usuraio
    );
end component LCD_DATOS;

component Teclado is
   Port (
   Reloj: in std_logic;
   col: in std_logic_vector(2 downto 0);
   filas: out std_logic_vector(3 downto 0); 
   Segmentos: out std_logic_vector(6 downto 0));
end component Teclado;

component TempSensorCtl is
	Generic (CLOCKFREQ : natural := 100); -- input CLK frequency in MHz
	Port (
		TMP_SCL : inout STD_LOGIC;
		TMP_SDA : inout STD_LOGIC;
--		TMP_INT : in STD_LOGIC; -- Interrupt line from the ADT7420, not used in this project
--		TMP_CT : in STD_LOGIC;  -- Critical Temperature interrupt line from ADT7420, not used in this project
		
		TEMP_O : out STD_LOGIC_VECTOR(12 downto 0); --12-bit two's complement temperature with sign bit
		RDY_O : out STD_LOGIC;	--'1' when there is a valid temperature reading on TEMP_O
		ERR_O : out STD_LOGIC; --'1' if communication error
		
		CLK_I : in STD_LOGIC;
		SRST_I : in STD_LOGIC
	);
end component TempSensorCtl;

component Top is Port(
           clk : in std_logic;
           rst,start: in std_logic;
--           rd_uart: in std_logic;  -- Se puede dejar abierto
--           rx: in std_logic;   -- Se puede dejar abierto         
--           tx_full, rx_empty: out std_logic;   -- Se puede dejar abierto
--           r_data: out std_logic_vector(7 downto 0);   -- Se puede dejar abierto
           tx: out std_logic     
          );
end component Top;

component Buzzer_motor is
    Port (
    clk,rst,start :  in std_logic;
	pulse: out std_logic
     );
end component Buzzer_motor;

component Completo is
Port ( clk : in std_logic;
       rst, start, dir : in std_logic;
       Sw: in std_logic_vector(2 downto 0);
       state: out std_logic_vector(3 downto 0)
      );
end component Completo;

SIGNAL Control_sg : std_logic_vector(6 downto 0);
SIGNAL Control_sg_LCD : std_logic_vector(1 downto 0);
SIGNAL TEMP_sg, TEMP_RD_sg : std_logic_vector(12 downto 0);
SIGNAL enable_sg, Start_bl, Start_buzzer, Start_motor, Dir_motor : std_logic ;


signal Control_motor : std_logic_vector(2 downto 0);
signal Errortemp, ReadyTemp: std_logic;
constant derecha : std_logic := '1';
constant Velocidad : std_logic_vector(2 downto 0) := "010"; 

begin

process (Control_sg)
begin
   case Control_sg is
      when "0010010" =>
        Control_sg_LCD <= "10";
        Start_bl <= '1';
        Start_buzzer <= '1';
        Start_motor <= '1';
      when "1001111" => 
        Control_sg_LCD <= "01";
        Start_bl <= '0';
        Start_buzzer <= '0';
        Start_motor <= '0';
      when others =>
        Control_sg_LCD <= (others => '0');
        Start_bl <= '0';
        Start_buzzer <= '0';
        Start_motor <= '0';
   end case;
end process;

process (Errortemp)
begin
   if ReadyTemp = '1' then
   case Errortemp is
   
      when '1' => TEMP_RD_sg <= (others => '1');
      when '0' => TEMP_RD_sg <= TEMP_sg;
      
   end case;
   
   else
      TEMP_RD_sg <= (others => '1');
   end if;
   
end process;



    Enable0 : Enable port map(
    clk => CLOCK,
    rst => REINI,
    ena => enable_sg
    );
    
    Teclado0 : Teclado Port map (
    Reloj => CLOCK,
    col => COLUMNAS,
    filas => FILAS,
    Segmentos => Control_sg);

    LCD0 : LCD_DATOS Port map(
    CLOCK => CLOCK,
    REINI => REINI,
    ENABLE => enable_sg,
    LCD_RS => LCD_RS,
    LCD_RW => LCD_RW,
    LCD_E => LCD_E,
    TEMP => TEMP_RD_sg,
    DATA => DATA,
    Control => Control_sg_LCD);
    
    TempSensorCtl0 : TempSensorCtl Port map(
    TMP_SCL => TMP_SCL,
    TMP_SDA => TMP_SDA,
    TEMP_O => TEMP_sg,
    RDY_O => ReadyTemp,
    ERR_O => Errortemp,
    CLK_I => CLOCK,
    SRST_I => REINI
    );

    Bluethoot : Top Port map(
    clk => CLOCK,
    rst => REINI,
    start => Start_bl,
    tx => TX  
    );

    Buzzer_Motor0 : Buzzer_motor Port map(
    clk => CLOCK,
    rst => REINI,
    start => Start_Buzzer,
    pulse => Sonido
     );
 
    Motor : Completo Port map(
    clk => CLOCK,
    rst => REINI,
    start => Start_motor,
    dir => Dir_motor,
    Sw => Control_motor,
    state => state
    );
    
    Dir_motor <= Derecha;
    Control_motor <=  Velocidad;   
    
end Behavioral;
