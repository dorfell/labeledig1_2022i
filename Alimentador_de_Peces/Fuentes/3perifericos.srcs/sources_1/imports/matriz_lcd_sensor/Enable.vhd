----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.04.2022 21:08:04
-- Design Name: 
-- Module Name: Enable - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Enable is
Port (clk, rst : in std_logic;
       ena: out std_logic );
end Enable;

architecture Behavioral of Enable is

signal Countpresent, Countnext : std_logic_vector(28 downto 0):=(others=>'0');

begin

--Flip - Flop
process (clk, rst)
begin  
   if rst = '1' then
      Countpresent <= (others => '0');
   elsif (clk'event and clk = '1') then
      Countpresent <= Countnext;
   else
      Countpresent <= Countpresent;
   end if;
end process;

--Comparador 

process(Countpresent)
begin
    if Countpresent = x"5e69ec0" then
        ena <= '1'; Countnext <= (others =>'0');
    else
        ena <= '0'; Countnext <= Countpresent + 1;
    end if;
end process;

end Behavioral;
