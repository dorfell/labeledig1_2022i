
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.math_real.all;

entity Rectangular is
  Port (x_pos,y_pos: in integer;
  rgb_signal: out std_logic_vector(2 downto 0):="000";
  video_on: in std_logic);
end Rectangular;

architecture Behavioral of Rectangular is
constant Circle_Size:integer:=300;
signal Circle_on:std_logic:='0';
constant Rect_xsize:integer:=15;
constant Rect_ysize:integer:=100;
signal Rectangle_on:std_logic:='0';
signal Boca_on:std_logic:='0';
signal Boca_size:integer:=150;
signal Circulo_RGB:std_logic_vector(2 downto 0):="110";
signal Rectangulos_RGB:std_logic_vector(2 downto 0):="000";
signal Boca_RGB:std_logic_vector(2 downto 0):="100";
begin
--Circulo
Circle_on<='1' when (((x_pos-320)**2)+((y_pos-240)**2)<(Circle_Size/2)**2) else
    '0';

--Rectangulos
Rectangle_on<='1' when ((245<=x_pos)and(x_pos<245+Rect_xsize) and 
(235-Rect_ysize<=y_pos) and (y_pos<235)) or ((395<=x_pos)and(x_pos<395+Rect_xsize) and 
(235-Rect_ysize<=y_pos) and (y_pos<235)) else '0';    
--Boca
Boca_on<='1' when (245 < x_pos) and (x_pos < 395) and (250 < y_pos) and (y_pos < 325) and (((x_pos-320)**2)+((y_pos-250)**2)<(Boca_size/2)**2) else '0';

process(video_on,Circle_on,Rectangle_on, Boca_on,
Circulo_RGB,Rectangulos_RGB,Boca_RGB)
begin
if video_on='0' then
    rgb_signal<="000";
else
    if Rectangle_on = '1' then
        rgb_signal<=Rectangulos_RGB;
    elsif Boca_on ='1' then
        rgb_signal<=Boca_RGB;
    elsif Circle_on='1' then
        rgb_signal<=Circulo_RGB;
    else
        rgb_signal<="000";
    end if;
end if;
end process;
end Behavioral;
