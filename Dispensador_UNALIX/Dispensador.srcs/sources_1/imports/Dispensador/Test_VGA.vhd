library ieee;
use ieee.std_logic_1164.all ;
entity Test_VGA is
port(
clk, rst: in std_logic;
sw: in std_logic_vector ( 2 downto 0 ) ;
hsync,vsync : out std_logic;
rgb : out std_logic_vector ( 2 downto 0 )
);
end Test_VGA ;
architecture arch of Test_VGA is
signal rgb_reg,rgb_next: std_logic_vector ( 2 downto 0) ;
signal video_on: std_logic ;
signal x_pos: integer;
signal y_pos: integer;
-- i n s t a n t i a t e VGA s y n c c i r c u i t
component VGA is
  Port (clk,rst: in std_logic;
  h_sync,v_sync:out std_logic:='0';
  sync,video_on:inout std_logic;
  x_pos,y_pos: out integer
  );
end component;
component Rectangular is
  Port (x_pos,y_pos: in integer;
  rgb_signal: out std_logic_vector(2 downto 0):="000";
  video_on: in std_logic);
end component;
begin
vga_sync_unit : component VGA
port map(clk=>clk, rst=>rst , h_sync=>hsync ,
sync=>open, x_pos=>x_pos,y_pos=>y_pos,
v_sync=>vsync, video_on=>video_on);
cara: component Rectangular
port map(x_pos=>x_pos,y_pos=>y_pos,
video_on => video_on,rgb_signal => rgb_next);
-- r g b b u f f e r
process (clk , rst, sw)
begin 
if rst='1' then 
    rgb_reg<=(others=>'0');
elsif (clk'event and clk='1') then
    if sw = "001" or sw = "010" or sw = "011"  then
        rgb_reg<=rgb_next;
    
    else
        rgb_reg<=sw;
    end if;
end if;
end process;
rgb<= rgb_reg;
end arch;
