
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



entity VGA is
  Port (clk,rst: in std_logic;
  h_sync,v_sync:out std_logic:='0';
  sync,video_on:inout std_logic;
  x_pos,y_pos: out integer
  );
 
end VGA;

architecture Behavioral of VGA is
constant HD : integer :=640;-- horizontal display area
constant HF : integer :=16 ; --h. frontporch (rigth)
constant HB : integer :=48 ; --h. backporch (left)
constant HR : integer :=96 ; --h. retrace
constant VD : integer :=480; -- vertical display area
constant VF : integer :=10;-- v . frontporch (bottom)
constant VB : integer := 33; -- v . backporch (top)
constant VR : integer :=2; -- v . retrace
signal h_count : integer :=0;
signal v_count : integer :=0;
signal h_count2 : integer :=0;
signal v_count2 : integer :=0;
signal h_sync2 : std_logic :='0';
signal v_sync2 : std_logic :='0';
signal qq: integer:=0;
signal h_end,v_end: std_logic:='0';
begin
process (clk, rst)
begin  
   if rst = '1' then
      qq <= 0;
      h_count <= 0;
      v_count<= 0;
   elsif (clk'event and clk = '1') then
      qq <= qq+1;
      h_count<=h_count2;
      v_count<=v_count2;
      h_sync<=h_sync2;
      v_sync<=v_sync2;
   else
      qq <= qq;
   end if;
   if qq=4 then 
    qq<=0;
   end if;
end process;
process (sync, h_end, h_count)
begin  
if sync='1' then 
    if h_end='1' then  
        h_count2<=0;
    else 
        h_count2<=h_count+1;
    end if;
else 
    h_count2<=h_count;
end if;
end process;
process (sync, h_end, h_count, v_count)
begin  
if sync='1' and h_end='1' then 
    if v_end='1' then  
        v_count2<=0;
    else 
        v_count2<=v_count+1;
    end if;
else 
    v_count2<=v_count;
end if;

end process;
sync<='1' when qq = 3  else 
    '0'; 
h_end<='1' when h_count = (HD+HF+HB+HR-1) else
    '0';
v_end<='1' when v_count = (VD+VF+VB+VR-1) else
   '0';
h_sync2<='0' when h_count>=(HD+HF) and h_count<=(HD+HF+HR-1) else 
    '1';
v_sync2<='0' when v_count>=(VD+VF) and v_count <=(VD+VF+VR-1) else 
    '1';
video_on<='1' when (h_count<HD) and (v_count<VD) else '0';
x_pos<=h_count;
y_pos<=v_count;
end Behavioral;
