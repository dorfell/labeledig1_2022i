library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Completo is
  Port ( clk, rst, dir: in std_logic;
         state: out std_logic_vector(3 downto 0);
         pulse : out std_logic;
         hsync,vsync : out std_logic;
         rgb : out std_logic_vector ( 2 downto 0 );
         sw1 : in std_logic_vector ( 2 downto 0 ):= "000";
         RS 		  : OUT STD_LOGIC;						              
        RW		  : OUT STD_LOGIC;						               
        ENA 	  : OUT STD_LOGIC;							             
        DATA_LCD : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        col   : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
	filas 	  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
end Completo;

architecture Behavioral of Completo is

component Sonido is
port ( clk,rst, en :  in std_logic;
	      Sw : in std_logic_vector (2 downto 0);
         pulse : out std_logic);
end component;

component Motor is
Port ( clk : in std_logic;
       rst, start, dir: in std_logic;
       state: out std_logic_vector(3 downto 0));
end component;


component Test_VGA is
port(   clk, rst: in std_logic;
        sw: in std_logic_vector ( 2 downto 0 ) ;
        hsync,vsync : out std_logic;
        rgb : out std_logic_vector ( 2 downto 0 ));
end component;


component Teclado is
Port (
   Reloj: in std_logic;
   col: in std_logic_vector(2 downto 0);
   filas: out std_logic_vector(3 downto 0); 
   Segmentos: out std_logic_vector(2 downto 0));
end component;


component LIB_LCD_INTESC_REVD is
port(CLK: IN STD_LOGIC;
	  RS 		  : OUT STD_LOGIC;						
	  RW		  : OUT STD_LOGIC;						
	  ENA 	  : OUT STD_LOGIC;							
	  DATA_LCD : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	  Sw : IN STD_LOGIC_VECTOR(2 DOWNTO 0));
end component;

component Decodificador is
port ( Aa: in std_logic_vector(2 downto 0); 
     en : out std_logic); 
end component;

signal en: std_logic;
signal sw : std_logic_vector(2 downto 0);

begin

Motor1: Motor port map(clk => clk, rst => rst, dir => dir, state => state, start => en);
LCD1: LIB_LCD_INTESC_REVD port map(clk => clk, RS => RS, RW => RW, ENA => ENA, DATA_LCD => DATA_LCD, SW => sw);
Sonido1: Sonido port map(clk => clk, rst => rst, en => en, sw => sw1, pulse => pulse);
VGA1: Test_VGA port map(clk => clk, rst => rst, sw => sw, hsync => hsync, vsync => vsync, rgb => rgb);
Teclado1: Teclado port map(Reloj => clk, col => col, filas => filas, segmentos => sw);
Deco: Decodificador port map( Aa => sw, en => en);

end Behavioral;
