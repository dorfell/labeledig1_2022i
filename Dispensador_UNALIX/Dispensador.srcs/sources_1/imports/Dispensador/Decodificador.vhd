library ieee;
use ieee.std_logic_1164.all; 

entity Decodificador is
  port ( Aa: in std_logic_vector(2 downto 0); 
     en : out std_logic); 
end Decodificador;

architecture behaviour of Decodificador is 
begin
  process (Aa)
  begin
    case Aa is                 
      when "001" => en <= '1';
      when "010" => en <= '1';
      when "011" => en <= '1';
      WHEN OTHERS => en <= '0';        
    end case; 
  end process; 
end behaviour;