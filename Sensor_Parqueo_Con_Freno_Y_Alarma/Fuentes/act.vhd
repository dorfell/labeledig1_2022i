library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity act is
  port ( Columnas   :in  STD_LOGIC_VECTOR(3 DOWNTO 0);         
         aviso : in	std_logic;
         Filas 	  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0):= "0000";
         mDer,mIzq :  out std_logic_vector( 1 downto 0));
end act;

architecture Behavioral of act is


-- Component Declaration for the instances
------------------------------------------------------
component motor is
 port ( mover : in std_logic_vector( 2 downto 0);
         
         aviso :  in std_logic;
         mDer,mIzq :  out std_logic_vector( 1 downto 0));

end component motor;

component Teclado is
  Port (
	Columnas   : IN  STD_LOGIC_VECTOR(3 DOWNTO 0); 
	Filas 	  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0):= "0000";
	leds : OUT	STD_LOGIC_VECTOR(3 DOWNTO 0)
); 
end component Teclado;
signal sgmover : std_logic_vector( 3 downto 0);


begin 
--! Instantiate the submodules 
-----------------------------------------
mtor: motor
  port map(
     mover(0)   => not sgmover(0),
     mover(1)   => not sgmover(1),
     mover(2)   => not sgmover(2),
     aviso      => aviso,
     mDer       => mDer,
     mIzq       => mIzq );

Tclado: Teclado
  port map(
    Columnas => Columnas,
    Filas 	  => Filas,
    leds(0)  => sgmover(0),
    leds(1)  => sgmover(1),
    leds(2)  => sgmover(2),
    leds(3)  => sgmover(3));



end Behavioral;