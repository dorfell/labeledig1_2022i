library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity top_spi is
  port ( clk,rst :  in std_logic;
         mem_sel :  in std_logic_vector( 3 downto 0);
         sck, cs, mosi : out std_logic);
end top_spi;

architecture Behavioral of top_spi is


-- Component Declaration for the instances
------------------------------------------------------
component divisor_1kHz is
  port( 
    clk,rst :  in std_logic;
    en_sck  :  in std_logic; 
    sck     : out std_logic);
end component divisor_1kHz;

component divisor_enable is
  port(
    clk,rst :  in std_logic;
    en_spi  : out std_logic);
end component divisor_enable;

component fsm_as1107 is
  port(
    clk, rst      :  in std_logic;
    en_spi        :  in std_logic;
  
    start         : out std_logic; 
    start_spi     : out std_logic;
    mem_addr      : out std_logic_vector(7 downto 0) );
end component fsm_as1107;

component mem_as1107 is
  port(
    en_sel   :  in std_logic;
    mem_addr :  in std_logic_vector( 7 downto 0);
    mem_sel  :  in std_logic_vector( 3 downto 0);
    mem_data : out std_logic_vector(15 downto 0) );
end component mem_as1107;

component fsm_spi is
  port(
    clk, rst      :  in std_logic;
    start_spi     :  in std_logic;
    en_spi        :  in std_logic;
    data          :  in std_logic_vector(15 downto 0);

    cs, mosi      : out std_logic );
end component fsm_spi;

signal sg_start_spi, sg_en_spi, sg_en_sel  : std_logic;
signal sg_en_sck, sg_cs, sg_sck : std_logic;

signal sg_mem_addr : std_logic_vector( 7 downto 0);
signal sg_mem_data : std_logic_vector(15 downto 0);


begin 
--! Instantiate the submodules 
-----------------------------------------
SM0: divisor_1kHz
  port map(
    clk    => clk,
    rst    => rst,
    en_sck => '1',
          
    sck    => sg_sck );

SM1: divisor_enable
  port map(
    clk    => clk,
    rst    => rst,
     
    en_spi => sg_en_spi );

SM2: fsm_as1107
  port map(
    clk       => clk,
    rst       => rst,
    en_spi    => sg_en_spi,
    
    start     => sg_en_sel,
    start_spi => sg_start_spi,
    mem_addr  => sg_mem_addr );

SM3: mem_as1107
  port map(
    en_sel   => sg_en_sel,
    mem_addr => sg_mem_addr,
    mem_sel  => mem_sel,
    mem_data => sg_mem_data );

SM4: fsm_spi
  port map(
    clk       => clk,
    rst       => rst,
    start_spi => sg_start_spi,
    en_spi    => sg_en_spi,
    data      => sg_mem_data,
    
    cs        => sg_cs,
    mosi      => mosi );

-- SPI output
sck <= sg_sck when sg_cs = '0' else 
       '0';

cs <= sg_cs;

end Behavioral;