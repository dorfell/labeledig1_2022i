

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.ALL;
use IEEE.std_logic_arith.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity divisor is
 port(clk, reset : in std_logic;
      S1 : out std_logic);
end divisor;

architecture Behavioral of divisor is

signal ceros: std_logic_vector(20 downto 0):=(others =>'0');
signal dd,qq : std_logic_vector(28 downto 0):=(others =>'0');
signal overflow : std_logic;
signal periodo : std_logic_vector(28 downto 0):="00000000000000010000100000000";
signal anchoB  : std_logic_vector(28 downto 0):="00000000000000000000000000001";
begin

mux_add :
    dd<= qq + 1 when overflow ='0' else
    (others=>'0');

ovf_com:
overflow<='1' when qq=periodo else
'0';

out_com :
    S1<= '1' when qq<anchoB else
    '0';
--reg
process (clk,reset)
begin  
if reset ='1' then
   qq<= (others=>'0');
elsif (clk'event and clk ='1') then
   qq<=dd;
end if;

end process;


end Behavioral;
