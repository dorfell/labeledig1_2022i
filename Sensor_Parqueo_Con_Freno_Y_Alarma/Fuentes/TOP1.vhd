library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;


entity TOP1 is
    Port (     clk : in  STD_LOGIC;
         echo : in  STD_LOGIC;
         Columnas   :in  STD_LOGIC_VECTOR(3 DOWNTO 0);
         Filas 	  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
         Trigger : out  STD_LOGIC;
         sck, cs, mosi : out std_logic;
         enable, aviso : out std_logic;
         mDer,mIzq :  out std_logic_vector( 1 downto 0);
         display_out : out STD_LOGIC_VECTOR (6 downto 0);
         A : out STD_LOGIC_VECTOR (7 downto 0):="11111111");
end TOP1;

architecture Behavioral of TOP1 is
    COMPONENT HCRS04 is
        Port ( clk : in  STD_LOGIC;
             echo : in  STD_LOGIC;
             Trigger : out  STD_LOGIC;
             A : out STD_LOGIC_VECTOR (7 downto 0):="11111111";
             distance : out  STD_LOGIC_VECTOR (3 downto 0);
             display_out : out STD_LOGIC_VECTOR (6 downto 0));
    END COMPONENT;
    COMPONENT top_spi is
        port ( clk,rst :  in std_logic;
             mem_sel :  in std_logic_vector( 3 downto 0);
             sck, cs, mosi : out std_logic);
    END COMPONENT;
    COMPONENT act is
        port ( Columnas   :in  STD_LOGIC_VECTOR(3 DOWNTO 0);
             aviso : in std_logic;
             Filas 	  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0):= "0000";
             mDer,mIzq :  out std_logic_vector( 1 downto 0));
    END COMPONENT;
    COMPONENT bocina  is
        port(
            interruptor : in std_logic_vector(1 downto 0);
            distancia : in std_logic_vector(3 downto 0);
            distancia_out : out std_logic_vector(3 downto 0);
            enable,aviso : out std_logic);
    END COMPONENT;


    signal rst,sgaviso: std_logic :='0';
    signal distance, sgdistance : std_logic_vector( 3 downto 0);
    signal sg_mem_data : std_logic_vector(15 downto 0);


begin
    --! Instantiate the submodules 
    -----------------------------------------
    Matrizleds: top_spi
        port map(
            clk    => clk,
            rst    => rst,

            mem_sel =>  sgdistance,
            sck =>  sck,
            cs =>  cs,
            mosi  =>  mosi);
    Ultrasonido: HCRS04
        port map(
            clk    => clk,
            echo   => echo,
            Trigger     => Trigger,
            display_out => display_out,
            distance     => distance);
    teclado_motor: act
        port map(
            Columnas   => Columnas,
            Filas 	   => Filas,
            aviso     =>sgaviso,
            mDer      => mDer,
            mIzq      => mIzq);
    buzzer: bocina
        port map(
            distancia =>distance,
            distancia_out=>sgdistance,
            interruptor(0)   => '0',
            interruptor(1)   => '1',
            enable      => enable,
            aviso         => sgaviso);
aviso<=sgaviso;
end Behavioral;
