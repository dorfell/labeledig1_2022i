----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 31.03.2022 00:42:13
-- Design Name: 
-- Module Name: Lab3 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.ALL;
use IEEE.std_logic_arith.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bocina is
 port(
      interruptor : in std_logic_vector(1 downto 0);
      distancia : in std_logic_vector(3 downto 0);
      distancia_out : out std_logic_vector(3 downto 0);
      enable,aviso : out std_logic);
end bocina;

architecture Behavioral of bocina is

signal sgdistancia: std_logic_vector(3 downto 0):=(others =>'0');
signal sgaviso: std_logic;
begin
process (distancia)
begin  
  if (distancia >0) then
    sgdistancia <=distancia ;
  else
    sgdistancia <= sgdistancia;
  end if;
end process;
process (sgdistancia)
begin  
   if  (sgdistancia>4)then
            sgaviso<='0';
   else
            sgaviso<='1';
        end if;
end process;

aviso<=sgaviso;
enable<=sgaviso;
distancia_out<=sgdistancia;

end Behavioral;
