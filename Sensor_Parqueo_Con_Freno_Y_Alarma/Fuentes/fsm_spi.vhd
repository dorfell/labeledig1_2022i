
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity fsm_spi is
  port(
    clk, rst      :  in std_logic;
    start_spi     :  in std_logic;
    en_spi        :  in std_logic;
    data          :  in std_logic_vector(15 downto 0);
    
    cs, mosi      : out std_logic );
end fsm_spi;

architecture Behavioral of fsm_spi is

type states is (s00,s01,s02,s03,s04,s05,s06,s07,s08, s09,s10,s11,s12,s13,s14,s15,s16);
signal present_state, next_state: states;

begin

--===================
-- Present State
--===================
process(clk, rst)
  begin
  if rst='1' then
    present_state <= s00;
  elsif (clk'event and clk='1') then
    if en_spi='1' then
	   present_state <= next_state;
    else		
	   present_state <= present_state;
    end if;
  end if;
end process;

		
--===================
-- Next state logic
--===================
process(present_state, start_spi)
  begin 
  case present_state is 
    when s00 =>
      if (start_spi = '1') then
        next_state <= s01;
      else 
        next_state <= s00;
      end if;

    when s01 =>
      next_state <= s02;

    when s02 =>
      next_state <= s03;

    when s03 =>
      next_state <= s04;

    when s04 =>
      next_state <= s05;

    when s05 =>
      next_state <= s06;

    when s06 =>
      next_state <= s07;

    when s07 =>
      next_state <= s08;

    when s08 =>
      next_state <= s09;
    
    when s09 =>
      next_state <= s10;
      
    when s10 =>
      next_state <= s11;
      
    when s11 =>
      next_state <= s12;
      
    when s12 =>
      next_state <= s13;

    when s13 =>
      next_state <= s14;

    when s14 =>
      next_state <= s15;

    when s15 =>
      next_state <= s16;

    when s16 =>
      next_state <= s00;
    		
  end case;
end process;
	
	
--===================	
-- Output logic
--===================
process(present_state, data)
  begin
  case present_state is
    when s00 =>
      cs     <= '1';
      mosi   <= '0'; 

    when s01 =>
      cs     <= '0';
      mosi   <= data(15); 

    when s02 =>
      cs     <= '0';
      mosi   <= data(14); 

    when s03 =>
      cs     <= '0';
      mosi   <= data(13); 

    when s04 =>
      cs     <= '0';
      mosi   <= data(12);
       
    when s05 =>
      cs     <= '0';
      mosi   <= data(11); 

    when s06 =>
      cs     <= '0';
      mosi   <= data(10); 

    when s07 =>
      cs     <= '0';
      mosi   <= data(9); 

    when s08 =>
      cs     <= '0';
      mosi   <= data(8); 

    when s09 =>
      cs     <= '0';
      mosi   <= data(7); 

    when s10 =>
      cs     <= '0';
      mosi   <= data(6); 

    when s11 =>
      cs     <= '0';
      mosi   <= data(5); 

    when s12 =>
      cs     <= '0';
      mosi   <= data(4); 

    when s13 =>
      cs     <= '0';
      mosi   <= data(3);
       
    when s14 =>
      cs     <= '0';
      mosi   <= data(2); 

    when s15 =>
      cs     <= '0';
      mosi   <= data(1); 

    when s16 =>
      cs     <= '0';
      mosi   <= data(0); 
						
  end case;
end process;

		  
end Behavioral;

