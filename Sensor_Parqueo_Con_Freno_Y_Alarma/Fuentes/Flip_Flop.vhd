
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Flip_Flop is
port(clk : IN std_logic;
		reset : IN std_logic;
enable : IN std_logic;
      q: in std_logic_vector(19 downto 0);
      echo_count: out std_logic_vector(19 downto 0));
end Flip_Flop;


architecture Behavioral of Flip_Flop is

begin

--process(echo) begin
--		if falling_edge(echo) then
--			echo_count <= echo_counter1;
--		end if;
--end process;

process (reset, clk, enable) begin

	if reset = '1' then
		echo_count <= (others => '0');
	elsif clk'event and clk = '1' then
		if enable = '1' then
			echo_count <= q;
		end if;
	end if;
end process;

end Behavioral;

