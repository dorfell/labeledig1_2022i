library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity motor is
    port ( mover : in std_logic_vector( 2 downto 0);
         aviso :in std_logic;
         mDer,mIzq :  out std_logic_vector( 1 downto 0));

end motor;

architecture Behavioral of motor is

signal sgdistancia:  std_logic_vector( 3 downto 0);
begin

    process (mover,aviso)
    begin
            if  (mover(1) = '1')then
                mDer <= "00" ;
                mIzq <= "00" ;
            elsif (mover(0) = '1') then
                mDer <= "01" ;
                mIzq <= "01" ;
            elsif  (mover(2) = '1') and (aviso = '0') then
                mDer <= "10" ;
                mIzq <= "10" ;
            else
                mDer <= "00" ;
                mIzq <= "00" ;
            end if;

    end process;


end Behavioral;