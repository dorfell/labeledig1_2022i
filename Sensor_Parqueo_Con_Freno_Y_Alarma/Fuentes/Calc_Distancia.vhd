library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;


entity distance_calculation is
port(
echo_count : in STD_LOGIC_VECTOR (19 downto 0);
distance : out  STD_LOGIC_VECTOR (3 downto 0)
);
end distance_calculation;

architecture Behavioral of distance_calculation is

begin

Distance <=   "0000" when (echo_count < 4000) else --0cm
			  "0001" when (echo_count > 5000   and echo_count < 30000) else 
			  "0010" when (echo_count > 35000  and echo_count < 60000) else
			  "0011" when (echo_count > 65000  and echo_count < 90000) else
			  "0100" when (echo_count > 95000  and echo_count < 120000) else
			  "0101" when (echo_count > 125000 and echo_count < 150000) else
			  "0110" when (echo_count > 155000 and echo_count < 180000) else
			  "0111" when (echo_count > 185000 and echo_count < 210000) else
			  "1000" when (echo_count > 215000 and echo_count < 240000) else
			  "1001" when (echo_count > 245000 and echo_count < 270000) else
			  "1111" when (echo_count > 275000);

end Behavioral;