----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.06.2022 22:35:40
-- Design Name: 
-- Module Name: Teclado - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Teclado is
  Port (
	Columnas   : IN  STD_LOGIC_VECTOR(3 DOWNTO 0); 
	Filas 	  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0):= "0000";
	leds : OUT	STD_LOGIC_VECTOR(3 DOWNTO 0)
); 
end Teclado;

architecture Behavioral of Teclado is

begin
process(Columnas)
begin
for i in 0 to 3 loop
  if Columnas(i) = '1' then
    leds(i) <= '1';
  else
    leds(i) <= '0';
  end if;
  end loop;
end process;



end Behavioral;



