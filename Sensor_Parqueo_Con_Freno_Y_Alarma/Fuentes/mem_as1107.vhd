
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity mem_as1107 is
    port(
        mem_addr :  in std_logic_vector( 7 downto 0);
        en_sel   :  in std_logic;
        mem_sel  :  in std_logic_vector( 3 downto 0);
        mem_data : out std_logic_vector(15 downto 0) );
end mem_as1107;

architecture Behavioral of mem_as1107 is

    signal sg_mem_data : std_logic_vector(11 downto 0);
    signal sg_mem_sel  : std_logic_vector(3 downto 0):=(others=>'0');
    signal D1, D2, D3, D4 : std_logic_vector(7 downto 0);
    signal D5, D6, D7, D8 : std_logic_vector(7 downto 0);

begin
process (en_sel, sg_mem_sel)
begin  
  if (en_sel = '1') and (mem_sel>0) then
    sg_mem_sel <=mem_sel ;
  else
    sg_mem_sel <= sg_mem_sel;
  end if;
end process;
    -- Addr     data for AS1107.
    -- D11-D8 & D7:D0
    --Initialization
    sg_mem_data <= x"0" & "00000000" when mem_addr = x"00" else -- No-Op
                   x"1" & D1         when mem_addr = x"01" else -- Digit 0
                   x"2" & D2         when mem_addr = x"02" else -- Digit 1
                   x"3" & D3         when mem_addr = x"03" else -- Digit 2
                   x"4" & D4         when mem_addr = x"04" else -- Digit 3
                   x"5" & D5         when mem_addr = x"05" else -- Digit 4
                   x"6" & D6         when mem_addr = x"06" else -- Digit 5
                   x"7" & D7         when mem_addr = x"07" else -- Digit 6
                   x"8" & D8         when mem_addr = x"08" else -- Digit 7c
                   x"9" & "00000000" when mem_addr = x"09" else -- Decode mode: no decode.
                   x"A" & "00001111" when mem_addr = x"0A" else -- Intensity control: max on.
                   x"B" & "00000111" when mem_addr = x"0B" else -- Scan limit: Digits 0-7.
                   x"C" & "00000001" when mem_addr = x"0C" else -- Shutdown: Normal Op, Feature reg default.
                   x"F" & "00000000" when mem_addr = x"0D" else -- Display test: Normal Op.

                   x"0" & "00000000";

    mem_data <= "0000" & sg_mem_data;

    -- images 1. Carita enoja 2. Carita feli 3. Corazon 4. Triste
D1 <= x"3E"      when sg_mem_sel = x"00" else
          x"40"      when sg_mem_sel = x"01" else
          x"62"      when sg_mem_sel = x"02" else
          x"22"      when sg_mem_sel = x"03" else
          x"18"      when sg_mem_sel = x"04" else
          x"27"      when sg_mem_sel = x"05" else
          x"3C"      when sg_mem_sel = x"06" else
          x"03"      when sg_mem_sel = x"07" else
          x"36"      when sg_mem_sel = x"08" else
          x"06"      when sg_mem_sel = x"09" else
          "00000000";

    D2 <= x"7F"      when sg_mem_sel = x"00" else
          x"42"      when sg_mem_sel = x"01" else
          x"73"      when sg_mem_sel = x"02" else
          x"63"      when sg_mem_sel = x"03" else
          x"1C"      when sg_mem_sel = x"04" else
          x"67"      when sg_mem_sel = x"05" else
          x"7E"      when sg_mem_sel = x"06" else
          x"03"      when sg_mem_sel = x"07" else
          x"7F"      when sg_mem_sel = x"08" else
          x"4F"      when sg_mem_sel = x"09" else
          "00000000";

    D3 <= x"71"      when sg_mem_sel = x"00" else
          x"7F"      when sg_mem_sel = x"01" else
          x"59"      when sg_mem_sel = x"02" else
          x"49"      when sg_mem_sel = x"03" else
          x"16"      when sg_mem_sel = x"04" else
          x"45"      when sg_mem_sel = x"05" else
          x"4B"      when sg_mem_sel = x"06" else
          x"71"      when sg_mem_sel = x"07" else
          x"49"      when sg_mem_sel = x"08" else
          x"49"      when sg_mem_sel = x"09" else
          "00000000";

    D4 <= x"59"      when sg_mem_sel = x"00" else
          x"7F"      when sg_mem_sel = x"01" else
          x"49"      when sg_mem_sel = x"02" else
          x"49"      when sg_mem_sel = x"03" else
          x"53"      when sg_mem_sel = x"04" else
          x"45"      when sg_mem_sel = x"05" else
          x"49"      when sg_mem_sel = x"06" else
          x"79"      when sg_mem_sel = x"07" else
          x"49"      when sg_mem_sel = x"08" else
          x"69"      when sg_mem_sel = x"09" else
          "00000000";

    D5 <= x"4D"      when sg_mem_sel = x"00" else
          x"40"      when sg_mem_sel = x"01" else
          x"6F"      when sg_mem_sel = x"02" else
          x"7F"      when sg_mem_sel = x"03" else
          x"7F"      when sg_mem_sel = x"04" else
          x"7D"      when sg_mem_sel = x"05" else
          x"79"      when sg_mem_sel = x"06" else
          x"0F"      when sg_mem_sel = x"07" else
          x"7F"      when sg_mem_sel = x"08" else
          x"3F"      when sg_mem_sel = x"09" else
          "00000000";

    D6 <= x"7F"      when sg_mem_sel = x"00" else
          x"40"      when sg_mem_sel = x"01" else
          x"66"      when sg_mem_sel = x"02" else
          x"36"      when sg_mem_sel = x"03" else
          x"7F"      when sg_mem_sel = x"04" else
          x"39"      when sg_mem_sel = x"05" else
          x"30"      when sg_mem_sel = x"06" else
          x"07"      when sg_mem_sel = x"07" else
          x"36"      when sg_mem_sel = x"08" else
          x"1E"      when sg_mem_sel = x"09" else
          "00000000";

    D7 <= x"3E"      when sg_mem_sel = x"00" else
          x"00"      when sg_mem_sel = x"01" else
          x"00"      when sg_mem_sel = x"02" else
          x"00"      when sg_mem_sel = x"03" else
          x"50"      when sg_mem_sel = x"04" else
          x"00"      when sg_mem_sel = x"05" else
          x"00"      when sg_mem_sel = x"06" else
          x"00"      when sg_mem_sel = x"07" else
          x"00"      when sg_mem_sel = x"08" else
          x"00"      when sg_mem_sel = x"09" else
          "00000000";

    D8 <= "00000000";
end Behavioral;
--  { 0x"3E", 0x"7F", 0x"71", 0x"59", 0x"4D", 0x"7F", 0x"3E", 0x"00" }, // '0'
--  { 0x"40", 0x"42", 0x"7F", 0x"7F", 0x"40", 0x"40", 0x"00", 0x"00" }, // '1'
--  { 0x"62", 0x"73", 0x"59", 0x"49", 0x"6F", 0x"66", 0x"00", 0x"00" }, // '2'
--  { 0x"22", 0x"63", 0x"49", 0x"49", 0x"7F", 0x"36", 0x"00", 0x"00" }, // '3'
--  { 0x"18", 0x"1C", 0x"16", 0x"53", 0x"7F", 0x"7F", 0x"50", 0x"00" }, // '4'
--  { 0x"27", 0x"67", 0x"45", 0x"45", 0x"7D", 0x"39", 0x"00", 0x"00" }, // '5'
--  { 0x"3C", 0x"7E", 0x"4B", 0x"49", 0x"79", 0x"30", 0x"00", 0x"00" }, // '6'
--  { 0x"03", 0x"03", 0x"71", 0x"79", 0x"0F", 0x"07", 0x"00", 0x"00" }, // '7'
--  { 0x"36", 0x"7F", 0x"49", 0x"49", 0x"7F", 0x"36", 0x"00", 0x"00" }, // '8'
--  { 0x"06", 0x"4F", 0x"49", 0x"69", 0x"3F", 0x"1E", 0x"00", 0x"00" }, // '9'
