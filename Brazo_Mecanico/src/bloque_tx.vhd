----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.12.2021 02:10:46
-- Design Name: 
-- Module Name: bloque_tx - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bloque_tx is
  Port (clk,btn_wr,reset: in std_logic;
     wr_bloque: out std_logic
   );
end bloque_tx;

architecture arch of bloque_tx is

    begin
      
      debonce_circuit_unit: entity work.debounce_circuit(Behavioral)
           port map (clk=>clk, reset=>reset,
                    sw=>btn_wr,
        db_tick=>wr_bloque,db_level=>open
        );

end arch;
