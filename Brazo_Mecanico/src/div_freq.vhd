library IEEE;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_unsigned.all;
entity div_freq is
   port(
      -- Puertos de Entrada
      reset,clk : in std_logic;
      f: in std_logic_vector (5 downto 0):=(others=>'0');
      -- Pines de Salida
      Parlante: out std_logic := '0');
      
end div_freq;
architecture rtl of div_freq is
  signal temp: std_logic :='0';
  signal conta: std_logic_vector(20 downto 0):=(others=>'0');
  signal n1: std_logic_vector(19 downto 0):=(others=>'0');
  signal n2: std_logic_vector(19 downto 0):=(others=>'0');
begin
  process (reset,clk,f) is
   begin
   
   case f is
   when "000001" => --Do
    n1<= x"1750C";
    n2<=x"7493C";
    
   when "000010" =>--Re
    n1<= x"14C08";
    n2<=x"67C28";
 
   when "000100" =>--Mi
    n1<= x"127E6";
    n2<=x"5C77E";

   when "001000" =>--Fa
    n1<= x"1174C";
    n2<=x"5747C";

   when "010000" =>--Sol
    n1<= x"0F906";
    n2<=x"4DD1E";
    
   when "100000" =>--La
    n1<= x"0DDAE";
    n2<=x"45466";
    
   when others =>
    n1<= x"0DDAE";
    n2<=x"45466";
    temp <= '0';
    
 end case;
   
     if (reset= '1') then
        temp <= '0';
        conta <= (others=>'0');
       elsif(rising_edge (clk)) then
          if (conta = n1) then
            temp <= '1';
            conta <= conta + 1;
          elsif(conta = n2) then
            temp <= '0';
            conta <= (others=>'0');
          else
            conta <= conta + 1;
          end if;
     end if;
  end process;
   Parlante <= temp;
end rtl;