----------------------------------------------------------------------------------
-- COPYRIGHT 2019 Jes�s Eduardo M�ndez Rosales
--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.
--
--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.
--
--You should have received a copy of the GNU General Public License
--along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--
--							LIBRER�A LCD
--
-- Descripci�n: Con �sta librer�} podr�s implementar c�digos para una LCD 16x2 de manera
-- f�cil y r�pida, con todas las ventajas de utilizar una FPGA.
--
-- Caracter??ticas:
-- 
--	Los comandos que puedes utilizar son los siguientes:
--
-- LCD_INI() -> Inicializa la lcd.
--		 			 NOTA: Dentro de los par�ntesis poner un vector de 2 bits para encender o apagar
--					 		 el cursor y activar o desactivar el parpadeo.
--
--		"1x" -- Cursor ON
--		"0x" -- Cursor OFF
--		"x1" -- Parpadeo ON
--		"x0" -- Parpadeo OFF
--
--   Por ejemplo: LCD_INI("10") -- Inicializar LCD con cursor encendido y sin parpadeo 
--	
--			
-- CHAR() -> Manda una letra may�scula o min�scula
--
--				 IMPORTANTE: 1) Debido a que VHDL no es sensible a may�sculas y min�sculas, si se quiere
--								    escribir una letra may�scula se debe escribir una "M" antes de la letra.
--								 2) Si se quiere escribir la letra "S" may�scula, se declara "MAS"
--											
-- 	Por ejemplo: CHAR(A)  -- Escribe en la LCD la letra "a"
--						 CHAR(MA) -- Escribe en la LCD la letra "A"	
--						 CHAR(S)	 -- Escribe en la LCD la letra "s"
--						 CHAR(MAS)	 -- Escribe en la LCD la letra "S"	
--	
--
-- POS() -> Escribir en la posici�n que se indique.
--				NOTA: Dentro de los par�ntesis se dene poner la posici�n de la LCD a la que se quiere ir, empezando
--						por el rengl�n seguido de la posici�n vertical, ambos n�meros separados por una coma.
--		
--		Por ejemplo: POS(1,2) -- Manda cursor al rengl�n 1, poscici�n 2
--						 POS(2,4) -- Manda cursor al rengl�n 2, poscici�n 4		
--
--
-- CHAR_ASCII() -> Escribe un caracter a partir de su c�digo en ASCII
--						 NOTA: Dentro de los parentesis escribir x"(n�mero hex.)"
--
--		Por ejemplo: CHAR_ASCII(x"40") -- Escribe en la LCD el caracter "@"
--
--
-- CODIGO_FIN() -> Finaliza el c�digo. 
--						 NOTA: Dentro de los par�ntesis poner cualquier n�mero: 1,2,3,4...,8,9.
--
--
-- BUCLE_INI() -> Indica el inicio de un bucle. 
--						NOTA: Dentro de los par�ntesis poner cualquier n�mero: 1,2,3,4...,8,9.
--
--
-- BUCLE_FIN() -> Indica el final del bucle.
--						NOTA: Dentro de los par�ntesis poner cualquier n�mero: 1,2,3,4...,8,9.
--
--
-- INT_NUM() -> Escribe en la LCD un n�mero entero.
--					 NOTA: Dentro de los par�ntesis poner s�lo un n�mero que vaya del 0 al 9,
--						    si se quiere escribir otro n�mero entero se tiene que volver
--							 a llamar la funci�n
--
--
-- CREAR_CHAR() -> Funci�n que crea el caracter dise�ado previamente en "CARACTERES_ESPECIALES.vhd"
--                 NOTA: Dentro de los par�ntesis poner el nombre del caracter dibujado (CHAR1,CHAR2,CHAR3,..,CHAR8)
--								 
--
-- CHAR_CREADO() -> Escribe en la LCD el caracter creado por medio de la funci�n "CREAR_CHAR()"
--						  NOTA: Dentro de los par�ntesis poner el nombre del caracter creado.
--
--     Por ejemplo: 
--
--				Dentro de CARACTERES_ESPECIALES.vhd se dibujan los caracteres personalizados utilizando los vectores 
--				"CHAR_1", "CHAR_2","CHAR_3",...,"CHAR_7","CHAR_8"
--
--								 '1' => [#] - Se activa el pixel de la matr��.
--                       '0' => [ ] - Se desactiva el pixel de la matriz.
--
-- 			Si se quiere crear el				Entonces CHAR_1 queda de la siguiente
--				siguiente caracter:					manera:
--												
--				  1  2  3  4  5						CHAR_1 <=
--  		  1 [ ][ ][ ][ ][ ]							"00000"&			
-- 		  2 [ ][ ][ ][ ][ ]							"00000"&			  
-- 		  3 [ ][#][ ][#][ ]							"01010"&   		  
-- 		  4 [ ][ ][ ][ ][ ]		=====>			"00000"&			   
-- 		  5 [#][ ][ ][ ][#]							"10001"&          
-- 		  6 [ ][#][#][#][ ]							"01110"&			  
-- 		  7 [ ][ ][ ][ ][ ]							"00000"&			  
-- 		  8 [ ][ ][ ][ ][ ]							"00000";			
--
--		
--			Como el caracter se cre? en el vector "CHAR_1",entonces se escribe en las funci�nes como "CHAR1"
--			
--			CREAR_CHAR(CHAR1)  -- Crea el caracter personalizado (CHAR1)
--			CHAR_CREADO(CHAR1) -- Muestra en la LCD el caracter creado (CHAR1)		
--
-- 
--
-- LIMPIAR_PANTALLA() -> Manda a limpiar la LCD.
--								 NOTA: �sta funci�n se activa poniendo dentro de los par�ntesis
--										 un '1' y se desactiva con un '0'. 
--
--		Por ejemplo: LIMPIAR_PANTALLA('1') -- Limpiar pantalla est? activado.
--						 LIMPIAR_PANTALLA('0') -- Limpiar pantalla est? desactivado.
--
--
--	Con los puertos de entrada "CORD" y "CORI" se hacen corrimientos a la derecha y a la
--	izquierda respectivamente. NOTA: La velocidad del corrimiento se puede cambiar 
-- modificando la variable "DELAY_COR".
--
-- Algunas funci�nes generan un vector ("BLCD") cuando se termin? de ejecutar dicha funci�n y
--	que puede ser utilizado como una bandera, el vector solo dura un ciclo de instruccion.
--	   
--		LCD_INI() ---------- BLCD <= x"01"
--		CHAR() ------------- BLCD <= x"02"
--		POS() -------------- BLCD <= x"03"
-- 	INT_NUM() ---------- BLCD <= x"04"
--	   CHAR_ASCII() ------- BLCD <= x"05"
--	   BUCLE_INI() -------- BLCD <= x"06"
--		BUCLE_FIN() -------- BLCD <= x"07"
--		LIMPIAR_PANTALLA() - BLCD <= x"08"
--	   CREAR_CHAR() ------- BLCD <= x"09"
--	 	CHAR_CREADO() ------ BLCD <= x"0A"
--
--
--		�IMPORTANTE!
--		
--		1) Se deber? especificar el n�mero de instrucciones en la constante "NUM_INSTRUCCIONES". El valor 
--			de la �ltima instrucci�n es el que se colocar?
--		2) En caso de utilizar a la librer�} como TOP del dise�o, se deber? comentar el puerto gen�rico y 
--			descomentar la constante "FPGA_CLK" para especificar la frecuencia de reloj.
--		3) Cada funci�n se acompa�a con " INST(NUM) <= <FUNCI�N> " como lo muestra en el c�digo
-- 		demostrativo.
--
--
--                C�DIGO DEMOSTRATIVO
--
--		CONSTANT NUM_INSTRUCCIONES : INTEGER := 7;
--
-- 	INST(0) <= LCD_INI("11"); 		-- INICIALIZAMOS LCD, CURSOR A HOME, CURSOR ON, PARPADEO ON.
-- 	INST(1) <= POS(1,1);				-- EMPEZAMOS A ESCRIBIR EN LA LINEA 1, POSICI�N 1
-- 	INST(2) <= CHAR(MH);				-- ESCRIBIMOS EN LA LCD LA LETRA "h" MAYUSCULA
-- 	INST(3) <= CHAR(O);			
-- 	INST(4) <= CHAR(L);
-- 	INST(5) <= CHAR(A);
-- 	INST(6) <= CHAR_ASCII(x"21"); -- ESCRIBIMOS EL CARACTER "!"
-- 	INST(7) <= CODIGO_FIN(1);	   -- FINALIZAMOS EL CODIGO
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all ;
USE WORK.COMANDOS_LCD_REVD.ALL;

entity LIB_LCD_INTESC_REVD is

GENERIC(
			FPGA_CLK : INTEGER := 125_000_000
);

PORT(CLK: IN STD_LOGIC;

-----------------------------------------------------
------------------PUERTOS DE LA LCD------------------
	  RS 		  : OUT STD_LOGIC;							--
	  RW		  : OUT STD_LOGIC;							--
	  ENA 	  : OUT STD_LOGIC;							--
	  DATA_LCD : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);   --
-----------------------------------------------------
-----------------------------------------------------
	  	  
-----------------------------------------------------------
--------------ABAJO ESCRIBE TUS PUERTOS--------------------	
        
 --Del bluetooth       
        reset : in std_logic:='0';
        rx : in std_logic;
        btn_wr:in std_logic;
        tx : out std_logic;
        tx_full: out std_logic;
        bien: out std_logic;
        flag_5s: out std_logic;
        PWM: out std_logic;
        ECO: in std_logic;
        TRIGGER      : OUT STD_LOGIC;
        Parlante: out std_logic
                
-------------------------------------------------------------
-----------------------------------------------------------
	 );

end LIB_LCD_INTESC_REVD;

architecture Behavioral of LIB_LCD_INTESC_REVD is

CONSTANT NUM_INSTRUCCIONES : INTEGER := 25; 	--INDICAR EL N�MERO DE INSTRUCCIONES PARA LA LCD

CONSTANT ESCALA_1S : INTEGER := (FPGA_CLK)-1; --RECUERDAMEEEEEEEEEEEEEEEEEEEEEEEE/1000

--------------------------------------------------------------------------------
-------------------------SE�ALES DE LA LCD (NO BORRAR)--------------------------
																										--
component PROCESADOR_LCD_REVD is																--
																										--
GENERIC(																								--
			FPGA_CLK : INTEGER := 50_000_000;												--
			NUM_INST : INTEGER := 1																--
);																										--
																										--
PORT( CLK 				 : IN  STD_LOGIC;														--
	   VECTOR_MEM 		 : IN  STD_LOGIC_VECTOR(8  DOWNTO 0);							--
	   C1A,C2A,C3A,C4A : IN  STD_LOGIC_VECTOR(39 DOWNTO 0);							--
	   C5A,C6A,C7A,C8A : IN  STD_LOGIC_VECTOR(39 DOWNTO 0);
	   reinicio_dir		 : IN  STD_LOGIC;							--
	   RS 				 : OUT STD_LOGIC;														--
	   RW 				 : OUT STD_LOGIC;														--
	   ENA 				 : OUT STD_LOGIC;														--
	   BD_LCD 			 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);			         	--
	   DATA 				 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);							--
	   DIR_MEM 			 : OUT INTEGER RANGE 0 TO NUM_INSTRUCCIONES					--
	);																									--
																										--
end component PROCESADOR_LCD_REVD;															--
																										--
COMPONENT CARACTERES_ESPECIALES_REVD is													--
																										--
PORT( C1,C2,C3,C4 : OUT STD_LOGIC_VECTOR(39 DOWNTO 0);								--
		C5,C6,C7,C8 : OUT STD_LOGIC_VECTOR(39 DOWNTO 0)									--
	 );																								--
																										--
end COMPONENT CARACTERES_ESPECIALES_REVD;													--
																										--
CONSTANT CHAR1 : INTEGER := 1;																--
CONSTANT CHAR2 : INTEGER := 2;																--
CONSTANT CHAR3 : INTEGER := 3;																--
CONSTANT CHAR4 : INTEGER := 4;																--
CONSTANT CHAR5 : INTEGER := 5;																--
CONSTANT CHAR6 : INTEGER := 6;																--
CONSTANT CHAR7 : INTEGER := 7;																--
CONSTANT CHAR8 : INTEGER := 8;																--
																										--
type ram is array (0 to  NUM_INSTRUCCIONES) of std_logic_vector(8 downto 0); 	--
signal INST : ram := (others => (others => '0'));										--
																										--
signal blcd 			  : std_logic_vector(7 downto 0):= (others => '0');		--																										
signal vector_mem 	  : STD_LOGIC_VECTOR(8  DOWNTO 0) := (others => '0');		--
signal c1s,c2s,c3s,c4s : std_logic_vector(39 downto 0) := (others => '0');		--
signal c5s,c6s,c7s,c8s : std_logic_vector(39 downto 0) := (others => '0'); 	--
signal dir_mem 		  : integer range 0 to NUM_INSTRUCCIONES := 0;				--
																										--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
---------------------------AGREGA TUS SE�ALES AQU�------------------------------

component Bloque_grande is  
Port (clk, reset : in std_logic;
        btn_wr:in std_logic;
        rx: in std_logic;
        rd_uart: in std_logic;
        w_data: in std_logic_vector (7 downto 0);
        r_data: out std_logic_vector (7 downto 0);
        rx_empty: out std_logic;
        tx_full: out std_logic;
        tx: out std_logic
   );
end component Bloque_grande;

component Servo_controller is

    generic( Max: natural := 1500000);

    Port ( clk :  in  STD_LOGIC;--reloj de 50MHz

            selector :  in  STD_LOGIC_VECTOR (1 downto 0); --selecciona las 4 posiciones

            PWM :  out  STD_LOGIC);--terminal donde sale la se?al de PWM
end component Servo_controller;


component INTESC_LIB_ULTRASONICO_RevC is

generic( FPGA_CLK : INTEGER := 125_000_000 );

PORT( CLK          : IN  STD_LOGIC;
      ECO          : IN  STD_LOGIC;
      TRIGGER      : OUT STD_LOGIC;
      DATO_LISTO   : OUT STD_LOGIC;
      DISTANCIA_CM : OUT STD_LOGIC_VECTOR(8 DOWNTO 0)
     );
end component INTESC_LIB_ULTRASONICO_RevC;


component div_freq is
   port(
      reset,clk : in std_logic;
      f: in std_logic_vector (5 downto 0):=(others=>'0');
      Parlante: out std_logic := '0');
      
end component div_freq;


type state_type is (rst,usuario, contrasena, contra_incorrecta, menu_contador, alimentar,distancia,dinosaurio,cambiar_hora); --bluetooth !!! 
signal state_reg, state_next: state_type;

signal        rd_uart_s: std_logic;
signal        r_data_s: std_logic_vector (7 downto 0);
signal        w_data_s: std_logic_vector (7 downto 0):= (others => '0');
signal        rx_empty_s: std_logic;

signal        selector_s  : STD_LOGIC_VECTOR(1 downto 0);

signal dato_listo   : std_logic := '0';
signal distancia_cm : std_logic_vector(8 downto 0) := (others => '0');

signal distancia_reg, distancia_next : std_logic_vector(8 downto 0) := (others => '0');

signal f_s : std_logic_vector(5 downto 0) := (others => '0');

signal reinicio_dir_s: std_logic:='0';

signal pulso_reg, pulso_next : unsigned (2 downto 0):=(others=>'0');
signal conta5_reg, conta5_next : unsigned (31 downto 0):=(others=>'0');


signal cont_incor_reg, cont_incor_next : unsigned (31 downto 0):=(others=>'0');

--signal bien:integer:=0;
signal data_reg, data_next: std_logic_vector (31 downto 0):=(others=>'0');

signal hora_reg: std_logic_vector (23 downto 0):=x"303330"; 

signal hora_next: std_logic_vector (23 downto 0):=(others=>'0');
signal contrasena_usuario: std_logic_vector (31 downto 0):=(others=>'0');

signal conta_retardo_reg, conta_retardo_next : integer range 0 to escala_1s := 0;

signal unidades_reg : integer range 0 to 9 :=0;
signal unidades_next : integer range 0 to 9 :=0;
signal decenas_reg : integer range 0 to 5 :=3;
signal  decenas_next : integer range 0 to 5 :=0;
signal minutos_reg : integer range 0 to 9 :=0;
signal minutos_next : integer range 0 to 9 :=0;

signal nota_reg, nota_next : integer:=0;

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

begin

---------------------------------------------------------------
-------------------COMPONENTES PARA LCD------------------------
																				
u1: PROCESADOR_LCD_REVD													
GENERIC map( FPGA_CLK => FPGA_CLK,									 
				 NUM_INST => NUM_INSTRUCCIONES )						
																				
PORT map( CLK=>CLK,
        VECTOR_MEM=>VECTOR_MEM,
        C1A=>C1S,
        C2A=>C2S,
        C3A=>C3S,
        C4A=>C4S,
        C5A=>C5S,
        C6A=>C6S,
        C7A=>C7S,
        C8A=>C8S,
        RS=>RS, 
        RW=>RW,
        ENA=>ENA,
        BD_LCD=>BLCD,
        DATA=>DATA_LCD,
        DIR_MEM=>DIR_MEM,
        reinicio_dir=>reinicio_dir_s);
																							 
U2 : CARACTERES_ESPECIALES_REVD 										 
PORT MAP( C1=>C1S,
        C2=>C2S,
        C3=>C3S,
        C4=>C4S,
        C5=>C5S,
        C6=>C6S,
        C7=>C7S,
        C8=>C8S );				 		 
																				 
VECTOR_MEM <= INST(DIR_MEM);											 
---------------------------------------------------------------
---------------------------------------------------------------
U3 : Bloque_grande
Port map(
        clk=>clk,
        reset=>reset,
        btn_wr=>btn_wr,
        rx=>rx,
        w_data=>w_data_s, 
        r_data=>r_data_s,
        rd_uart=>rd_uart_s,
        rx_empty=>rx_empty_s,
        tx_full=>tx_full,
        tx=>tx
);


U4 : Servo_controller 
    Port map(
    clk=>clk,
    selector=>selector_s,
    PWM=>PWM
);


U5: INTESC_LIB_ULTRASONICO_RevC 
generic map( FPGA_CLK => 125_000_000 ) -- Reloj de FPGA opera a 50MHz
PORT map( CLK          => CLK, 
          ECO          => ECO, 
          TRIGGER      => TRIGGER, 
          DATO_LISTO   => dato_listo, 
          DISTANCIA_CM => distancia_cm 
         );
         
U6: div_freq
   port map(
      clk=>clk,
      reset=>reset,
      f=>f_s,
      Parlante=>Parlante
);

-------------------------------------------------------------------
---------------ESCRIBE TU C�DIGO PARA LA LCD-----------------------

-------------------------------------------------------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
--------------------ESCRIBE TU C�DIGO DE VHDL----------------------

    process (clk , reset)
    begin
        if reset='1' then
            state_reg <= rst;
            pulso_reg <=(others=> '0' );
            data_reg <= (others =>'0');
            hora_reg <= x"303330"; 
            unidades_reg <=0;
            decenas_reg<=3;
            minutos_reg<=0;
                       
            conta5_reg <= (others =>'0');
            nota_reg<=0; 
            cont_incor_reg <= (others =>'0');
            
            --distancia_reg <= (others =>'0');?
                        
        elsif (clk'event and clk='1') then
            state_reg <= state_next;
            data_reg <= data_next;
            hora_reg <= hora_next;
            conta5_reg <=conta5_next;
            nota_reg<=nota_next;
            cont_incor_reg <=cont_incor_next;
            pulso_reg <= pulso_next;
            
            conta_retardo_reg<=conta_retardo_next;
            unidades_reg <=unidades_next;
            decenas_reg <=decenas_next;
            minutos_reg <=minutos_next;
            if dato_listo = '1' then
                distancia_cm <= distancia_next;
            end if;
        end if;
     end process;

process (state_reg,pulso_reg, r_data_s, rx_empty_s, data_reg,conta5_reg,cont_incor_reg,nota_reg,
contrasena_usuario,conta_retardo_reg,conta_retardo_next,unidades_reg,unidades_next,
decenas_next,decenas_reg,minutos_reg, minutos_next,hora_reg,distancia_cm,dato_listo) --otra condicion?
    begin
       state_next <= state_reg;
       pulso_next<= pulso_reg;
       conta5_next<=conta5_reg;
       nota_next<=nota_reg;
       
       conta_retardo_next<=conta_retardo_reg;
       unidades_next <=unidades_reg;
       decenas_next <=decenas_reg;
       minutos_next <=minutos_reg;
       
       distancia_next <= distancia_cm;
       
       cont_incor_next<=cont_incor_reg;
       data_next<= data_reg;
       hora_next<= hora_reg;
       rd_uart_s <='0';
       reinicio_dir_s<='0';
       bien<='0';
       flag_5s<='0';
       case state_reg is
            
            when rst=>
            reinicio_dir_s<='1';
            INST(0) <= LIMPIAR_PANTALLA('1');
            state_next<=usuario;
            selector_s<="00";
            
            
            when usuario=>
            
            INST(0) <= LIMPIAR_PANTALLA('1');
            INST(1)  <= LCD_INI("11");
            INST(2)  <= CHAR(MU);
            INST(3)  <= CHAR(s);
            INST(4)  <= CHAR(u);
            INST(5)  <= CHAR(a);
            INST(6)  <= CHAR(r);
            INST(7)  <= CHAR(i);
            INST(8)  <= CHAR(o);
            INST(9)  <= CHAR_ASCII(x"3A");
            INST(10)  <= POS(2,1);
            INST(11)  <= INT_NUM(1);
            INST(12)  <= CHAR_ASCII(x"29");
            INST(13)  <= CHAR(MP);
            INST(14)  <= CHAR(i);
            INST(15)  <= CHAR(p);
            INST(16)  <= CHAR(e);
            INST(17)  <= CHAR_ASCII(x"20");
            INST(18)  <= INT_NUM(2);
            INST(19)  <= CHAR_ASCII(x"29");
            INST(20)  <= CHAR(MD);
            INST(21)  <= CHAR(i);
            INST(22)  <= CHAR(a);
            INST(23)  <= CHAR(n);
            INST(24)  <= CHAR(a);
            
            if (rx_empty_s='0') then --ojitooo
                rd_uart_s<='1';
                if (r_data_s=x"31") then
                    contrasena_usuario<=x"34333231";
                    state_next<=contrasena;
                    reinicio_dir_s<='1';
                    rd_uart_s<='1';
                elsif (r_data_s=x"32") then
                    contrasena_usuario<=x"31313131";
                    state_next<=contrasena;
                    reinicio_dir_s<='1';
                    rd_uart_s<='1';
                else
                    state_next<=usuario;
                    reinicio_dir_s<='1';
                    rd_uart_s<='1'; 
                end if; 
        
            end if;
                          
            when contrasena=>

            INST(0) <= LIMPIAR_PANTALLA('1');
            INST(1)  <= LCD_INI("00");
            INST(2)  <= CHAR(MC);
            INST(3)  <= CHAR(o);
            INST(4)  <= CHAR(n);
            INST(5)  <= CHAR(t);
            INST(6)  <= CHAR(r);
            INST(7)  <= CHAR(a);
            INST(8)  <= CHAR(s);
            INST(9)  <= CHAR(e);
            INST(10)  <= CHAR(n);  --No hay ? 
            INST(11)  <= CHAR(a);
            INST(12)  <= CHAR_ASCII(x"3A");
            INST(13) <= CHAR_ASCII(x"20");
            INST(15)<= CHAR_ASCII(x"20");
            INST(16)<= CHAR_ASCII(x"20");
            INST(17)<= CHAR_ASCII(x"20");
            INST(18)<= CHAR_ASCII(x"20");
            INST(19)<= POS(2,1);
            INST(20)<= CHAR_ASCII(x"20");
            INST(21)<= CHAR_ASCII(x"20");
            INST(22)<= CHAR_ASCII(x"20");
            INST(23)<= POS(1,13);
            INST(24)<= CHAR_ASCII(x"20");
--            INST(13)  <= POS(2,1);
            
            if (rx_empty_s='0') then --ojitooo
                if (pulso_reg=5) then
                    if (data_reg=contrasena_usuario) then
                        state_next <= menu_contador;

                        reinicio_dir_s<='1';
                        pulso_next <= (others => '0');
                        bien<='1';                      
                        rd_uart_s<='1';
                    else
                    	pulso_next <="001";
                        state_next <= contra_incorrecta;

                        bien<='0';
                        reinicio_dir_s<='1';
                        rd_uart_s<='1';
                    end if;
                else
                    rd_uart_s<='1';
                    if (r_data_s /= x"0A") then
                        pulso_next<= pulso_reg + 1;
                        if (pulso_reg>=1) then
                            data_next <= r_data_s & data_reg (31 downto 8);
                        end if;
                    end if;
                    
                end if;
            
            end if;
            
            when contra_incorrecta=>
            
            INST(0) <= LIMPIAR_PANTALLA('1');
            INST(1)  <= LCD_INI("11");
            INST(2)  <= CHAR(MC);
            INST(3)  <= CHAR(o);
            INST(4)  <= CHAR(n);
            INST(5)  <= CHAR(t);
            INST(6)  <= CHAR(r);
            INST(7)  <= CHAR(a);
            INST(8)  <= CHAR(s);
            INST(9)  <= CHAR(e);
            INST(10)  <= CHAR(n); 
            INST(11)  <= CHAR(a);
            INST(12)  <= POS(2,1);
            INST(13)  <= CHAR(MI);
            INST(14)  <= CHAR(n);
            INST(15)  <= CHAR(c);
            INST(16)  <= CHAR(o);
            INST(17)  <= CHAR(r);
            INST(18)  <= CHAR(r);
            INST(19)  <= CHAR(e);
            INST(20)  <= CHAR(c);
            INST(21)  <= CHAR(t);
            INST(22)  <= CHAR(a);
            INST(23)  <= CHAR_ASCII(x"20");
            INST(24)  <= CHAR(MX);
            
            if (cont_incor_reg=x"2540BE40") then -- 5ms 00098968   aca 35milis 0042C1D8 --> x"2540BE40" 5s RECUERDAMEEEEEEEEEEEEEEEEEEEEEEEE
                    state_next<=contrasena;
                    cont_incor_next<= (others => '0');
                    reinicio_dir_s<='1';
                else
                    cont_incor_next<= cont_incor_reg + 1;
               end if;
            
            
            when menu_contador =>
            	                       	          	
            	INST(0) <= LIMPIAR_PANTALLA('1');
                INST(1)  <= LCD_INI("00");
                INST(2)  <= CHAR(MA);
                INST(3)  <= CHAR(l);
                INST(4)  <= CHAR(i);
                INST(5)  <= CHAR(m);
                INST(6)  <= CHAR(e);
                INST(7)  <= CHAR(n);
                INST(8)  <= CHAR(t);
                INST(9)  <= CHAR(a);
                INST(10)  <= CHAR_ASCII(x"20");
                INST(11)  <= CHAR(e);
                INST(12)  <= CHAR(n);
                INST(13)  <= CHAR_ASCII(x"3A");              
                INST(14) <= BUCLE_INI(1);
                INST(15) <= POS(2,7);
                INST(16) <= INT_NUM(minutos_reg);
                INST(17)  <= CHAR_ASCII(x"3A"); 
                INST(18) <= INT_NUM(decenas_reg);
                INST(19) <= INT_NUM(unidades_reg);
                INST(20) <= BUCLE_FIN(1);
                INST(21)  <= CHAR_ASCII(x"20");
                INST(22)  <= CHAR_ASCII(x"20");
                INST(23)  <= CHAR_ASCII(x"20");
                INST(24)  <= CHAR_ASCII(x"20");

               
               if conta_retardo_reg = escala_1s then
                    conta_retardo_next <= 0;
                    unidades_next <= unidades_reg-1;
                    if unidades_reg = 0 then
                        unidades_next <= 9;
                        decenas_next <= decenas_reg -1; --09
                        if (decenas_reg = 0)  then --and (unidades =0)
                            decenas_next <= 5;
                            minutos_next <= minutos_reg-1;
                                if (minutos_reg =0) then
                                    state_next<=alimentar;
                                    reinicio_dir_s<='1';
                                end if;
                        end if;
                    end if;
               else
                    conta_retardo_next <= conta_retardo_reg+1;
               end if;
               
               
               if (rx_empty_s='0') then --ojitooo
                rd_uart_s<='1';
                if (r_data_s=x"61") then
                    state_next<=alimentar;
                    reinicio_dir_s<='1';
                    rd_uart_s<='1';
                elsif (r_data_s=x"63") then
                    state_next<=cambiar_hora;
                    reinicio_dir_s<='1';
                    rd_uart_s<='1';
                else
                    rd_uart_s<='1';  --OJITOOO
                end if;     
                        
            end if;
               
                
            when alimentar=>
                
                INST(0) <= LIMPIAR_PANTALLA('1');
                INST(1)  <= LCD_INI("00");
                INST(2)  <= CHAR(MA);
                INST(3)  <= CHAR(l);
                INST(4)  <= CHAR(i);
                INST(5)  <= CHAR(m);
                INST(6)  <= CHAR(e);
                INST(7)  <= CHAR(n);
                INST(8)  <= CHAR(t);
                INST(9)  <= CHAR(a);
                INST(10)  <= CHAR(n);
                INST(11)  <= CHAR(d);
                INST(12)  <= CHAR(o);
                INST(13)  <= CHAR_ASCII(x"2E");
                INST(14)  <= CHAR_ASCII(x"2E");
                INST(15)  <= CHAR_ASCII(x"2E");
                INST(16)<= CHAR_ASCII(x"20");
                INST(17)<= CHAR_ASCII(x"20");
                INST(18) <= POS(2,1);
                INST(19)<= CHAR_ASCII(x"20");
                INST(20)<= CHAR_ASCII(x"20");
                INST(21)<= CHAR_ASCII(x"20");
                INST(22)<= CHAR_ASCII(x"20");
                INST(23)<= CHAR_ASCII(x"20");
                INST(24)<= CHAR_ASCII(x"20");
              
              
               if (conta5_reg=x"165A0BC0") then -- aca 3s 165A0BC0--aca 30milis 001312D0 --> x"2540BE40" 5s RECUERDAMEEEEEEEEEEEEEEEEEEEEEEEE
                    flag_5s<='1';
                   unidades_next<=to_integer(unsigned(hora_reg(19 downto 16)));
                   decenas_next<=to_integer(unsigned(hora_reg(11 downto 8)));
                   minutos_next<=to_integer(unsigned(hora_reg(3 downto 0)));
                    selector_s<="00";
                    state_next<=distancia;
                    reinicio_dir_s<='1';
                    conta5_next <= (others => '0');
                else
                    conta5_next<= conta5_reg + 1;
                    selector_s<="10";         
               end if;
              
            when distancia=> ---Demora en sacar el dato correcto?
--                if dato_listo = '1' then
                    bien<='1';
                    if distancia_cm > "000010011" then -- 19cm en binario en 9 bits
                        state_next<=dinosaurio;
                        reinicio_dir_s<='1';
                    else
                        state_next<=menu_contador;
                        reinicio_dir_s<='1';
                    end if;
--                else
--                    state_next<=usuario;
--                        reinicio_dir_s<='1';
--                end if;
             
            when dinosaurio=>
                INST(0)  <= LCD_INI("00");
                INST(1)  <= CREAR_CHAR(1);
                INST(2)  <= CREAR_CHAR(2);
                INST(3)  <= CREAR_CHAR(3);
                INST(4)  <= CREAR_CHAR(4);
                INST(5)  <= CREAR_CHAR(5);
                INST(6)  <= CREAR_CHAR(6);
                INST(7)  <= POS(1,1);
                INST(8)  <= CHAR_CREADO(1);
                INST(9)  <= CHAR_CREADO(2);
                INST(10) <= CHAR_CREADO(3);
                INST(11)<= CHAR_ASCII(x"20");
--                INST(12)<= CHAR_ASCII(x"20");
                INST(12)  <= CHAR(MR);
                INST(13)  <= CHAR(e);
                INST(14)  <= CHAR(l);
                INST(15)  <= CHAR(l);
                INST(16)  <= CHAR(e);
                INST(17)  <= CHAR(n);
                INST(18)  <= CHAR(a);
                INST(19)  <= CHAR(r);              
                INST(20) <= POS(2,1);
                INST(21) <= CHAR_CREADO(4);
                INST(22) <= CHAR_CREADO(5);
                INST(23) <= CHAR_CREADO(6);
--                INST(24) <= CODIGO_FIN(1);
             
                if (conta5_reg=x"07735940") then --aca 30milis 001312D0 --> x"07735940" 1s RECUERDAMEEEEEEEEEEEEEEEEEEEEEEEE
                    nota_next<=nota_reg+1;
                    conta5_next <= (others => '0');
                else
                    conta5_next<= conta5_reg + 1;      
               end if;
             
               case nota_reg is
                    when 1=>
                        f_s<="000100";
                    
                    when 2=>
                        f_s<="000010";
                        
                    when 3=>
                        f_s<="000001";
                        
                    when 4=>
                        f_s<="000100";
                    
                    when 5=>
                        f_s<="000010";
                        
                    when 6=>
                        f_s<="000001";
                        
                    when others=>
                        f_s<="000000"; 
               end case;
               
                
            when cambiar_hora=>
                
                INST(0) <= LIMPIAR_PANTALLA('1');
            	INST(1)  <= LCD_INI("00");
                INST(2)  <= CHAR(MI);
                INST(3)  <= CHAR(n);
                INST(4)  <= CHAR(g);
                INST(5)  <= CHAR(r);
                INST(6)  <= CHAR(e);
                INST(7)  <= CHAR(s);
                INST(8)  <= CHAR(e);
                INST(9)  <= POS(2,1);
                INST(10)  <= CHAR(n);
                INST(11)  <= CHAR(u);
                INST(12)  <= CHAR(e);
                INST(13)  <= CHAR(v);
                INST(14)  <= CHAR(a);
                INST(15)  <= CHAR_ASCII(x"20");
                INST(16)  <= CHAR(h);
                INST(17)  <= CHAR(o);
                INST(18)  <= CHAR(r);
                INST(19)  <= CHAR(a);
            INST(20) <= CHAR_ASCII(x"20");
            INST(21)<= CHAR_ASCII(x"20");
            INST(22)<= CHAR_ASCII(x"20");
            INST(23)<= CHAR_ASCII(x"20");
            INST(24)<= CHAR_ASCII(x"20");
                
            if (rx_empty_s='0') then --ojitooo
                if (pulso_reg=4) then
                        state_next <= menu_contador;
                        
                       unidades_next<=to_integer(unsigned(hora_reg(19 downto 16)));
                       decenas_next<=to_integer(unsigned(hora_reg(11 downto 8)));
                       minutos_next<=to_integer(unsigned(hora_reg(3 downto 0)));
                        
                        reinicio_dir_s<='1';
                        pulso_next <= (others => '0');
                        rd_uart_s<='1';
                else
                    rd_uart_s<='1';
                    if (r_data_s /= x"0A") then
                        pulso_next<= pulso_reg + 1;
                        if (pulso_reg>=1) then
                            hora_next <= r_data_s & hora_reg (23 downto 8);
                        end if;
                    end if;
                end if;
            end if;
                                
        end case;
     end process;


--Asignar contrase?a por cada usuario
--Asignar valor para el contador cuando se cambia la hora y se termina de alimentar
--Opcion de cerrar sesion?
--En cada mensaje toca limpiar la pantalla?     

-------------------------------------------------------------------
-------------------------------------------------------------------

end Behavioral;

