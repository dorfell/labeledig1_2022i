----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06.12.2021 16:50:29
-- Design Name: 
-- Module Name: Baudrate - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;

use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity baudrate is
    Port (
 -- Puertos de Entrada
      reset,clk : in std_logic;
 -- Pines de Salida
      baudrate_tick : out std_logic :='0'
    );
end baudrate;



architecture arch of baudrate is
signal temp: std_logic :='0';
signal conta: std_logic_vector(20 downto 0):=(others=>'0');

begin
  process (reset,clk) is
   begin

     if (reset= '1') then
        temp <= '0';
        conta <= (others=>'0');
     elsif(rising_edge (clk)) then
         if (conta=x"32E") then --814 --DVSR 125M/(16 * baud rate)  --baudrate=9600
            temp<='1';
            conta <= (others=>'0');
         else
            temp <='0';
            conta <= conta + 1;
         end if;
      end if;
end process;
     baudrate_tick <= temp;
end arch;
