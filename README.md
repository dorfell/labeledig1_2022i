# labeledig1_2022I

Projects developed in the digital electronic lab. Hardware Platforms: Nexys 4, 4DDR, A7 and Zybo-Z7. Tools: Vivado. Period: 2022-I.

## Abatidor de Alimentos
- Diego Naranjo: dnaranjo@unal.edu.co
- Juan Tabares:  jtabarest@unal.edu.co
- https://www.youtube.com/watch?v=iQBupF9fw_E


## Alimentador de mascotas
- Sharon Jirsa:   sjirsag@unal.edu.co
- Santiago Gomez: sagomezpe@unal.edu.co
- https://www.youtube.com/shorts/wCP-pDW-pHQ


## Alimentador de Peces
- Juan Daleman:     jdaleman@unal.edu.co
- Gabriel Manrique: gmanriqueo@unal.edu.co
- Seykarim Mestre:  srmestrez@unal.edu.co
- https://www.youtube.com/watch?v=EL4QncmrLtU


## Brazo Mecánico
- Camilo Acuña:     dacunar@unal.edu.co
- Andrés Rodriguez: arodriguezper@unal.edu.co


## Caja Fuerte CG CY
- Cristian Gómez: crgomezo@unal.edu.co
- Cristian Yepes: cyepesf@unal.edu.co
- https://www.youtube.com/watch?v=uhYWWu4dG60


## Caja Fuerte CM JD
- Camilo Martín: camartinm@unal.edu.co
- Jorge Daza:    jodazar@unal.edu.co
- https://www.youtube.com/watch?v=OVHJDSpdnnY


## Dispensador de Comida
- Juan Ballesteros: juballesterosg@unal.edu.co
- Cesar Vargas:     cvargasmo@unal.edu.co
- https://www.youtube.com/watch?v=ySaoyvyFGBo


## Dispensador UNALIX
- Wilmer Araque: waraque@unal.edu.co
- Juan Moreno:   jmorenoor@unal.edu.co
- https://www.youtube.com/watch?v=qdK8OLQ-MTM


## Juego Musical
- Nicolás Prieto: nprietos@unal.edu.co
- Oscar Olivo:    oolivos@unal.edu.co
- Brian Muñoz:    bmunozg@unal.edu.co
- https://www.youtube.com/watch?v=fP9w4GWX1dU


## Parqueo Seguro
- Julián Gonzales: egonzalezn@unal.edu.co
- Sara Ospina:     saospinar@unal.edu.co

## Poste de Luz Inteligente
- Johan Santamaría: josantamariaa@unal.edu.co
- Fabian Abreo:     fabreo@unal.edu.co
- https://www.youtube.com/watch?v=5rhWV5-TwhU


## Sensor Parqueo con Freno y Alarma
- Nicolás Carvajal: jcarvajalu@unal.edu.co
- Giovanni Obregon: eobregon@unal.edu.co
- https://www.youtube.com/watch?v=LtLu-TNcDUY

