library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity ex_div is
port ( clk,rst :  in std_logic;
           sel : in std_logic;
         pulse_p, pulse_l   : out std_logic);
end ex_div;

architecture Behavioral of ex_div is



signal dd, qq : std_logic_vector(28 downto 0):=(others=>'0');
signal ovf    : std_logic;
signal pulse : std_logic;
signal pulse1 : std_logic;

begin

mux_add  :
       dd <= qq + 1  when ovf='0' else
            (others=>'0');

-- ? [Hz]
ovf_com  :
ovf <= '1' when qq = x"5D536" else -- 38[M]
              '0';

-- Flip-Flop
process (clk, rst)
begin  
   if rst = '1' then
      qq <= (others => '0');
   elsif (sel='1') then
      if (clk'event and clk = '1') then
      qq <= dd;
        else
      qq <= qq;
   end if;
   end if;
end process;

-- output
pulse1 <= '1' when qq < x"53FE3" else -- 4 [M]
              '0';      

pulse_p <= pulse1;

end Behavioral;
