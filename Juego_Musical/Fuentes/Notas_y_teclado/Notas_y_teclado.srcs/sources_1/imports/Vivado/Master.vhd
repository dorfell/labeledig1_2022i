----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.05.2022 20:03:53
-- Design Name: 
-- Module Name: Master - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Master is
  Port (start,rst,clk:in std_logic;
    rgb_salida:out std_logic_vector(2 downto 0);
    v_sync,h_sync:out std_logic;
    
	COLUMNAS   : IN  STD_LOGIC_VECTOR(3 DOWNTO 0); --PUERTO CONECTADO A LAS COLUMNAS DEL TECLADO
	FILAS 	  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
	IND		  : OUT STD_LOGIC;				   
	SALIDA: OUT std_logic_vector(6 downto 0);
	

    LCD_RS : out STD_LOGIC; --del LCD (JD1)
    LCD_RW : out STD_LOGIC; --read/write del LCD (JD2)
    LCD_E : out STD_LOGIC; --enable del LCD (JD3)
    DATA : out STD_LOGIC_VECTOR (7 downto 0); --bus de datos de la LCD (JC1-4,7-10)
    
    pulse_p: out std_logic 
   );
end Master;

architecture structural of Master is
component Nota_completa is
  Port (linea:in integer;
  start,rst,clk, set,enano:in std_logic;
  rgb_out: out std_logic_vector(2 downto 0):="000";
  v_sync,h_sync: out std_logic;
  x_pos,y_pos: out integer;
  punto: out std_logic;
  pulse_p: out std_logic  
   );
end component;

component Keypad is
  Port ( 
    CLK 		  : IN  STD_LOGIC; 				   --RELOJ FPGA
	COLUMNAS   : IN  STD_LOGIC_VECTOR(3 DOWNTO 0); --PUERTO CONECTADO A LAS COLUMNAS DEL TECLADO
	FILAS 	  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
	IND		  : OUT STD_LOGIC;				   
	CHAR : OUT	STD_LOGIC_VECTOR(3 DOWNTO 0)
);
end component Keypad;

component Decodificador is
  port( salida: in std_logic_vector(3 downto 0);
  salidad: out std_logic_vector(6 downto 0)
  );
end component Decodificador;

component LCD is
  port(     CLOCK : in STD_LOGIC; --reloj de 100MHz
    REINI : in STD_LOGIC; --boton de reinicio a BTNC
    Puntaje: in integer;
    LCD_RS : out STD_LOGIC; --del LCD (JD1)
    LCD_RW : out STD_LOGIC; --read/write del LCD (JD2)
    LCD_E : out STD_LOGIC; --enable del LCD (JD3)
    DATA : out STD_LOGIC_VECTOR (7 downto 0) --bus de datos de la LCD (JC1-4,7-10)
  );
end component LCD;

signal punto: std_logic;
signal Puntaje: integer:= 0;
signal CHAR: std_logic_vector (3 downto 0);
signal set:std_logic_vector (3 downto 0);
signal rgb:std_logic_vector(2 downto 0);
signal rgb_notad1:std_logic_vector(2 downto 0);
signal line_on:std_logic;
signal x_position:integer;
signal y_position:integer;

begin

teclado: Keypad port map(
CLK=>CLK,
COLUMNAS=>COLUMNAS,
FILAS=>FILAS,
IND=>IND,
CHAR=>set
);

deco: Decodificador port map(
salida=>set,
salidad=>SALIDA
);

DNote:Nota_completa
port map(
linea=>0,start=>start,rst=>rst,set=>set(0),rgb_out=>rgb_notad1,
v_sync=> v_sync,h_sync=> h_sync,clk=> clk,enano=>'1',x_pos=> x_position, y_pos=> y_position, punto=>punto, pulse_p=>pulse_p);

pantalla: LCD port map (
    CLOCK => clk,
    REINI => rst, --boton de reinicio a BTNC
    Puntaje => puntaje,
    LCD_RS => LCD_RS, --del LCD (JD1)
    LCD_RW => LCD_RW, --read/write del LCD (JD2)
    LCD_E => LCD_E, --enable del LCD (JD3)
    DATA => DATA --bus de datos de la LCD (JC1-4,7-10)
);

line_on<='1' when y_position>400 and y_position<420 else '0';

rgb_salida<="111" when line_on='1' else
            rgb_notad1;

process(punto)
begin if rst='1' then
    puntaje<=0;
elsif rising_edge(punto) then
        puntaje<=puntaje+5;
end if;
end process;

end structural;
