----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 19.05.2022 20:49:09
-- Design Name: 
-- Module Name: Notas - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Notas is
    Port (x_pos,y_pos: in integer:=0;
    x_line: in integer;
    clk: in std_logic;
    start,rst,ena,set: in std_logic;
    rgb_nota: in std_logic_vector(2 downto 0):="000";
    rgb_signal: out std_logic_vector(2 downto 0):="000";
    video_on: in std_logic;
    punto: out std_logic;
    pulse_p:out std_logic );
    
end Notas;

architecture Behavioral of Notas is

component ex_div is
port (
           clk,rst :  in std_logic;
           sel : in std_logic;
         pulse_p, pulse_l   : out std_logic

);
end component;

constant Circle_Size:integer:=50;
signal Circle_on:std_logic:='0';
signal y_colum:integer:=0;
--Circulo
signal tick:std_logic:='0';
signal y_next:integer:=0;
signal delta:integer:=11;
signal finish:std_logic:='0';
signal delta_next:integer:=0;
signal  aux_clk:std_logic;

signal enable:std_logic;
signal result:std_logic_vector(2 downto 0):="000";

begin

divisord: ex_div
port map(
clk =>clk,rst=>rst,
           sel=>enable ,
         pulse_p=>pulse_p, pulse_l=>open);

Circle_on<='1' when ((x_pos-320)**2)+((y_pos-y_colum)**2)<(Circle_Size/2)**2 else
    '0';
process(video_on,Circle_on,
rgb_nota,result)
--RGB
begin
if video_on='0' then
    rgb_signal<="000";
else
    if Circle_on='1' then
        rgb_signal<=rgb_nota;
    else
        rgb_signal<=result;
    end if;
end if;
end process;

process(tick,rst,finish)
begin if rst='1' or finish='1' then
    y_next<=0;
elsif rising_edge(tick) then
    if ena='1' then
        y_next<=y_next+5;
    end if;
end if;
end process;
--comparador
process(enable,set)
begin
if enable='0' then
    result <= "000";
elsif enable='1' then
    if set='1' then
        result<="111";
    elsif rst='1' then
        result<="000";
    else 
        result<="000";
    end if;
end if;
end process;

punto <= '1' when result = "111" else '0';

tick<='1' when y_pos=481 and x_pos=0 else '0'; 
--aux_clk<='1' when (rising_edge(tick)) else '0';
finish<='1' when y_colum > 600 else '0';
enable<='1' when (y_colum + Circle_Size/2)>412 and (y_colum - Circle_Size/2)<412 else '0';
y_colum<=y_next;
end Behavioral;
