
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Nota_completa is
  Port (linea:in integer;
  start,rst,clk,set,enano:in std_logic;
  rgb_out: out std_logic_vector(2 downto 0):="000";
  v_sync,h_sync: out std_logic;
  x_pos,y_pos: out integer;
  punto: out std_logic;
  pulse_p: out std_logic 
  );
end Nota_completa;

architecture structural of Nota_completa is
signal rgb: std_logic_vector(2 downto 0):="000";
signal v_aux,enable: std_logic;
signal video_on:std_logic;
signal x_position,y_position:integer:=0;
signal posicion:integer;
signal ena:std_logic;

component Notas is
    Port (x_pos,y_pos: in integer:=0;
    x_line: in integer;
    clk: in std_logic;
    start,rst,ena,set: in std_logic;
    rgb_nota: in std_logic_vector(2 downto 0):="000";
    rgb_signal: out std_logic_vector(2 downto 0):="000";
    video_on: in std_logic;
    punto: out std_logic;
    pulse_p:out std_logic);
end component;

component Sync2_ElectricBoogalo is
  Port (clk,rst: in std_logic;
  h_sync,v_sync:out std_logic:='0';
  sync,video_on:inout std_logic;
  x_pos,y_pos: out integer
  );

end component;
begin
synco:Sync2_ElectricBoogalo
port map(
clk=>clk,rst=>rst,v_sync=>v_aux,
h_sync=>h_sync,sync=>open,video_on=>video_on,
x_pos=>x_position,y_pos=> y_position
); 
nota:Notas
port map(x_pos=>x_position,y_pos=>y_position,x_line=>posicion,rst=>rst,start=>start,
 rgb_nota=> rgb,video_on=> video_on,rgb_signal=>rgb_out,clk=>clk,ena=>enano,set=>set, punto => punto, pulse_p =>pulse_p
); 


rgb<= "001" when linea=0 else
       "011" when linea=1 else
       "101" when linea=2 else
       "110";
posicion<=125  when linea=0 else
        275 when linea=1 else
        425 when linea=2 else
       575;
v_sync<=v_aux;
x_pos<=x_position;
y_pos<=y_position;
end structural;
