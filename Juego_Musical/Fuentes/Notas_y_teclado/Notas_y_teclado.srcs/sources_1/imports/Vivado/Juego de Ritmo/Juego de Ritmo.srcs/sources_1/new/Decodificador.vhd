----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 24.03.2022 19:24:33
-- Design Name: 
-- Module Name: Decodificador - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Decodificador is
  port( salida: in std_logic_vector(3 downto 0);
  salidad: out std_logic_vector(6 downto 0)
  );
end Decodificador;

architecture Behavioral of Decodificador is

begin

salidad (6)<= NOT(((NOT salida(0)) AND ((NOT salida(1)) AND (NOT salida(2)) AND (NOT salida(3)))) OR (salida(1) AND (NOT salida(2)) AND (NOT salida(3))) OR (salida(0) AND ((NOT salida(1)) AND salida(2) AND (NOT salida(3)))) OR (salida(1) AND salida(2) AND (NOT salida(3))) OR ((NOT salida(1)) AND (NOT salida(2)) AND salida(3)) OR ((NOT salida(0)) AND (salida(1) AND (NOT salida(2)) AND salida(3))) OR ((NOT salida(0)) AND ((NOT salida(1)) AND salida(2) AND salida(3))) OR (salida(1) AND salida(2) AND salida(3)));

salidad(5)<= NOT (((NOT salida(1)) AND (NOT salida(2)) AND (NOT salida(3))) OR (salida(1) AND (NOT salida(2)) AND (NOT salida(3))) OR ((NOT salida(0)) AND ((NOT salida(1)) AND salida(2) AND (NOT salida(3)))) OR (salida(0) AND ((salida(1)) AND salida(2) AND (NOT salida(3)))) OR ((NOT salida(1)) AND (NOT salida(2)) AND salida(3)) OR ((NOT salida(0)) AND (salida(1) AND (NOT salida(2)) AND salida(3))) OR (salida(0) AND ((NOT salida(1)) AND salida(2) AND salida(3))));

salidad(4) <= NOT (((NOT salida(1)) AND (NOT salida(2)) AND (NOT salida(3))) OR (salida(0) AND (salida(1) AND (NOT salida(2)) AND (NOT salida(3)))) OR ((NOT salida(1)) AND salida(2) AND (NOT salida(3))) OR (salida(1) AND salida(2) AND (NOT salida(3))) OR ((NOT salida(1)) AND (NOT salida(2)) AND salida(3)) OR (salida(1) AND (NOT salida(2)) AND salida(3)) OR (salida(0) AND ((NOT salida(1)) AND salida(2) AND salida(3))));

salidad(3)<= NOT (((NOT salida(0)) AND ((NOT salida(1)) AND (NOT salida(2)) AND (NOT salida(3)))) OR (salida(1) AND (NOT salida(2)) AND (NOT salida(3))) OR (salida(0) AND ((NOT salida(1)) AND salida(2) AND (NOT salida(3)))) OR ((NOT salida(0)) AND (salida(1) AND salida(2) AND (NOT salida(3)))) OR ((NOT salida(0)) AND ((NOT salida(1)) AND (NOT salida(2)) AND salida(3))) OR (salida(0) AND (salida(1) AND (NOT salida(2)) AND salida(3))) OR ((NOT salida(1)) AND salida(2) AND salida(3)) OR ((NOT salida(0)) AND (salida(1) AND salida(2) AND salida(3))));

salidad(2)<= NOT (((NOT salida(0)) AND ((NOT salida(1)) AND (NOT salida(2)) AND (NOT salida(3)))) OR ((NOT salida(0)) AND (salida(1) AND (NOT salida(2)) AND (NOT salida(3)))) OR ((NOT salida(0)) AND (salida(1) AND salida(2) AND (NOT salida(3)))) OR ((NOT salida(0)) AND ((NOT salida(1)) AND (NOT salida(2)) AND salida(3))) OR (salida(1) AND (NOT salida(2)) AND salida(3)) OR ((NOT salida(1)) AND salida(2) AND salida(3)) OR (salida(1) AND salida(2) AND salida(3)));

salidad(1)<= NOT (((NOT salida(0)) AND ((NOT salida(1)) AND (NOT salida(2)) AND (NOT salida(3)))) OR ((NOT salida(1)) AND salida(2) AND (NOT salida(3))) OR ((NOT salida(0)) AND (salida(1) AND salida(2) AND (NOT salida(3)))) OR ((NOT salida(1)) AND (NOT salida(2)) AND salida(3)) OR (salida(1) AND (NOT salida(2)) AND salida(3)) OR ((NOT salida(0)) AND ((NOT salida(1)) AND salida(2) AND salida(3))) OR (salida(1) AND salida(2) AND salida(3)));

salidad(0)<= NOT ((salida(1) AND (NOT salida(2)) AND (NOT salida(3))) OR ((NOT salida(1)) AND salida(2) AND (NOT salida(3))) OR ((NOT salida(0)) AND (salida(1) AND salida(2) AND (NOT salida(3)))) OR ((NOT salida(1)) AND (NOT salida(2)) AND salida(3)) OR (salida(1) AND (NOT salida(2)) AND salida(3)) OR (salida(0) AND ((NOT salida(1)) AND salida(2) AND salida(3))) OR (salida(1) AND salida(2) AND salida(3)));

end Behavioral;
