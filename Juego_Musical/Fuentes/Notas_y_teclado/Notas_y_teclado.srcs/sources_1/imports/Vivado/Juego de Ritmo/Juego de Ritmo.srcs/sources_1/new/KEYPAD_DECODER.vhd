----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.01.2022 12:24:41
-- Design Name: 
-- Module Name: KEYPAD_DECODER - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity KEYPAD_DECODER is
PORT(
	BOTON_PRES : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
	CHAR : OUT	STD_LOGIC_VECTOR(3 DOWNTO 0)				   					  
);
end KEYPAD_DECODER;



architecture Behavioral of KEYPAD_DECODER is


begin

process(BOTON_PRES)
begin
    case (BOTON_PRES) is
      when "0000" =>
         CHAR <= "0000"; -- 0
      when "0001" =>
         CHAR <= "0001"; -- 1
      when "0010" =>
         CHAR <= "0010"; -- 2
      when "0011" =>
         CHAR <= "0011"; -- 3
      when "0100" =>
         CHAR <= "0100"; -- 4
      when "0101" =>
         CHAR <= "0101"; -- 5
      when "0110" =>
         CHAR <= "0110"; -- 6
      when "0111" =>
         CHAR <= "0111"; -- 7
      when "1000" =>
         CHAR <= "1000"; -- 8
      when "1001" =>
        CHAR <=  "1001"; -- 9
      when "1010" =>
         CHAR <= "1010"; -- A
      when "1011" =>
         CHAR <= "1011"; -- B
      when "1100" =>
         CHAR <= "1100"; -- C
      when "1101" =>
         CHAR <= "1101"; -- D
      when "1110" =>
         CHAR <= "1110"; -- #
      when "1111" =>
         CHAR <= "1111"; -- *
      when others =>
         CHAR <= "0000"; -- 0 en caso de ghosting
   end case;
end process;

end Behavioral;
