----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.06.2022 10:55:54
-- Design Name: 
-- Module Name: Indicador - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Indicador is
  Port (verificacion: in std_logic;
        pwm: in std_logic;
        leds: out std_logic_vector(2 downto 0);
        parlante: out std_logic);
end Indicador;

architecture Behavioral of Indicador is

begin

leds<="101" when verificacion = '1' else "011";

parlante<='1' when verificacion = '1' else '0';

end Behavioral;
