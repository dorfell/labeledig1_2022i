----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 14.06.2022 13:34:24
-- Design Name: 
-- Module Name: BT - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BT is
--  Port ( );
end BT;

architecture Behavioral of BT is

component Mensaje_Alimento is

port(
    clk, rst:  in std_logic;
    start:  in std_logic;  --Empieza la carga del mensaje
    data_user: out std_logic_vector(7 downto 0); --Se utiliza para el mensaje a tx
    wr_uart : out std_logic --Libera el mensaje al terminar de cargar la fifo
--    rst_fifo : out std_logic   
);
end component Mensaje_alimento;

component UART is
	generic(
		DBIT: integer := 8;     -- # data bits
		SB_TICK: integer := 16; -- # ticks de parada
										-- 32/48/64 para 1/1.5/2 bits
		DVSR: integer := 652;   -- divisor de ticks
										-- = clk/(16*baudrate)
		DVSR_BIT: integer := 10; -- # bits de DVSR
		FIFO_W: integer := 4    -- # bits de direccion del fifo
										-- # palabras en FIFO = 2^FIFO_W
	);
	port(
		clk, rst: in std_logic;
		rd_uart, wr_uart: in std_logic; -- Para el FIFO
		rx: in std_logic;
		w_data: in std_logic_vector(DBIT-1 downto 0);
		tx_full, rx_empty: out std_logic;
		r_data: out std_logic_vector(DBIT-1 downto 0);
		tx: out std_logic
	);
end component UART;

begin


end Behavioral;
