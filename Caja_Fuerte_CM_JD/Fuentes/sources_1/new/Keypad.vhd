----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.05.2022 21:22:54
-- Design Name: 
-- Module Name: Keypad - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Keypad is
  Port ( 
    CLK 		  : IN  STD_LOGIC; 				   --RELOJ FPGA
	COLUMNAS   : IN  STD_LOGIC_VECTOR(3 DOWNTO 0); --PUERTO CONECTADO A LAS COLUMNAS DEL TECLADO
	FILAS 	  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
	IND		  : OUT STD_LOGIC;				   
	CHAR : OUT	STD_LOGIC_VECTOR(3 DOWNTO 0)
);
end Keypad;

architecture Behavioral of Keypad is

component LOGIC_KEYPAD is

PORT(
	CLK 		  : IN  STD_LOGIC; 				   --RELOJ FPGA
	COLUMNAS   : IN  STD_LOGIC_VECTOR(3 DOWNTO 0); --PUERTO CONECTADO A LAS COLUMNAS DEL TECLADO
	FILAS 	  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);  --PUERTO CONECTADO A LA FILAS DEL TECLADO
	BOTON_PRES : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --PUERTO QUE INDICA LA TECLA QUE SE PRESION�
	IND		  : OUT STD_LOGIC					   --BANDERA QUE INDICA CUANDO SE PRESION� UNA TECLA (S�LO DURA UN CICLO DE RELOJ)
);
end component LOGIC_KEYPAD;

component KEYPAD_DECODER is
PORT(
	BOTON_PRES : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);				   
	CHAR : OUT	STD_LOGIC_VECTOR(3 DOWNTO 0)					  
);
end component KEYPAD_DECODER;

signal BOTON_PRES: STD_LOGIC_VECTOR(3 DOWNTO 0);

begin

logic: LOGIC_KEYPAD port map(
CLK=>CLK,
COLUMNAS=>COLUMNAS,
FILAS=>FILAS,
BOTON_PRES=>BOTON_PRES,
IND=>IND
);

decoder: KEYPAD_DECODER port map(
BOTON_PRES=>BOTON_PRES,
CHAR=>CHAR
);

end Behavioral;
