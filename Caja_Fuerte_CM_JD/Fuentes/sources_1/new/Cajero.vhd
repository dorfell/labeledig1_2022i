----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.05.2022 22:10:53
-- Design Name: 
-- Module Name: Cajero - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Cajero is
  Port (
    CLK : IN  STD_LOGIC; 				   --RELOJ FPGA
    RST: in std_logic;
	COLUMNAS : IN  STD_LOGIC_VECTOR(2 DOWNTO 0); --PUERTO CONECTADO A LAS COLUMNAS DEL TECLADO
	FILAS : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);	   --BANDERA QUE INDICA CUANDO SE PRESION� UNA TECLA (S�LO DURA UN CICLO DE RELOJ
    COMUN: OUT STD_LOGIC_VECTOR(7 downto 0);
    SALIDA: OUT std_logic_vector(6 downto 0);
    hsync,vsync : out std_logic;
    rgb : out std_logic_vector ( 2 downto 0 );
    tx: out std_logic ;
    leds: out std_logic_vector(2 downto 0);
    parlante: out std_logic  
   );
end Cajero;

architecture Behavioral of Cajero is

component Test_VGA is
port(
clk, reset: in std_logic;
num: in std_logic_vector(3 downto 0);
--sw: in std_logic_vector ( 2 downto 0 ) ;
hsync,vsync : out std_logic;
verificacion: out std_logic;
rgb : out std_logic_vector ( 2 downto 0 )
);
end component Test_VGA ;

component Teclado is
   Port (
   Reloj: in std_logic;
   col: in std_logic_vector(2 downto 0);
   filas: out std_logic_vector(3 downto 0); 
   Segmentos: out std_logic_vector(6 downto 0)
    );
end component Teclado;

component Decodificador is
  port( segm: in std_logic_vector(6 downto 0);
  num: out std_logic_vector(3 downto 0)
  );
end component Decodificador;

component UART is
	generic(
		DBIT: integer := 8;     -- # data bits
		SB_TICK: integer := 16; -- # ticks de parada
										-- 32/48/64 para 1/1.5/2 bits
		DVSR: integer := 652;   -- divisor de ticks
										-- = clk/(16*baudrate)
		DVSR_BIT: integer := 10; -- # bits de DVSR
		FIFO_W: integer := 4    -- # bits de direccion del fifo
										-- # palabras en FIFO = 2^FIFO_W
	);
	port(
		clk, rst: in std_logic;
		rd_uart, wr_uart: in std_logic; -- Para el FIFO
		rx: in std_logic;
		w_data: in std_logic_vector(DBIT-1 downto 0);
		tx_full, rx_empty: out std_logic;
		r_data: out std_logic_vector(DBIT-1 downto 0);
		tx: out std_logic
	);
end component UART;

component Mensaje_Alimento is

port(
    clk, rst:  in std_logic;
    aux: in std_logic;
    start:  in std_logic;  --Empieza la carga del mensaje
    data_user: out std_logic_vector(7 downto 0); --Se utiliza para el mensaje a tx
    wr_uart : out std_logic --Libera el mensaje al terminar de cargar la fifo
--    rst_fifo : out std_logic   
);
end component Mensaje_alimento;

component digital_clock_top is
port (
	clkin: 	in STD_LOGIC;
	clkout:		out STD_LOGIC
);
end component digital_clock_top;

component DivisorParlante is
port (
	clkin: 	in STD_LOGIC;
	clkout:		out STD_LOGIC
);
end component DivisorParlante;

component Indicador is
  Port (verificacion: in std_logic;
        pwm: in std_logic;
        leds: out std_logic_vector(2 downto 0);
        parlante: out std_logic);
end component Indicador;

signal num: std_logic_vector(3 downto 0);
signal segmentos: std_logic_vector(6 downto 0);
signal data: std_logic_vector(7 downto 0);
signal write: std_logic;
signal start: std_logic;
signal verificacion: std_logic;
signal pwm: std_logic;

begin

mostrar: Indicador port map(
verificacion=>verificacion,
pwm=>pwm,
leds=>leds,
parlante=>parlante
);

divisor10k: DivisorParlante port map(
clkin=>CLK,
clkout=>pwm
);

divisor: digital_clock_top port map(
clkin=>CLK,
clkout=>start
);

msg: Mensaje_Alimento port map(
clk=>CLK,
rst=>RST,
start=>start,
data_user=>data,
wr_uart=>write,
aux=>verificacion
);

BT: UART port map(
clk=>CLK,
rst=>RST,
rd_uart=>'0',
rx=>'0',
wr_uart=>write,
w_data=>data,
tx_full=>open,
rx_empty=>open,
r_data=>open,
tx=>tx
);

vga: Test_VGA port map(
clk=>CLK,
reset=>RST,
hsync=>hsync,
vsync=>vsync,
rgb=>rgb,
num=>num,
verificacion=>verificacion
);

tec: Teclado port map(
Reloj=>CLK,
col=>COLUMNAS,
filas=>FILAS,
Segmentos=>segmentos
);

deco: Decodificador port map(
num=>num,
segm=>segmentos
);

SALIDA<=segmentos;
COMUN<="11111110";
end Behavioral;
