library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
entity DivisorParlante is
port (
	clkin: 	in STD_LOGIC;
	clkout:		out STD_LOGIC
);
end DivisorParlante;

architecture rtl of DivisorParlante is
	constant max_count: INTEGER := 125000000/12500;
	signal count: INTEGER range 0 to max_count;
	signal clk_state: STD_LOGIC := '0';
	
begin
	gen_clock: process(clkin, clk_state, count)
	begin
		if clkin'event and clkin='1' then
			if count < max_count then 
				count <= count+1;
			else
			 if clk_state='1' then
				clk_state <= not clk_state;
			    count<=0;
			 else
				clk_state <= not clk_state;
			end if;
			end if;
		end if;
	end process;
	
	persecond: process (clk_state)
	begin
		clkout <= clk_state;
	end process;
	
end rtl;