----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 14.06.2022 12:11:16
-- Design Name: 
-- Module Name: Decodificador - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Decodificador is
  Port (segm: in std_logic_vector(6 downto 0);
        num: out std_logic_vector(3 downto 0));
end Decodificador;

architecture Behavioral of Decodificador is

begin

num<="0000" when segm = "0000001" else
"0001" when segm = "1001111" else
"0010" when segm = "0010010" else
"0011" when segm = "0000110" else
"0100" when segm = "1001100" else
"0101" when segm = "0100100" else
"0110" when segm = "0100000" else
"0111" when segm = "0001111" else
"1000" when segm = "0000000" else
"1001" when segm = "0000100" else
"1010" when segm = "0110000" else
"1011" when segm = "0111000";


end Behavioral;
