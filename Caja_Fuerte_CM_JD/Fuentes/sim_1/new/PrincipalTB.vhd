-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 27.5.2022 03:17:19 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_Principal is
end tb_Principal;

architecture tb of tb_Principal is

    component Principal
        port (clk      : in std_logic;
              rst      : in std_logic;
              h_sync   : out std_logic;
              v_sync   : out std_logic;
              sync     : inout std_logic;
              video_on : inout std_logic;
              x_pos    : out integer;
              y_pos    : out integer);
    end component;

    signal clk      : std_logic;
    signal rst      : std_logic;
    signal h_sync   : std_logic;
    signal v_sync   : std_logic;
    signal sync     : std_logic;
    signal video_on : std_logic;
    signal x_pos    : integer;
    signal y_pos    : integer;

    constant TbPeriod : time := 1 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : Principal
    port map (clk      => clk,
              rst      => rst,
              h_sync   => h_sync,
              v_sync   => v_sync,
              sync     => sync,
              video_on => video_on,
              x_pos    => x_pos,
              y_pos    => y_pos);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clk is really your main clock signal
    clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed

        -- Reset generation
        -- EDIT: Check that rst is really your reset signal
        rst <= '1';
        wait for 100 ns;
        rst <= '0';
        wait for 100 ns;

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_Principal of tb_Principal is
    for tb
    end for;
end cfg_tb_Principal;