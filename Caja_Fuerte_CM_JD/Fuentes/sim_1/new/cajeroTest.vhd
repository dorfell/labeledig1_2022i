-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 27.5.2022 03:03:07 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_Cajero is
end tb_Cajero;

architecture tb of tb_Cajero is

    component Cajero
        port (CLK      : in std_logic;
              RST      : in std_logic;
              COLUMNAS : in std_logic_vector (3 downto 0);
              FILAS    : out std_logic_vector (3 downto 0);
              IND      : out std_logic;
              COMUN    : out std_logic_vector (7 downto 0);
              SALIDA   : out std_logic_vector (6 downto 0);
              hsync    : out std_logic;
              vsync    : out std_logic;
              rgb      : out std_logic_vector (2 downto 0 ));
    end component;

    signal CLK      : std_logic;
    signal RST      : std_logic;
    signal COLUMNAS : std_logic_vector (3 downto 0);
    signal FILAS    : std_logic_vector (3 downto 0);
    signal IND      : std_logic;
    signal COMUN    : std_logic_vector (7 downto 0);
    signal SALIDA   : std_logic_vector (6 downto 0);
    signal hsync    : std_logic;
    signal vsync    : std_logic;
    signal rgb      : std_logic_vector (2 downto 0 );

    constant TbPeriod : time := 1000 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : Cajero
    port map (CLK      => CLK,
              RST      => RST,
              COLUMNAS => COLUMNAS,
              FILAS    => FILAS,
              IND      => IND,
              COMUN    => COMUN,
              SALIDA   => SALIDA,
              hsync    => hsync,
              vsync    => vsync,
              rgb      => rgb);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        COLUMNAS <= (others => '0');

        -- Reset generation
        -- EDIT: Check that RST is really your reset signal
        RST <= '1';
        wait for 100 ns;
        RST <= '0';
        wait for 100 ns;

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_Cajero of tb_Cajero is
    for tb
    end for;
end cfg_tb_Cajero;