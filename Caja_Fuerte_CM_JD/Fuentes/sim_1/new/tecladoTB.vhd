-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 27.5.2022 03:07:51 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_Keypad is
end tb_Keypad;

architecture tb of tb_Keypad is

    component Keypad
        port (CLK      : in std_logic;
              COLUMNAS : in std_logic_vector (3 downto 0);
              FILAS    : out std_logic_vector (3 downto 0);
              IND      : out std_logic;
              CHAR     : out std_logic_vector (3 downto 0));
    end component;

    signal CLK      : std_logic;
    signal COLUMNAS : std_logic_vector (3 downto 0);
    signal FILAS    : std_logic_vector (3 downto 0);
    signal IND      : std_logic;
    signal CHAR     : std_logic_vector (3 downto 0);

    constant TbPeriod : time := 1 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : Keypad
    port map (CLK      => CLK,
              COLUMNAS => COLUMNAS,
              FILAS    => FILAS,
              IND      => IND,
              CHAR     => CHAR);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        COLUMNAS <= (others => '0');

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;
        
        COLUMNAS <= "0000";

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;
        
        COLUMNAS <= "0010";

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;
        
        COLUMNAS <= "0111";

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;
        
        COLUMNAS <= "1111";

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_Keypad of tb_Keypad is
    for tb
    end for;
end cfg_tb_Keypad;