library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity matriz is
  port ( clk,rstin :  in std_logic;
        Segmentos :  in std_logic_vector( 6 downto 0);
        col : in std_logic_vector(3 downto 0);
        sck, cs, mosi : out std_logic);
end matriz;

architecture Behavioral of matriz is


-- Component Declaration for the instances
------------------------------------------------------
component divisor_1kHz is
  port( 
    clk,rst :  in std_logic;
    en_sck  :  in std_logic; 
    sck     : out std_logic);
end component divisor_1kHz;

component divisor_enable is
  port(
    clk,rst :  in std_logic;
    en_spi  : out std_logic);
end component divisor_enable;

component fsm_spi is
  port(
    clk, rst      :  in std_logic;
    start_spi     :  in std_logic;
    en_spi        :  in std_logic;
    data          :  in std_logic_vector(15 downto 0);
    
    cs, mosi      : out std_logic);
end component fsm_spi;

component mem_as1107 is
  port(
    mem_addr :  in std_logic_vector( 7 downto 0);
    Segmentos  :  in std_logic_vector( 6 downto 0);
    mem_data : out std_logic_vector(15 downto 0) );
end component mem_as1107;

component fsm_as1107 is
  port(
    clk, rst      :  in std_logic;
    en_spi        :  in std_logic;
  
    start_spi     : out std_logic;
    mem_addr      : out std_logic_vector(7 downto 0) );
end component fsm_as1107;

component Top_rst is
  Port (
  
  col: in std_logic_vector(3 downto 0);
  clk, rst : in std_logic; 
    enablerst1, enablersts: out std_logic  
    );
end component Top_rst;


signal sg_start_spi, sg_en_spi  : std_logic;
signal sg_en_sck, sg_cs, sg_sck, rst: std_logic;
--signal MEM_SELE: std_logic_vector (1 downto 0); 
--signal SONIDO:  std_logic;

signal sg_mem_addr : std_logic_vector( 7 downto 0);
signal sg_mem_data : std_logic_vector(15 downto 0);
signal enablersts   : std_logic;

begin 


--! Instantiate the submodules 
-----------------------------------------
SM0: divisor_1kHz
  port map(
    clk    => clk,
    rst    => rst,
    en_sck => '1',
          
    sck    => sg_sck );

    
SM1: divisor_enable
  port map(
    clk    => clk,
    rst    => rst,
     
    en_spi => sg_en_spi );

    
SM2: fsm_spi
  port map(
    clk       => clk,
    start_spi => sg_start_spi,
    en_spi    => sg_en_spi,
    data      => sg_mem_data,
    rst       => rst,
    cs        => sg_cs,
    mosi      => mosi );


SM3: mem_as1107
  port map(
    mem_addr => sg_mem_addr,
    Segmentos => Segmentos,
    mem_data => sg_mem_data );


SM4: fsm_as1107
  port map(
    clk       => clk,
    rst       => rst,
    en_spi    => sg_en_spi,
    start_spi => sg_start_spi,
    mem_addr  => sg_mem_addr );
    
SM5: top_rst
  port map(
    clk => clk,
    rst => rstin,
    col => col,
    enablersts => enablersts
  );

-- SPI output
sck <= sg_sck when sg_cs = '0' else 
       '0';

cs <= sg_cs;

--rst <= enablerst1 or rstin;
rst <= enablersts;

end Behavioral;