

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FSM_BP is
  Port ( 
  
   clk, rst:  in std_logic; --enable es la salida del divisor 
    col: in std_logic_vector(3 downto 0);
    enable40: in std_logic;
   enablecontador: out std_logic;
   enablemux: out std_logic  
  );
end FSM_BP;

architecture Behavioral of FSM_BP is

type states is (idle,s_wait);
signal present_state, next_state: states;

begin

process(clk, rst)
  begin
  if rst = '1' then
    present_state <= idle;
    -- rst indica el estado inicial del sistema.
  elsif (clk'event and clk='1') then -- si hay flanco de subida entonces...
	   present_state <= next_state; --NS PASA AL ESTADO PRESENTE
    else		--en=0
	   present_state <= present_state;
--	
  end if;
end process;

-- next state logic

process(present_state, col, enable40)
  begin 
  case present_state is 
    when idle =>
	   if col(0) = '1' or col(1) = '1' or col(2) = '1' then 
		   next_state <= s_wait;   --s1 sound on 
		else --start = 1
		   next_state <= idle; 
		end if;
    
--    when S1 =>
--           next_state <= s_wait;
           
    when s_wait =>
    
    if enable40 = '1' then
		   next_state <= s_wait;   --s1 sound on 
		else --start = 1
		   next_state <= idle; 
		end if;
	    		
  end case;
end process;


--===================	
-- Output logic 
--===================
process(present_state)
  begin
  case present_state is

-- 2^s_{x}
    when idle =>
	  enablecontador  <= '0'; --1
	  enablemux <= '0';

--    when S1 =>
--      enablecontador  <= '1'; --1
--	  enablemux <= '1';
	  
    when s_wait =>
	  enablecontador  <= '1'; --1
	  enablemux <= '1';

  
  end case;
end process;

end Behavioral;
