
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity btn_on is
  Port ( 
  clk: in std_logic;
  rst :  in std_logic;
  bton_on: out std_logic;
  col: in std_logic_vector(3 downto 0)
  );
end btn_on;

architecture Behavioral of btn_on is

component contador40ms is
port(
    clk,rst :  in std_logic;
    enablecontador  :  in std_logic;       --enable habilita o deshabilita cierto circuito
    enable40  : out std_logic
     );
end component contador40ms;

component FSM_btn is
port(
    clk, rst:  in std_logic; --enable es la salida del divisor 
    col: in std_logic_vector(3 downto 0);
    enable40: in std_logic;
    enablecontador: out std_logic 
     );
end component FSM_btn ;

signal enablecontador: std_logic;
signal enable40: std_logic;

begin

SM2: contador40ms port map (clk=>clk, rst=>rst, enablecontador=>enablecontador, enable40=>enable40);

SM3: FSM_btn port map (clk=>clk, rst=>rst, col=>col, enable40=>enable40, enablecontador=>enablecontador);

bton_on <= enablecontador;

end Behavioral;
