--PWM
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity Contador40ms is
	port ( clk,rst :  in std_logic;
	       enablecontador  :  in std_logic;
	       --enable habilita o deshabilita cierto circuito
           enable40  : out std_logic);
           
end Contador40ms;

architecture Behavioral of Contador40ms is

signal dd, qq : std_logic_vector(23 downto 0):=(others=>'0');
signal ovf    : std_logic;

begin

mux_add  : 
       dd <= qq + 1  when ovf='0' else
            (others=>'0'); 

-- ? [Hz]
ovf_com  :				
		 ovf <= '1' when qq = x"4C4B40" else -- 40 ms 
              '0';

-- Flip-Flop
process (clk)
begin  
   if rst = '1' then
      qq <= (others => '0');
   elsif (clk'event and clk = '1') then
   if (enablecontador = '1') then
      qq <= dd;
      else
     qq <= qq;
   end if;
   end if;
end process;
		
-- output
enable40 <=  '0' when qq = x"4C4B40" else '1';


end Behavioral;
