
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TOP is
  Port ( 
  clk: in std_logic;
  rst :  in std_logic;
  col: in std_logic_vector(3 downto 0);
  fil: out std_logic_vector(3 downto 0);
  sck, cs, mosi : out std_logic;
  state: out std_logic_vector(3 downto 0);
  sonido: out std_logic
  );
end TOP;

architecture Behavioral of TOP is


component TECLADO_M is
  port(
    CLK 		  : IN  STD_LOGIC; 						  --RELOJ FPGA
	COLUMNAS   : IN  STD_LOGIC_VECTOR(3 DOWNTO 0); --PUERTO CONECTADO A LAS COLUMNAS DEL TECLADO
	FILAS 	  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --PUERTO CONECTADO A LA FILAS DEL TECLADO
	BOTON_PRES : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --PUERTO QUE INDICA LA TECLA QUE SE PRESION�
	IND		  : OUT STD_LOGIC		--BANDERA QUE INDICA CUANDO SE PRESION� UNA TECLA (S�LO DURA UN CICLO DE RELOJ)
    );
end component teclado_m;

component btn_on is
  Port ( 
  clk: in std_logic;
  rst :  in std_logic;
  bton_on: out std_logic;
  col:std_logic_vector(3 downto 0)
  );
end component btn_on;

component pulso40ms is

	port ( clk, rst :  in std_logic;
           pls40m  : out std_logic);
           
end component pulso40ms;

component FSM_PW is
  Port ( 
   clk, rst:  in std_logic;
   bton_on: in std_logic;
   btn_pres: in std_logic_vector(3 downto 0);
   pulso_btn_on: in std_logic;
   dibujo: out std_logic_vector(7 downto 0);
   dir: out std_logic_vector(1 downto 0);
   start: out std_logic
  );
end component FSM_PW;

component top_spi is
  port ( clk,rst_spi,start_spi :  in std_logic;
        dibujo: in std_logic_vector(7 downto 0);
        sck, cs, mosi : out std_logic);
end component top_spi;

component rst_start_spi is

	port ( clk, rst :  in std_logic;
           rst_spi  : out std_logic;
           start_spi: out std_logic);
end component rst_start_spi;

component TopMotor is
Port ( clk : in std_logic;
       rst: in std_logic;
       dir: in std_logic_vector(1 downto 0);
       start: in std_logic;
       Sw: in std_logic_vector(2 downto 0);
--     stop_motor: in std_logic;
       state: out std_logic_vector(3 downto 0);
       statex: out std_logic
--       btn_pres: in std_logic_vector (3 downto 0)
          
--          start : in std_logic;
--       dir : in std_logic_vector (1 downto 0);
      );
end component TopMotor;



signal enablecontador: std_logic;
signal enable40: std_logic;
signal bton_on: std_logic;
signal btn_pres: std_logic_vector(3 downto 0);
signal pls40m: std_logic;
signal dibujo: std_logic_vector(7 downto 0);
signal start_spi,rst_spi: std_logic;
signal dir: std_logic_vector(1 downto 0);
signal start: std_logic;
signal Sw: std_logic_vector(2 downto 0);
signal statex: std_logic;

begin

SM1: Teclado_M port map (clk => clk, columnas => col, filas => fil, boton_pres => btn_pres);

SM2: btn_on port map (clk=>clk, rst=>rst, bton_on=>bton_on, col=>col);
 
SM3: pulso40ms port map (clk=>clk, rst=>rst, pls40m=>pls40m);

SM4: FSM_PW port map (clk=>clk, rst=>rst, dibujo=>dibujo, bton_on=>bton_on, btn_pres=>btn_pres , pulso_btn_on=>pls40m, dir=>dir, start=>start);

SM5: top_spi port map (clk=>clk, rst_spi=>rst_spi, start_spi=>start_spi, dibujo=>dibujo, sck=>sck, cs=>cs, mosi=>mosi);

SM6: rst_start_spi port map (clk=>clk, rst=>rst, start_spi=>start_spi, rst_spi=>rst_spi);

SM7: topmotor port map (clk=>clk,rst=>rst,dir=>dir,start=>start,Sw=>Sw, state=>state,statex=>statex);

Sw<="10"&statex;

sonido<= '1' when bton_on='1' else '0';

end Behavioral;
