--PWM
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity clk40ms is

	port ( clk, rst :  in std_logic;
           clk40m  : out std_logic);
           
end clk40ms;

architecture Behavioral of clk40ms is

signal dd, qq : std_logic_vector(23 downto 0):=(others=>'0');
signal ovf    : std_logic;

begin

mux_add  : 
       dd <= qq + 1  when ovf='0' else
            (others=>'0'); 

-- ? [Hz]
ovf_com  :				
		 ovf <= '1' when qq = x"4C4B40" else -- 40 ms 
              '0';

-- Flip-Flop
process (clk)
begin  
   if rst = '1' then
      qq <= (others => '0');
   elsif (clk'event and clk = '1') then
      qq <= dd;
      else
     qq <= qq;
   end if;
end process;
-- output
clk40m <=  '1' when qq < x"2625A0" else '0';


end Behavioral;
