library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FSM_PW is
  Port ( 
   clk, rst:  in std_logic;
   btn_pres: in std_logic_vector(3 downto 0);
   bton_on: in std_logic;
   pulso_btn_on: in std_logic;
   dibujo: out std_logic_vector(7 downto 0);
   dir: out std_logic_vector(1 downto 0);
   start: out std_logic
--   password: out std_logic_vector(15 downto 0) := (others=>'0')
   
  );
end FSM_PW;

architecture Behavioral of FSM_PW is

type states is (d0,Dig1,d1,Dig2,d2,Dig3,d3,Dig4,d4,C,SF,HF,CM,tran);

signal present_state, next_state: states;

signal press_on: std_logic;
signal password: std_logic_vector(15 downto 0) :=(others=>'0');


begin

process(clk, rst, pulso_btn_on)
  begin
  if rst = '1' then
    present_state <= d0;
    -- rst indica el estado inicial del sistema.
  elsif (clk'event and clk='1') then
  if pulso_btn_on='1'then
   	   present_state <= next_state; --NS PASA AL ESTADO PRESENTE
    else		--en=0
	   present_state <= present_state;

  end if;
  end if;
end process;

-- next state logic

process(present_state, press_on, btn_pres)
  begin 
  case present_state is 
  
    when d0 =>
	   if press_on = '1' then 
		   next_state <= Dig1;   --s1 sound on 
		else --start = 1
		   next_state <= d0; 
		end if;
    
    when Dig1 =>
       if press_on = '0' then 
		   next_state <= d1;   --s1 sound on 
		else --start = 1
		   next_state <= Dig1; 
		end if;
           
    when d1 =>
    
        if press_on = '1' then 
		   next_state <= Dig2;   --s1 sound on 
		else --start = 1
		   next_state <= d1; 
		end if;
	
	when Dig2 =>
    
        if press_on = '0' then 
		   next_state <= d2;   --s1 sound on 
		else --start = 1
		   next_state <= Dig2; 
		end if;    	
		
	when d2 =>
    
        if press_on = '1' then 
		   next_state <= Dig3;   --s1 sound on 
		else --start = 1
		   next_state <= d2; 
		end if; 	
    
    when Dig3 =>
    
        if press_on = '0' then 
		   next_state <= d3;   --s1 sound on 
		else --start = 1
		   next_state <= Dig3; 
		end if; 	
	
	when d3 =>
    
        if press_on = '1' then 
		   next_state <= Dig4;   --s1 sound on 
		else --start = 1
		   next_state <= d3; 
		end if; 
		
	when Dig4 =>
    
        if press_on = '0' then 
		   next_state <= d4;   --s1 sound on 
		else --start = 1
		   next_state <= Dig4; 
		end if; 
		
	when d4 =>
    
        if press_on = '1' then 
		   next_state <= C;   --s1 sound on 
		else --start = 1
		   next_state <= d4; 
		end if; 
	
	when C =>
    
        if password = x"1289" then 
		   next_state <= HF;   --s1 sound on 
		else --start = 1
		   next_state <= SF; 
		end if;
	
	when SF =>
    
         if btn_pres = x"F" then 
		   next_state <= tran;   --s1 sound on 
		else --start = 1
		   next_state <= SF; 
		end if; 
	
	when HF =>
    
        if btn_pres = x"F" then 
		   next_state <= CM;   --s1 sound on 
		else --start = 1
		   next_state <= HF; 
		end if;
	
	when CM =>
    
        if btn_pres = x"E" then 
		   next_state <= tran;   --s1 sound on 
		else --start = 1
		   next_state <= CM; 
		end if;
		
    when tran =>
        if press_on = '0' then 
		   next_state <= d0;   --s1 sound on 
		else --start = 1
		   next_state <= tran; 
		end if;
		
		
			
  end case;
end process;


--===================	
-- Output logic 
--===================
process(present_state)
  begin
  case present_state is

-- 2^s_{x}
    when d0 =>
	  dibujo <= x"0A";

    when Dig1 =>
      dibujo <= "0000"&btn_pres;
      password(15 downto 12) <= btn_pres;
      
	  
    when d1 =>
	  dibujo <= x"0B";
	
	when Dig2 =>
	  dibujo <= "0000"&btn_pres;
      password(11 downto 8) <= btn_pres;
	  
	when d2 =>
	  dibujo <= x"0C";
	  
	when Dig3 =>
      dibujo <= "0000"&btn_pres;
      password(7 downto 4) <= btn_pres;
	  
    when d3 =>
	  dibujo <= x"0D";
	
	when Dig4 =>
	  dibujo <= "0000"&btn_pres;
      password(3 downto 0) <= btn_pres;
	  
	when d4 =>
	  dibujo <= x"10";
	 
	when C =>
	  dibujo<=x"13";
    
    when SF =>
      dibujo <= x"11";
    
    when HF =>
      dibujo <= x"12";
      dir <= "01";
      start <='1';
      
    when CM =>
      dibujo <=x"0E";
      dir <= "10";
      start <='1';
      
    when tran =>
      dibujo <= x"0F";
  
  end case;
end process;

press_on <= '1' when bton_on='1' and pulso_btn_on='1' else
            '0';

end Behavioral;
