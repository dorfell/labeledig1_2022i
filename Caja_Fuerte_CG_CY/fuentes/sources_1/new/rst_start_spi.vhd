--PWM
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity rst_start_spi is

	port ( clk, rst :  in std_logic;
           rst_spi  : out std_logic;
           start_spi: out std_logic);
end rst_start_spi;

architecture Behavioral of rst_start_spi is

signal dd, qq : std_logic_vector(27 downto 0):=(others=>'0');
signal ovf    : std_logic;

begin

mux_add  : 
       dd <= qq + 1  when ovf='0' else
            (others=>'0'); 

-- ? [Hz]
ovf_com  :				
		 ovf <= '1' when qq = x"3B9ACA0" else -- 40 ms 
              '0';

-- Flip-Flop
process (clk)
begin  
   if rst = '1' then
      qq <= (others => '0');
   elsif (clk'event and clk = '1') then
      qq <= dd;
      else
     qq <= qq;
   end if;
end process;
-- output
rst_spi <=  '1' when (qq < x"0000005") else '0';
start_spi <= '1' when (qq > x"0000005") and (qq < x"000000A") else '0';

end Behavioral;
