
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Top_tb is
end Top_tb;
 
 
architecture behavior of top_tb is 
 
  -- Component Declaration for the Unit Under Test (UUT)
  component TOP is
    port (        
          clk: in std_logic;
          rst :  in std_logic;
          col: in std_logic_vector(3 downto 0);
          fil: out std_logic_vector(3 downto 0);
          sck, cs, mosi : out std_logic
          );
  end component TOP;

 
  --Inputs -----------------------------
  signal clk, rst: std_logic;
  signal col : std_logic_vector(3 downto 0);   
  --Outputs -----------------------------
  signal enable_bps : std_logic;

   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
   constant clock_period : time := 8 ns;
 
 
begin
 
 
  -- Instantiate the Unit Under Test (UUT)
  uut: TOP 
    port map(
      --inputs
      clk   => clk,
      rst   => rst,
      col => col
       );


  -- Clock process definitions
  clock_process :process
    begin
	  clk <= '0';
      wait for clock_period/2;
      clk <= '1';
      wait for clock_period/2;
  end process;
 

  -- Stimulus process
  stim_proc: process
    begin		
      -- mantiene el rst por 8 ns x 2.
      wait for clock_period*2;
  
      -- simulación
      rst <= '0';
      wait for 20ms ;
      
      
--      col <= "0100";  --"4321"
--      wait for 10ms;

      col <= "0100";
      wait for 40ms;
      
--      col <= "0100";
--      wait for 10ms;

--      col <= "0000";
--      wait for 40ms;
      
--      col <= "0100";  --"4321"
--      wait for 10ms;

--      col <= "0000";
--      wait for 40ms;
      
--      col <= "0100";
--      wait for 10ms;

--      col <= "0000";
--      wait for 40ms;
      
      wait;
   end process;

end;