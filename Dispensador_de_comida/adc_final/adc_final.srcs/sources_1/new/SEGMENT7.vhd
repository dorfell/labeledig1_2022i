----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.05.2022 14:39:51
-- Design Name: 
-- Module Name: SEGMENT7 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity SEGMENT7 is
    port (
        f, g, h, i, j, k, l : out std_logic;
        x3, x2, x1, x0 : in std_logic
    );
end entity;

architecture arch of SEGMENT7 is
begin

    -- Decodificamos..
    process (x3, x2, x1, x0)
        variable auxVectOut : std_logic_vector (6 downto 0);
        variable auxVectIn : std_logic_vector (3 downto 0);
    begin

        -- Cargamos entradas al vector auxiliar.
        auxVectIn(3) := x3;
        auxVectIn(2) := x2;
        auxVectIn(1) := x1;
        auxVectIn(0) := x0;

         if auxVectIn = "0000" then auxVectOut := "0000001"; -- 0
        elsif auxVectIn = "0001" then auxVectOut := "1001111"; -- 1
        elsif auxVectIn = "0010" then auxVectOut := "0010010"; -- 2
        elsif auxVectIn = "0011" then auxVectOut := "0000110"; -- 3
        elsif auxVectIn = "0100" then auxVectOut := "1001100"; -- 4
        elsif auxVectIn = "0101" then auxVectOut := "0100100"; -- 5
        elsif auxVectIn = "0110" then auxVectOut := "0100000"; -- 6
        elsif auxVectIn = "0111" then auxVectOut := "0001111"; -- 7
        elsif auxVectIn = "1000" then auxVectOut := "0000000"; -- 8
        elsif auxVectIn = "1001" then auxVectOut := "0000100"; -- 9
        elsif auxVectIn = "1010" then auxVectOut := "0001000"; -- A
        elsif auxVectIn = "1011" then auxVectOut := "1100000"; -- B
        elsif auxVectIn = "1100" then auxVectOut := "0110001"; -- C
        elsif auxVectIn = "1101" then auxVectOut := "1000010"; -- D
        elsif auxVectIn = "1110" then auxVectOut := "0110000"; -- E

        
        end if;

        -- Cargamos salidas al vector auxiliar.
        f <= auxVectOut(6);
        g <= auxVectOut(5);
        h <= auxVectOut(4);
        i <= auxVectOut(3);
        j <= auxVectOut(2);
        k <= auxVectOut(1);
        l <= auxVectOut(0);

    end process;

end architecture;

