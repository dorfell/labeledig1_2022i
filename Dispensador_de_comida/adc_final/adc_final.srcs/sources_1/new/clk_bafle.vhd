
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity clk_bafle is
	port ( clk,rst :  in std_logic;
        enable : out std_logic );
         
end clk_bafle;


architecture Behavioral of clk_bafle is

   
   

signal dd, qq : std_logic_vector(28 downto 0):=(others=>'0');
signal ovf:std_logic;




begin

mux_add  : 
       dd <= qq+1  when ovf='0' else
            (others=>'0'); 

-- ? [Hz]
ovf_com  :				
		 ovf <= '1' when qq = x"377c8" else -- 227272 MHz
              '0';

-- Flip-Flop
process (clk, rst)
begin  
   if rst = '1' then
      qq <= (others => '0');
   elsif (clk'event and clk = '1') then
      qq <= dd;
   else
      qq <= qq;
   end if;
end process;
enable <= '1' when qq > x"34B64" else -- 204545 [M]Hz
              '0';

-- output

end Behavioral;

