----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.05.2022 14:51:33
-- Design Name: 
-- Module Name: reloj500k - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity reloj500k is
	port ( clk,rst :  in std_logic;
        clk_adc : out std_logic );
         
end reloj500k;


architecture Behavioral of reloj500k is

   

signal dd, qq : std_logic_vector(28 downto 0):=(others=>'0');
signal ovf:std_logic;




begin

mux_add  : 
       dd <= qq+1  when ovf='0' else
            (others=>'0'); 

-- ? [Hz]
ovf_com  :				
		 ovf <= '1' when qq = x"c7" else -- 10
              '0';

-- Flip-Flop
process (clk, rst)
begin  
   if rst = '1' then
      qq <= (others => '0');
   elsif (clk'event and clk = '1') then
      qq <= dd;
   else
      qq <= qq;
   end if;
end process;
clk_adc <= '1' when qq > x"63" else -- 25 [M]
              '0';

-- output

end Behavioral;


