library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity ex_div_bafle is
	port ( clk,rst, S0,S1,S2 :  in std_logic;
         pulse   : out std_logic);
end ex_div_bafle;


architecture Behavioral of ex_div_bafle is
component mux is
 port(
 
     
     Z0,Z1,Z2: in STD_LOGIC;
     SAL: out std_logic_vector(28 downto 0)
  );
end component mux;

signal dd, qq : std_logic_vector(28 downto 0):=(others=>'0');
signal ovf    : std_logic;
signal SAL : std_logic_vector(28 downto 0);


   signal cab_S0 : std_logic;
   signal cab_S1 : std_logic;
   signal cab_S2 :  std_logic;

begin

cab_S0 <= S0;
cab_S1 <= S1;
cab_S2 <= S2;


 MULTIPLEXOR_FRECUENCIAS: mux PORT MAP (
          
     
          
    
          Z0 => cab_S0,
          Z1 => cab_S1,
          Z2 => cab_S2,
          SAL => SAL
          
        );
mux_add  : 
       dd <= qq + 1  when ovf='0' else
            (others=>'0'); 

-- ? [Hz]
ovf_com  :				
		 ovf <= '1' when qq = x"5F5E100" else -- 100[M]
              '0';

-- Flip-Flop
process (clk, rst)
begin  
   if rst = '1' then
      qq <= (others => '0');
   elsif (clk'event and clk = '1') then
      qq <= dd;
   else
      qq <= qq;
   end if;
end process;

-- output
         
      
		 pulse <= '1' when qq < SAL else -- 25 [M]
              '0';

end Behavioral;
