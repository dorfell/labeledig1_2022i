library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux is
 port(
     Z0,Z1,Z2: in STD_LOGIC;
     SAL: out std_logic_vector(28 downto 0)
  );
end mux;
 
architecture bhv of mux is
begin
process (Z0,Z1,Z2) is
begin
  if (Z0 ='0' and Z1 = '0' and Z2 = '0') then
      SAL <= "00000000000110001111100000001";
  elsif (Z0 ='0' and Z1 = '0' and Z2 = '1') then
      SAL <= "00000000000110001111100000001";
  elsif (Z0 ='0' and Z1 = '1' and Z2 = '0') then
      SAL <= "00000000000110001111100000001";
       elsif (Z0 ='0' and Z1 = '1' and Z2 = '1') then
      SAL <= "00000000000110001111100000001";
  else
      SAL <= "00000000000110001111100000001";
  end if;
 
end process;
end bhv;