----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 14.06.2022 23:18:25
-- Design Name: 
-- Module Name: estados_bafle - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------




library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity estados_bafle is
	port(
			clk, rst, enable :  in std_logic;
			start        :  in std_logic;
			ENA_bafle         : out std_logic);
end estados_bafle;

architecture Behavioral of estados_bafle is

type states is (idle,s0,s1,s2,s3,s4,s5,s6,s7,s8,s9);
signal present_state, next_state: states;

begin

--===================
-- Present State
--===================
process(clk, rst,enable)
  begin
  if rst = '1' then
    present_state <= s0;
  elsif (clk'event and clk='1' and enable='1') then
    
	   present_state <= next_state;
    else		
	   present_state <= present_state;
	
  end if;
end process;

		
--===================
-- Next state logic
--===================
process(present_state, start, enable, clk)
  begin 
  case present_state is 
    when idle =>
	   if (start = '1') then
		   next_state <= s0;
		else 
		   next_state <= idle;
		end if;
    when s0 =>
	      next_state <= s1;
    when s1 =>
	      next_state <= s2;
    
    when s2 =>
	      next_state <= s3;
	      
	when s3 =>
	      next_state <= s4;
	when s4 =>
	      next_state <= s5;
	when s5 =>
	      next_state <= s6;
	when s6 =>
	      next_state <= s7;
	when s7 =>
	      next_state <= s8;
	when s8 =>
	      next_state <= s9;

    when s9 =>
		   next_state <= idle;
		
	    
    
  end case;
end process;
	
--===================	
-- Output logic
--===================
process(present_state)
  begin
  case present_state is
   when idle =>
	   ENA_bafle <= '0';
	   
    when s0 =>
	     ENA_bafle <= '1';

    when s1 =>
	    ENA_bafle <= '1';

    when s2 =>
	     ENA_bafle <= '1';

    when s3 =>
	     ENA_bafle <= '1';
	when s4 =>
	     ENA_bafle <= '1';
	when s5 =>
	     ENA_bafle <= '1';
	when s6 =>
	     ENA_bafle <= '1';
	when s7 =>
	     ENA_bafle <= '1';
	when s8 =>
	     ENA_bafle <= '1';
	when s9 =>
	     ENA_bafle <= '1';
	     

						
  end case;
end process;

end Behavioral;
