----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.05.2022 14:37:44
-- Design Name: 
-- Module Name: ex_div - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity ex_div is
	port ( clk,rst :  in std_logic;
        enable : out std_logic );
         
end ex_div;


architecture Behavioral of ex_div is

   
   

signal dd, qq : std_logic_vector(28 downto 0):=(others=>'0');
signal ovf:std_logic;




begin

mux_add  : 
       dd <= qq+1  when ovf='0' else
            (others=>'0'); 

-- ? [Hz]
ovf_com  :				
		 ovf <= '1' when qq = x"5F5E100" else -- 100 MHz
              '0';

-- Flip-Flop
process (clk, rst)
begin  
   if rst = '1' then
      qq <= (others => '0');
   elsif (clk'event and clk = '1') then
      qq <= dd;
   else
      qq <= qq;
   end if;
end process;
enable <= '1' when qq > x"5F5E0FF" else -- 99.999999 [M]Hz
              '0';

-- output

end Behavioral;
