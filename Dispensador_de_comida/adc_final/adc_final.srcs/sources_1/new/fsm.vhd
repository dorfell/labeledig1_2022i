----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.05.2022 14:38:43
-- Design Name: 
-- Module Name: fsm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------



library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity fsm is
	port(
			clk, rst, enable :  in std_logic;
			start        :  in std_logic;
			start_adc         : out std_logic);
end fsm;

architecture Behavioral of fsm is

type states is (idle,s0,s1,s2,s3);
signal present_state, next_state: states;

begin

--===================
-- Present State
--===================
process(clk, rst)
  begin
  if rst = '1' then
    present_state <= s0;
  elsif (clk'event and clk='1') then
    
	   present_state <= next_state;
    else		
	   present_state <= present_state;
	
  end if;
end process;

		
--===================
-- Next state logic
--===================
process(present_state, start, enable, clk)
  begin 
  case present_state is 
    when idle =>
	   if (start = '1') then
		   next_state <= s0;
		else 
		   next_state <= idle;
		end if;
    when s0 =>
	      next_state <= s1;
    when s1 =>
	      next_state <= s2;
    
    when s2 =>
	      next_state <= s3;

    when s3 =>
    if (enable = '1') then
		   next_state <= idle;
		end if;
	    
    
  end case;
end process;
	
--===================	
-- Output logic
--===================
process(present_state)
  begin
  case present_state is
   when idle =>
	   start_adc <= '0';
	   
    when s0 =>
	     start_adc <= '1';

    when s1 =>
	    start_adc <= '0';

    when s2 =>
	     start_adc <= '0';

    when s3 =>
	     start_adc <= '0';

						
  end case;
end process;

end Behavioral;
