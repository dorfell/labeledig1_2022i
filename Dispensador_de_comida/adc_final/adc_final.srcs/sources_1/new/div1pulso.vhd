----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 14.06.2022 23:22:46
-- Design Name: 
-- Module Name: div1pulso - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity div1pulso is
	port ( clk,rst :  in std_logic;
        enable : out std_logic );
         
end div1pulso;


architecture Behavioral of div1pulso is

   
   

signal dd, qq : std_logic_vector(28 downto 0):=(others=>'0');
signal ovf:std_logic;




begin

mux_add  : 
       dd <= qq+1  when ovf='0' else
            (others=>'0'); 

-- ? [Hz]
ovf_com  :				
		 ovf <= '1' when qq = x"5F5E100" else -- 227272 MHz
              '0';

-- Flip-Flop
process (clk, rst)
begin  
   if rst = '1' then
      qq <= (others => '0');
   elsif (clk'event and clk = '1') then
      qq <= dd;
   else
      qq <= qq;
   end if;
end process;
enable <= '1' when qq > x"5F5E0FF" else -- 204545 [M]Hz
              '0';

-- output

end Behavioral;
