----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.05.2022 14:36:20
-- Design Name: 
-- Module Name: altonivel - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity altonivel is
   
  Port ( clk, rst,start, dir : in std_logic;
             rd_uart: in std_logic;  -- Se puede dejar abierto
           rx: in std_logic;   -- Se puede dejar abierto
           tx: out std_logic;
			leds_salidas0,leds_salidas1,leds_salidas2,leds_salidas3,leds_salidas4,leds_salidas5,leds_salidas6,leds_salidas7       : out std_logic;
			leds_entradas0,leds_entradas1,leds_entradas2,leds_entradas3,leds_entradas6,leds_entradas7       : in std_logic;
			clk_adc,oe,start_adc,bafle : out std_logic ;
       Sw: in std_logic_vector(2 downto 0);
       mem_sel :  in std_logic_vector( 1 downto 0);
         sck, cs, mosi : out std_logic;
       state: out std_logic_vector(3 downto 0)
			);
end altonivel;

architecture Behavioral of altonivel is
component fsm is
		port(
			clk, rst, enable :  in std_logic;
			start        :  in std_logic;
			start_adc         : out std_logic);
end component fsm;
component ex_div is
      port ( clk,rst:  in std_logic;
      enable : out std_logic
     );
end component;

component clk_bafle is
      port ( clk,rst:  in std_logic;
      enable : out std_logic
     );
end component;

component reloj500k is
	port ( clk,rst :  in std_logic;
        clk_adc : out std_logic );
         
end component;
component Divisor is
port ( clk,rst :  in std_logic;
	      Sw : in std_logic_vector (2 downto 0);
         pulse : out std_logic);
end component;

component FSM_motor is
port( clk, rst, en, dir :  in std_logic;
        start        :  in std_logic;
        state         : out std_logic_vector(3 downto 0));
end component;
component divisor_1kHz is
  port( 
    clk,rst :  in std_logic;
    en_sck  :  in std_logic; 
    sck     : out std_logic);
end component divisor_1kHz;

component divisor_enable is
  port(
    clk,rst :  in std_logic;
    en_spi  : out std_logic);
end component divisor_enable;

component fsm_as1107 is
  port(
    clk, rst      :  in std_logic;
    start         :  in std_logic;
    en_spi        :  in std_logic;
  
    start_spi     : out std_logic;
    mem_addr      : out std_logic_vector(7 downto 0) );
end component fsm_as1107;

component mem_as1107 is
  port(
    mem_addr :  in std_logic_vector( 7 downto 0);
    mem_sel  :  in std_logic_vector( 1 downto 0);
    mem_data : out std_logic_vector(15 downto 0) );
end component mem_as1107;

component fsm_spi is
  port(
    clk, rst      :  in std_logic;
    start_spi     :  in std_logic;
    en_spi        :  in std_logic;
    data          :  in std_logic_vector(15 downto 0);

    cs, mosi      : out std_logic );
end component fsm_spi;
component mux is
 port(
 
     
     Z0,Z1,Z2: in STD_LOGIC;
     SAL: out std_logic_vector(28 downto 0)
  );
 end component mux;
 component div1pulso is
	port ( clk,rst :  in std_logic;
        enable : out std_logic );
end component div1pulso;
 
 component estados_bafle is
	port(
			clk, rst, enable :  in std_logic;
			start        :  in std_logic;
			ENA_bafle         : out std_logic);
end component estados_bafle;

component UART is
	generic(
		DBIT: integer := 8;     -- # data bits
		SB_TICK: integer := 16; -- # ticks de parada
										-- 32/48/64 para 1/1.5/2 bits
		DVSR: integer := 652;   -- divisor de ticks
										-- = clk/(16*baudrate)
		DVSR_BIT: integer := 10; -- # bits de DVSR
		FIFO_W: integer := 4    -- # bits de direccion del fifo
										-- # palabras en FIFO = 2^FIFO_W
	);
	port(
		clk, rst: in std_logic;
		rd_uart, wr_uart: in std_logic; -- Para el FIFO
		rx: in std_logic;
		w_data: in std_logic_vector(DBIT-1 downto 0);
		tx_full, rx_empty: out std_logic;
		r_data: out std_logic_vector(DBIT-1 downto 0);
		tx: out std_logic
	);
end component;
component Retraso_Fifo is
	port(
			clk, start, rst:  in std_logic;  --Empieza la carga del mensaje
            pulse : out std_logic
	);
end component;

component Mensaje_Alimento is
	port(
			clk, rst:  in std_logic;
			start        :  in std_logic;  --Empieza la carga del mensaje
			data_user: out std_logic_vector(7 downto 0); --Se utiliza para el mensaje a tx
			wr_uart : out std_logic --Libera el mensaje al terminar de cargar la fifo
	);
end component;
 
 
signal fifo_start : std_logic;
signal write_alimento: std_logic;
signal msg_alimento: std_logic_vector(7 downto 0); 
signal stop: std_logic;
signal str: std_logic; --Se ha cambiado la entrada de boton a teclado 
signal en: std_logic;
signal Clok    : std_logic := '1';
signal Reset   : std_logic := '0';
signal Clock_adc : std_logic;
 signal enable : std_logic  := '0';
  signal starts : std_logic  := '0';
  signal sg_start_spi, sg_en_spi  : std_logic;
signal sg_en_sck, sg_cs, sg_sck : std_logic;

signal sg_mem_addr : std_logic_vector( 7 downto 0);
signal sg_mem_data : std_logic_vector(15 downto 0);
	signal permite_bafle: std_logic;
	signal sonido_bafle: std_logic;
	
	signal dd, qq : std_logic_vector(28 downto 0):=(others=>'0');
signal ovf    : std_logic;
signal SAL : std_logic_vector(28 downto 0);


   signal cab_S0 : std_logic;
   signal cab_S1 : std_logic;
   signal cab_S2 :  std_logic;
   signal enable_pulso : std_logic;

begin


 leds_salidas0 <= leds_entradas0;
 leds_salidas1 <= leds_entradas1;
 leds_salidas2 <= leds_entradas2;
 leds_salidas3 <= leds_entradas3;
 leds_salidas4 <= (NOT(leds_entradas7));
 leds_salidas5 <= (NOT(leds_entradas7 XOR leds_entradas6));
 leds_salidas6 <= (leds_entradas7 XOR leds_entradas6);
 leds_salidas7 <= leds_entradas7;
 clk_adc <= Clock_adc;
 Clok <= clk;
 oe <= '1';
 bafle <= (sonido_bafle AND permite_bafle);
reloj : ex_div
    port map(
    
        clk    => Clok,
        rst   => rst,
      enable => enable
        );
        
clkbafle : clk_bafle
    port map(
    
        clk    => Clok,
        rst   => rst,
      enable => sonido_bafle
        );
 state_baf : estados_bafle port map(
 clk => clk,
 rst => rst,
 enable => enable_pulso,
 start => start,
 ENA_bafle => permite_bafle
 
 
 );
 pulso_bafle : div1pulso port map(
 clk => clk,
 rst => rst,
 enable => enable_pulso
 
 );
reloj_adc : reloj500k
    port map(
        
        clk    => Clk,
        rst   => rst,
      clk_adc => Clock_adc
        );

estados : fsm
    port map(
    
        clk    => Clock_adc,
        rst   => rst,
      enable => enable,
      start => start,
    
     start_adc => start_adc
        );
FSM1 : FSM_motor port map(clk => clk, rst => rst, start => start, dir => dir, state => state, en => en);
Div: Divisor port map(clk => clk, rst => rst, Sw => Sw, pulse => en);
SM0: divisor_1kHz
  port map(
    clk    => clk,
    rst    => rst,
    en_sck => '1',
          
    sck    => sg_sck );

SM1: divisor_enable
  port map(
    clk    => clk,
    rst    => rst,
     
    en_spi => sg_en_spi );

SM2: fsm_as1107
  port map(
    clk       => clk,
    rst       => rst,
    start     => start,
    en_spi    => sg_en_spi,
    
    start_spi => sg_start_spi,
    mem_addr  => sg_mem_addr );

SM3: mem_as1107
  port map(
    mem_addr => sg_mem_addr,
    mem_sel  => mem_sel,
    mem_data => sg_mem_data );

SM4: fsm_spi
  port map(
    clk       => clk,
    rst       => rst,
    start_spi => sg_start_spi,
    en_spi    => sg_en_spi,
    data      => sg_mem_data,
    
    cs        => sg_cs,
    mosi      => mosi );
    
retraso : Retraso_Fifo PORT MAP(
        clk => clk,
        rst => rst,
        start => start,
        pulse => fifo_start
    );

    mensaje : Mensaje_Alimento PORT MAP(
        clk => clk,
        rst => rst,
        start => fifo_start, --Inicia entrega de alimento
        data_user => msg_alimento, --Vector que entrega los caracteres
        wr_uart => write_alimento
    );

    uart_bluetooth : UART PORT MAP(
            clk => clk, 
            rst => rst,
            rd_uart => rd_uart, 
            wr_uart => write_alimento, 
            rx => rx, 
            tx => tx,
            w_data => msg_alimento, --Recibe los caracteres
            tx_full => open,
            rx_empty => open,
            r_data => open
        );
mux_add  : 
       dd <= qq + 1  when ovf='0' else
            (others=>'0'); 

-- ? [Hz]
ovf_com  :				
		 ovf <= '1' when qq = x"5F5E100" else -- 100[M]
              '0';
    
sck <= sg_sck when sg_cs = '0' else 
       '0';

cs <= sg_cs;
process (clk, rst)
begin  
   if rst = '1' then
      qq <= (others => '0');
   elsif (clk'event and clk = '1') then
      qq <= dd;
   else
      qq <= qq;
   end if;
end process;

-- output
         
      
		 
              



end Behavioral;
