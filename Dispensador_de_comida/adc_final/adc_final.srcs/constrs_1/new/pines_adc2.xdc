## Clock signal
##Bank = 35, Pin name = IO_L12P_T1_MRCC_35,					Sch name = CLK100MHZ
set_property PACKAGE_PIN E3 [get_ports clk]							
set_property IOSTANDARD LVCMOS33 [get_ports clk]
create_clock -add -name sys_clk_pin -period 10.00 -waveform {0 5} [get_ports clk]
	
## LEDs

	

##Pmod Header JA
##Bank = 15, Pin name = IO_L1N_T0_AD0N_15,					Sch name = JA1
set_property PACKAGE_PIN B13 [get_ports {leds_entradas0}]					
set_property IOSTANDARD LVCMOS33 [get_ports {leds_entradas0}]
##Bank = 15, Pin name = IO_L5N_T0_AD9N_15,					Sch name = JA2
set_property PACKAGE_PIN F14 [get_ports {leds_entradas1}]					
set_property IOSTANDARD LVCMOS33 [get_ports {leds_entradas1}]
##Bank = 15, Pin name = IO_L16N_T2_A27_15,					Sch name = JA3
set_property PACKAGE_PIN D17 [get_ports {leds_entradas2}]					
set_property IOSTANDARD LVCMOS33 [get_ports {leds_entradas2}]
##Bank = 15, Pin name = IO_L16P_T2_A28_15,					Sch name = JA4
set_property PACKAGE_PIN E17 [get_ports {leds_entradas3}]					
set_property IOSTANDARD LVCMOS33 [get_ports {leds_entradas3}]

##Bank = 15, Pin name = IO_L21N_T3_A17_15,					Sch name = JA9
set_property PACKAGE_PIN D18 [get_ports {leds_entradas6}]					
set_property IOSTANDARD LVCMOS33 [get_ports {leds_entradas6}]
##Bank = 15, Pin name = IO_L21P_T3_DQS_15,					Sch name = JA10
set_property PACKAGE_PIN E18 [get_ports {leds_entradas7}]					
set_property IOSTANDARD LVCMOS33 [get_ports {leds_entradas7}]


##Bank = 34, Pin name = IO_L10N_T1_34,						Sch name = LED8
set_property PACKAGE_PIN V4 [get_ports {leds_salidas0}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {leds_salidas0}]
##Bank = 34, Pin name = IO_L8N_T1_34,						Sch name = LED9
set_property PACKAGE_PIN U3 [get_ports {leds_salidas1}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {leds_salidas1}]
##Bank = 34, Pin name = IO_L7N_T1_34,						Sch name = LED10
set_property PACKAGE_PIN V1 [get_ports {leds_salidas2}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {leds_salidas2}]
##Bank = 34, Pin name = IO_L17P_T2_34,						Sch name = LED11
set_property PACKAGE_PIN R1 [get_ports {leds_salidas3}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {leds_salidas3}]


	
	
	
##Buttons
##Bank = 15, Pin name = IO_L11N_T1_SRCC_15,					Sch name = BTNC
set_property PACKAGE_PIN E16 [get_ports rst]						
set_property IOSTANDARD LVCMOS33 [get_ports rst]
##Bank = 15, Pin name = IO_L14P_T2_SRCC_15,					Sch name = BTNU
set_property PACKAGE_PIN F15 [get_ports start]						
set_property IOSTANDARD LVCMOS33 [get_ports start]
	
	
##Pmod Header JB
##Bank = 15, Pin name = IO_L15N_T2_DQS_ADV_B_15,				Sch name = JB1
set_property PACKAGE_PIN G14 [get_ports {oe}]					
set_property IOSTANDARD LVCMOS33 [get_ports {oe}]
##Bank = 14, Pin name = IO_L13P_T2_MRCC_14,					Sch name = JB2
set_property PACKAGE_PIN P15 [get_ports {clk_adc}]					
set_property IOSTANDARD LVCMOS33 [get_ports {clk_adc}]
##Bank = 14, Pin name = IO_L21N_T3_DQS_A06_D22_14,			Sch name = JB3
set_property PACKAGE_PIN V11 [get_ports {start_adc}]					
set_property IOSTANDARD LVCMOS33 [get_ports {start_adc}]


##Pmod Header JXADC
##Bank = 15, Pin name = IO_L9P_T1_DQS_AD3P_15,				Sch name = XADC1_P -> XA1_P
set_property PACKAGE_PIN A13 [get_ports {leds_salidas4}]				
	set_property IOSTANDARD LVCMOS33 [get_ports {leds_salidas4}]
##Bank = 15, Pin name = IO_L8P_T1_AD10P_15,					Sch name = XADC2_P -> XA2_P
set_property PACKAGE_PIN A15 [get_ports {leds_salidas5}]				
	set_property IOSTANDARD LVCMOS33 [get_ports {leds_salidas5}]
##Bank = 15, Pin name = IO_L7P_T1_AD2P_15,					Sch name = XADC3_P -> XA3_P
set_property PACKAGE_PIN B16 [get_ports {leds_salidas6}]				
	set_property IOSTANDARD LVCMOS33 [get_ports {leds_salidas6}]
##Bank = 15, Pin name = IO_L10P_T1_AD11P_15,					Sch name = XADC4_P -> XA4_P
set_property PACKAGE_PIN B18 [get_ports {leds_salidas7}]				
	set_property IOSTANDARD LVCMOS33 [get_ports {leds_salidas7}]
##Bank = 15, Pin name = IO_L9N_T1_DQS_AD3N_15,				Sch name = XADC1_N -> XA1_N
set_property PACKAGE_PIN A14 [get_ports {Sw[1]}]				
	set_property IOSTANDARD LVCMOS33 [get_ports {Sw[1]}]
##Bank = 15, Pin name = IO_L8N_T1_AD10N_15,					Sch name = XADC2_N -> XA2_N
set_property PACKAGE_PIN A16 [get_ports {Sw[2]}]				
	set_property IOSTANDARD LVCMOS33 [get_ports {Sw[2]}]
##Bank = 15, Pin name = IO_L7N_T1_AD2N_15,					Sch name = XADC3_N -> XA3_N 
set_property PACKAGE_PIN B17 [get_ports {mem_sel[0]}]				
	set_property IOSTANDARD LVCMOS33 [get_ports {mem_sel[0]}]
##Bank = 15, Pin name = IO_L10N_T1_AD11N_15,					Sch name = XADC4_N -> XA4_N
set_property PACKAGE_PIN A18 [get_ports {mem_sel[1]}]				
	set_property IOSTANDARD LVCMOS33 [get_ports {mem_sel[1]}]

##motor

##Switches
## Switches
##Bank = 34, Pin name = IO_L21P_T3_DQS_34,					Sch name = SW0
set_property PACKAGE_PIN U9 [get_ports {Sw[0]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {Sw[0]}]
##Bank = 34, Pin name = IO_L23P_T3_34,						Sch name = SW2
set_property PACKAGE_PIN R7 [get_ports {dir}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {dir}]

 
    
    
##Pmod Header JC
##Bank = 35, Pin name = IO_L23P_T3_35,						Sch name = JC1
set_property PACKAGE_PIN K2 [get_ports {state[0]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {state[0]}]
##Bank = 35, Pin name = IO_L6P_T0_35,						Sch name = JC2
set_property PACKAGE_PIN E7 [get_ports {state[1]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {state[1]}]
##Bank = 35, Pin name = IO_L22P_T3_35,						Sch name = JC3
set_property PACKAGE_PIN J3 [get_ports {state[2]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {state[2]}]
##Bank = 35, Pin name = IO_L21P_T3_DQS_35,					Sch name = JC4
set_property PACKAGE_PIN J4 [get_ports {state[3]}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {state[3]}]
	
	



##Pmod Header JD
##Bank = 35, Pin name = IO_L21N_T2_DQS_35,					Sch name = JD1
set_property PACKAGE_PIN H4 [get_ports {mosi}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {mosi}]
##Bank = 35, Pin name = IO_L17P_T2_35,						Sch name = JD2
set_property PACKAGE_PIN H1 [get_ports {cs}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {cs}]
##Bank = 35, Pin name = IO_L17N_T2_35,						Sch name = JD3
set_property PACKAGE_PIN G1 [get_ports {sck}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {sck}]
	
##UART/BLUETOOTH	
	
##Bank = 34, Pin name = IO_L20P_T3_34,						Sch name = SW5
set_property PACKAGE_PIN V7 [get_ports {rd_uart}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {rd_uart}]


    
##Bank = 35, Pin name = IO_L15P_T2_DQS_35,					Sch name = JD7
set_property PACKAGE_PIN H2 [get_ports {tx}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {tx}]
##Bank = 35, Pin name = IO_L20P_T3_35,						Sch name = JD8
set_property PACKAGE_PIN G4 [get_ports {rx}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {rx}]
	
	
	

	
	
##BAFLE
##Pmod Header JXADC


	
##Bank = 14, Pin name = IO_L19N_T3_A09_D25_VREF_14,			Sch name = JB10 
set_property PACKAGE_PIN U11 [get_ports {bafle}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {bafle}]
