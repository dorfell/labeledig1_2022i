----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.05.2022 14:41:32
-- Design Name: 
-- Module Name: clk_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 


entity clk_tb is
 
end clk_tb;



architecture Behavioral of clk_tb is
component fsm is
		port(
			clk, rst, enable :  in std_logic;
			start        :  in std_logic;
			start_adc         : out std_logic);
end component fsm;
component ex_div is
      port ( clk,rst:  in std_logic;
      enable : out std_logic
     );
end component;
component reloj500k is
	port ( clk,rst :  in std_logic;
        clk_adc : out std_logic );
         
end component;
constant ClockFrequency : integer := 100e6; -- 100 MHz
    constant ClockPeriod    : time    := 1000 ms / ClockFrequency;
signal Clok    : std_logic := '1';
signal Reset   : std_logic := '0';
signal Clock_adc : std_logic;
 signal enable : std_logic  := '0';
  signal starts : std_logic  := '0';
  signal start_adc :std_logic;
begin

reloj : ex_div
    port map(
    
        clk    => Clok,
        rst   => Reset,
      enable => enable
        );
reloj_adc : reloj500k
    port map(
        
        clk    => Clok,
        rst   => Reset,
      clk_adc => Clock_adc
        );

estados : fsm
    port map(
    
        clk    => Clock_adc,
        rst   => Reset,
      enable => enable,
      start => starts,
    
     start_adc => start_adc
        );


 Clok <= not Clok after ClockPeriod / 2;



stim_proc : process 
    begin
   -- Take the DUT out of reset

 
        
        wait for 200 ns;
        Reset <= '0';
        starts <= '0';
        wait for 200 ns;
        starts <='1';
        wait for 1000 ms;
        starts <= '0';
        wait for 1000 ms;
        starts <= '1';
        wait for 300 ms;
        starts <= '0';
 
        -- Reset the DUT
        wait;
        
    end process;
end Behavioral;