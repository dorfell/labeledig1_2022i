library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity divisor_enable is
  port ( clk,rst :  in std_logic;
         en_spi : out std_logic);
end divisor_enable;

architecture Behavioral of divisor_enable is

signal dd, qq : std_logic_vector(19 downto 0):=(others=>'0');
signal ovf    : std_logic;

begin

mux_add: 
  dd <= qq + 1  when ovf='0' else
        (others=>'0'); 

-- 1 [kHz]
ovf_com:				    
  ovf <= '1' when qq = x"1E848" else -- 125k 
         '0';
			  
en_comp :             
  en_spi <= '1' when ((qq > x"0F424") and (qq < x"0F426")) else --  pulse en 62.5k
            '0';

-- Register CE
process(clk, rst)
begin
  if rst = '1' then
    qq <= (others=>'0');
  elsif clk'event and clk='1' then
    qq <= dd;
  else
    qq <= qq;
  end if;
end process;

end Behavioral;