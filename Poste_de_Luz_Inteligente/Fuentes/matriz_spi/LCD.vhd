library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity LCD is
    
    Port (
    CLOCK : in STD_LOGIC; --reloj de 100MHz
    REINI : in STD_LOGIC; --boton de reinicio a BTNC
    LCD_RS : out STD_LOGIC; --del LCD (JD1)
    LCD_RW : out STD_LOGIC; --read/write del LCD (JD2)
    LCD_E : out STD_LOGIC; --enable del LCD (JD3)
    DATA : out STD_LOGIC_VECTOR (7 downto 0); --bus de datos de la LCD (JC1-4,7-10)
    SW : in STD_LOGIC --interruptores de la tarjeta
    );

end LCD;


architecture LCD of LCD is

-- FSM states
type STATE_TYPE is (
RST, ST0, ST1, FSET, EMSET, DO, CLD, RETH, SDDRAMA, WRITE1, WRITE2,
WRITE3, WRITE4, WRITE5, WRITE6, WRITE7, WRITE8, WRITE9, WRITE10,
WRITE11, WRITE12, WRITE13, WRITE14, SDDRAMA2, WRITE15, WRITE16,
WRITE17, WRITE18, WRITE19, WRITE20, WRITE21, WRITE22, WRITE23, SDDRAMA3, WRITE24);

-- se�ales
signal State,Next_State : STATE_TYPE;
--signal CONT1 : STD_LOGIC_VECTOR(24 downto 0) := X"0000000"; -- 33.554.432 = 0.33554432 s MAX
signal CONT1   : STD_LOGIC_VECTOR(23 downto 0) := X"000000"; -- 16,777,216 = 0.33554432 s MAX
signal CONT2   : STD_LOGIC_VECTOR(5 downto 0) :="000000"; -- 64 = 0.64 us
signal RESET   : STD_LOGIC :='0';
signal READY   : STD_LOGIC :='0';

signal fila1_1 : STD_LOGIC_VECTOR(7 downto 0) := x"20";  -- space
signal fila1_2 : STD_LOGIC_VECTOR(7 downto 0) := x"20";  -- space
signal fila1_3 : STD_LOGIC_VECTOR(7 downto 0) := x"20";  -- space
signal fila1_4 : STD_LOGIC_VECTOR(7 downto 0) := x"20";  -- space
signal fila1_5 : STD_LOGIC_VECTOR(7 downto 0) := x"20";  -- space
signal fila1_6 : STD_LOGIC_VECTOR(7 downto 0) := x"20";  -- space
signal fila1_7 : STD_LOGIC_VECTOR(7 downto 0) := x"20";  -- space
signal fila1_8 : STD_LOGIC_VECTOR(7 downto 0) := x"20";  -- space
signal fila1_9 : STD_LOGIC_VECTOR(7 downto 0) := x"20";  -- space
signal fila1_10: STD_LOGIC_VECTOR(7 downto 0) := x"20";  -- space
signal fila1_11: STD_LOGIC_VECTOR(7 downto 0) := x"20";  -- space
signal fila1_12: STD_LOGIC_VECTOR(7 downto 0) := x"20";  -- space
signal fila1_13: STD_LOGIC_VECTOR(7 downto 0) := x"20";  -- space
signal fila1_14: STD_LOGIC_VECTOR(7 downto 0) := x"20";  -- space

--constantes ASCII (la "M_" antes de la letra es para may�scula)
-- ASCII: hexadecimal(x"") decimal
--constant M_S: STD_LOGIC_VECTOR(7 downto 0) := x"53"; -- S 83
--constant simb161: STD_LOGIC_VECTOR(7 downto 0) := x"A1"; -- � 161
--constant i: STD_LOGIC_VECTOR(7 downto 0) := x"69"; -- I 105
--constant m: STD_LOGIC_VECTOR(7 downto 0) := x"6D"; -- m 109
--constant b: STD_LOGIC_VECTOR(7 downto 0) := x"62"; -- b 98
--constant o: STD_LOGIC_VECTOR(7 downto 0) := x"6F"; -- o 111
--constant l: STD_LOGIC_VECTOR(7 downto 0) := x"6C"; -- l 108
--constant s: STD_LOGIC_VECTOR(7 downto 0) := x"73"; -- s 115
--constant M_A: STD_LOGIC_VECTOR(7 downto 0) := x"41"; -- A 65
--constant M_C: STD_LOGIC_VECTOR(7 downto 0) := x"43"; -- C 67
--constant M_I: STD_LOGIC_VECTOR(7 downto 0) := x"49"; -- I 73
--constant corchetA: STD_LOGIC_VECTOR(7 downto 0) := x"5B"; -- [ 91
--constant corchetC: STD_LOGIC_VECTOR(7 downto 0) := x"5D"; -- ] 93
--constant M_W: STD_LOGIC_VECTOR(7 downto 0) := x"57"; -- W 87
--constant siete: STD_LOGIC_VECTOR(7 downto 0) := x"37"; -- n�mero 7 55
--constant cero: STD_LOGIC_VECTOR(7 downto 0) := x"30"; -- n�mero 0 48
--constant space: STD_LOGIC_VECTOR(7 downto 0) := x"20"; -- [space] 32
--constant menos: STD_LOGIC_VECTOR(7 downto 0) := x"2D"; -- - 45

----------------------------------------------------------------------------------------------------------------
----constantes ASCII (la "M_" antes de la letra es para may�scula)
constant space: STD_LOGIC_VECTOR(7 downto 0) := x"20"; -- [space] 32
constant admirC: STD_LOGIC_VECTOR(7 downto 0) := x"21"; -- ! 33
constant dblcomi: STD_LOGIC_VECTOR(7 downto 0) := x"22"; -- " 34
constant gato: STD_LOGIC_VECTOR(7 downto 0) := x"23"; -- # 35
constant pesos: STD_LOGIC_VECTOR(7 downto 0) := x"24"; -- $ 36
constant porcient: STD_LOGIC_VECTOR(7 downto 0) := x"25"; -- % 37
constant ampersan: STD_LOGIC_VECTOR(7 downto 0) := x"26"; -- & 38
constant apostrof: STD_LOGIC_VECTOR(7 downto 0) := x"27"; -- ' 39
constant parentA: STD_LOGIC_VECTOR(7 downto 0) := x"28"; -- ( 40
constant parentC: STD_LOGIC_VECTOR(7 downto 0) := x"29"; -- ) 41
constant asterisc: STD_LOGIC_VECTOR(7 downto 0) := x"2A"; -- * 42
constant mas: STD_LOGIC_VECTOR(7 downto 0) := x"2B"; -- + 43
constant coma: STD_LOGIC_VECTOR(7 downto 0) := x"2C"; -- , 44
constant menos: STD_LOGIC_VECTOR(7 downto 0) := x"2D"; -- - 45
constant punto: STD_LOGIC_VECTOR(7 downto 0) := x"2E"; -- . 46
constant diagonal: STD_LOGIC_VECTOR(7 downto 0) := x"2F"; -- / 47
constant cero: STD_LOGIC_VECTOR(7 downto 0) := x"30"; -- n�mero 0 48
constant uno: STD_LOGIC_VECTOR(7 downto 0) := x"31"; -- n�mero 1 49
constant dos: STD_LOGIC_VECTOR(7 downto 0) := x"32"; -- n�mero 2 50
constant tres: STD_LOGIC_VECTOR(7 downto 0) := x"33"; -- n�mero 3 51
constant cuatro: STD_LOGIC_VECTOR(7 downto 0) := x"34"; -- n�mero 4 52
constant cinco: STD_LOGIC_VECTOR(7 downto 0) := x"35"; -- n�mero 5 53
constant seis: STD_LOGIC_VECTOR(7 downto 0) := x"36"; -- n�mero 6 54
constant siete: STD_LOGIC_VECTOR(7 downto 0) := x"37"; -- n�mero 7 55
constant ocho: STD_LOGIC_VECTOR(7 downto 0) := x"38"; -- n�mero 8 56
constant nueve: STD_LOGIC_VECTOR(7 downto 0) := x"39"; -- n�mero 9 57
constant dosptos: STD_LOGIC_VECTOR(7 downto 0) := x"3A"; -- s�mbolo : 58
constant ptoycoma: STD_LOGIC_VECTOR(7 downto 0) := x"3B"; -- s�mbolo ; 59
constant menor: STD_LOGIC_VECTOR(7 downto 0) := x"3C"; -- s�mbolo < 60
constant igual: STD_LOGIC_VECTOR(7 downto 0) := x"3D"; -- s�mbolo = 61
constant mayor: STD_LOGIC_VECTOR(7 downto 0) := x"3E"; -- s�mbolo > 62
constant interrog: STD_LOGIC_VECTOR(7 downto 0) := x"3F"; -- s�mbolo ? 63

constant arroba: STD_LOGIC_VECTOR(7 downto 0) := x"40"; -- s�mbolo @ 64
constant M_A: STD_LOGIC_VECTOR(7 downto 0) := x"41"; -- A 65
constant M_B: STD_LOGIC_VECTOR(7 downto 0) := x"42"; -- B 66
constant M_C: STD_LOGIC_VECTOR(7 downto 0) := x"43"; -- C 67
constant M_D: STD_LOGIC_VECTOR(7 downto 0) := x"44"; -- D 68
constant M_E: STD_LOGIC_VECTOR(7 downto 0) := x"45"; -- E 69
constant M_F: STD_LOGIC_VECTOR(7 downto 0) := x"46"; -- F 70
constant M_G: STD_LOGIC_VECTOR(7 downto 0) := x"47"; -- G 71
constant M_H: STD_LOGIC_VECTOR(7 downto 0) := x"48"; -- H 72
constant M_I: STD_LOGIC_VECTOR(7 downto 0) := x"49"; -- I 73
constant M_J: STD_LOGIC_VECTOR(7 downto 0) := x"4A"; -- J 74
constant M_K: STD_LOGIC_VECTOR(7 downto 0) := x"4B"; -- K 75
constant M_L: STD_LOGIC_VECTOR(7 downto 0) := x"4C"; -- L 76
constant M_M: STD_LOGIC_VECTOR(7 downto 0) := x"4D"; -- M 77
constant M_N: STD_LOGIC_VECTOR(7 downto 0) := x"4E"; -- N 78
constant M_O: STD_LOGIC_VECTOR(7 downto 0) := x"4F"; -- O 79
constant M_P: STD_LOGIC_VECTOR(7 downto 0) := x"50"; -- P 80
constant M_Q: STD_LOGIC_VECTOR(7 downto 0) := x"51"; -- Q 81
constant M_R: STD_LOGIC_VECTOR(7 downto 0) := x"52"; -- R 82
constant M_S: STD_LOGIC_VECTOR(7 downto 0) := x"53"; -- S 83
constant M_T: STD_LOGIC_VECTOR(7 downto 0) := x"54"; -- T 84
constant M_U: STD_LOGIC_VECTOR(7 downto 0) := x"55"; -- U 85
constant M_V: STD_LOGIC_VECTOR(7 downto 0) := x"56"; -- V 86
constant M_W: STD_LOGIC_VECTOR(7 downto 0) := x"57"; -- W 87
constant M_X: STD_LOGIC_VECTOR(7 downto 0) := x"58"; -- X 88
constant M_Y: STD_LOGIC_VECTOR(7 downto 0) := x"59"; -- Y 89
constant M_Z: STD_LOGIC_VECTOR(7 downto 0) := x"5A"; -- Z 90
constant corchetA: STD_LOGIC_VECTOR(7 downto 0) := x"5B"; -- [ 91
constant diaginv: STD_LOGIC_VECTOR(7 downto 0) := x"5C"; -- \ 92
constant corchetC: STD_LOGIC_VECTOR(7 downto 0) := x"5D"; -- ] 93
constant circunfl: STD_LOGIC_VECTOR(7 downto 0) := x"5E"; -- ^ 94
constant guionbaj: STD_LOGIC_VECTOR(7 downto 0) := x"5F"; -- _ 95
constant apostroI: STD_LOGIC_VECTOR(7 downto 0) := x"60"; -- ` 96
constant a: STD_LOGIC_VECTOR(7 downto 0) := x"61"; -- a 97
constant b: STD_LOGIC_VECTOR(7 downto 0) := x"62"; -- b 98
constant c: STD_LOGIC_VECTOR(7 downto 0) := x"63"; -- c 99
constant d: STD_LOGIC_VECTOR(7 downto 0) := x"64"; -- d 100
constant e: STD_LOGIC_VECTOR(7 downto 0) := x"65"; -- e 101
constant f: STD_LOGIC_VECTOR(7 downto 0) := x"66"; -- f 102
constant g: STD_LOGIC_VECTOR(7 downto 0) := x"67"; -- g 103
constant h: STD_LOGIC_VECTOR(7 downto 0) := x"68"; -- h 104
constant i: STD_LOGIC_VECTOR(7 downto 0) := x"69"; -- i 105
constant j: STD_LOGIC_VECTOR(7 downto 0) := x"6A"; -- j 106
constant k: STD_LOGIC_VECTOR(7 downto 0) := x"6B"; -- k 107
constant l: STD_LOGIC_VECTOR(7 downto 0) := x"6C"; -- l 108
constant m: STD_LOGIC_VECTOR(7 downto 0) := x"6D"; -- m 109
constant n: STD_LOGIC_VECTOR(7 downto 0) := x"6E"; -- n 110
constant o: STD_LOGIC_VECTOR(7 downto 0) := x"6F"; -- o 111

constant p: STD_LOGIC_VECTOR(7 downto 0) := x"70"; -- p 112
constant q: STD_LOGIC_VECTOR(7 downto 0) := x"71"; -- q 113
constant r: STD_LOGIC_VECTOR(7 downto 0) := x"72"; -- r 114
constant s: STD_LOGIC_VECTOR(7 downto 0) := x"73"; -- s 115
constant t: STD_LOGIC_VECTOR(7 downto 0) := x"74"; -- t 116
constant u: STD_LOGIC_VECTOR(7 downto 0) := x"75"; -- u 117
constant v: STD_LOGIC_VECTOR(7 downto 0) := x"76"; -- v 118
constant w: STD_LOGIC_VECTOR(7 downto 0) := x"77"; -- w 119
constant x: STD_LOGIC_VECTOR(7 downto 0) := x"78"; -- x 120
constant y: STD_LOGIC_VECTOR(7 downto 0) := x"79"; -- y 121
constant z: STD_LOGIC_VECTOR(7 downto 0) := x"7A"; -- z 122
constant llaveA: STD_LOGIC_VECTOR(7 downto 0) := x"7B"; -- { 123
constant barra: STD_LOGIC_VECTOR(7 downto 0) := x"7C"; -- | 124
constant llaveC: STD_LOGIC_VECTOR(7 downto 0) := x"7D"; -- } 125
-- constant apostroI: STD_LOGIC_VECTOR(7 downto 0) := x"7E"; -- ~ 126
constant borrar: STD_LOGIC_VECTOR(7 downto 0) := x"7F"; -- delete 127
constant simb128: STD_LOGIC_VECTOR(7 downto 0) := x"80"; -- � 128
constant simb129: STD_LOGIC_VECTOR(7 downto 0) := x"81"; -- � 129
constant simb130: STD_LOGIC_VECTOR(7 downto 0) := x"82"; -- � 130
constant simb131: STD_LOGIC_VECTOR(7 downto 0) := x"83"; -- � 131
constant simb132: STD_LOGIC_VECTOR(7 downto 0) := x"84"; -- � 132
constant simb133: STD_LOGIC_VECTOR(7 downto 0) := x"85"; -- � 133
constant simb134: STD_LOGIC_VECTOR(7 downto 0) := x"86"; -- � 134
constant simb135: STD_LOGIC_VECTOR(7 downto 0) := x"87"; -- � 135
constant simb136: STD_LOGIC_VECTOR(7 downto 0) := x"88"; -- � 136
constant simb137: STD_LOGIC_VECTOR(7 downto 0) := x"89"; -- � 137
constant simb138: STD_LOGIC_VECTOR(7 downto 0) := x"8A"; -- � 138
constant simb139: STD_LOGIC_VECTOR(7 downto 0) := x"8B"; -- � 139
constant simb140: STD_LOGIC_VECTOR(7 downto 0) := x"8C"; -- � 140
constant simb141: STD_LOGIC_VECTOR(7 downto 0) := x"8D"; -- � 141
constant simb142: STD_LOGIC_VECTOR(7 downto 0) := x"8E"; -- � 142
constant simb143: STD_LOGIC_VECTOR(7 downto 0) := x"8F"; -- � 143
constant simb144: STD_LOGIC_VECTOR(7 downto 0) := x"90"; -- � 144
constant simb145: STD_LOGIC_VECTOR(7 downto 0) := x"91"; -- � 145
constant simb146: STD_LOGIC_VECTOR(7 downto 0) := x"92"; -- � 146
constant simb147: STD_LOGIC_VECTOR(7 downto 0) := x"93"; -- � 147
constant simb148: STD_LOGIC_VECTOR(7 downto 0) := x"94"; -- � 148
constant simb149: STD_LOGIC_VECTOR(7 downto 0) := x"95"; -- � 149
constant simb150: STD_LOGIC_VECTOR(7 downto 0) := x"96"; -- � 150
constant simb151: STD_LOGIC_VECTOR(7 downto 0) := x"97"; -- � 151
constant simb152: STD_LOGIC_VECTOR(7 downto 0) := x"98"; -- � 152
constant simb153: STD_LOGIC_VECTOR(7 downto 0) := x"99"; -- � 153
constant simb154: STD_LOGIC_VECTOR(7 downto 0) := x"9A"; -- � 154
constant simb155: STD_LOGIC_VECTOR(7 downto 0) := x"9B"; -- � 155
constant simb156: STD_LOGIC_VECTOR(7 downto 0) := x"9C"; -- � 156
constant simb157: STD_LOGIC_VECTOR(7 downto 0) := x"9D"; -- � 157
constant simb158: STD_LOGIC_VECTOR(7 downto 0) := x"9E"; -- � 158
constant simb159: STD_LOGIC_VECTOR(7 downto 0) := x"9F"; -- � 159

constant simb160: STD_LOGIC_VECTOR(7 downto 0) := x"A0"; -- � 160
constant simb161: STD_LOGIC_VECTOR(7 downto 0) := x"A1"; -- � 161
constant simb162: STD_LOGIC_VECTOR(7 downto 0) := x"A2"; -- � 162
constant simb163: STD_LOGIC_VECTOR(7 downto 0) := x"A3"; -- � 163
constant simb164: STD_LOGIC_VECTOR(7 downto 0) := x"A4"; -- � 164
constant simb165: STD_LOGIC_VECTOR(7 downto 0) := x"A5"; -- � 165
constant simb166: STD_LOGIC_VECTOR(7 downto 0) := x"A6"; -- a 166
constant simb167: STD_LOGIC_VECTOR(7 downto 0) := x"A7"; -- o 167
constant simb168: STD_LOGIC_VECTOR(7 downto 0) := x"A8"; -- � 168
constant simb169: STD_LOGIC_VECTOR(7 downto 0) := x"A9"; -- � 169
constant simb170: STD_LOGIC_VECTOR(7 downto 0) := x"AA"; -- � 170
constant simb171: STD_LOGIC_VECTOR(7 downto 0) := x"AB"; -- 1?2 171
constant simb172: STD_LOGIC_VECTOR(7 downto 0) := x"AC"; -- 1?4 172
constant simb173: STD_LOGIC_VECTOR(7 downto 0) := x"AD"; -- � 173
constant simb174: STD_LOGIC_VECTOR(7 downto 0) := x"AE"; -- � 174
constant simb175: STD_LOGIC_VECTOR(7 downto 0) := x"AF"; -- � 175
constant simb176: STD_LOGIC_VECTOR(7 downto 0) := x"B0"; -- ? 176
constant simb177: STD_LOGIC_VECTOR(7 downto 0) := x"B1"; -- ? 177
constant simb178: STD_LOGIC_VECTOR(7 downto 0) := x"B2"; -- ? 178
constant simb179: STD_LOGIC_VECTOR(7 downto 0) := x"B3"; -- ? 179
constant simb180: STD_LOGIC_VECTOR(7 downto 0) := x"B4"; -- ? 180
constant simb181: STD_LOGIC_VECTOR(7 downto 0) := x"B5"; -- � 181
constant simb182: STD_LOGIC_VECTOR(7 downto 0) := x"B6"; -- � 182
constant simb183: STD_LOGIC_VECTOR(7 downto 0) := x"B7"; -- � 183
constant simb184: STD_LOGIC_VECTOR(7 downto 0) := x"B8"; -- � 184
constant simb185: STD_LOGIC_VECTOR(7 downto 0) := x"B9"; -- ? 185
constant simb186: STD_LOGIC_VECTOR(7 downto 0) := x"BA"; -- ? 186
constant simb187: STD_LOGIC_VECTOR(7 downto 0) := x"BB"; -- ? 187
constant simb188: STD_LOGIC_VECTOR(7 downto 0) := x"BC"; -- ? 188
constant simb189: STD_LOGIC_VECTOR(7 downto 0) := x"BD"; -- � 189
constant simb190: STD_LOGIC_VECTOR(7 downto 0) := x"BE"; -- � 190
constant simb191: STD_LOGIC_VECTOR(7 downto 0) := x"BF"; -- ? 191
constant simb192: STD_LOGIC_VECTOR(7 downto 0) := x"C0"; -- ? 192
constant simb193: STD_LOGIC_VECTOR(7 downto 0) := x"C1"; -- ? 193
constant simb194: STD_LOGIC_VECTOR(7 downto 0) := x"C2"; -- ? 194
constant simb195: STD_LOGIC_VECTOR(7 downto 0) := x"C3"; -- ? 195
constant simb196: STD_LOGIC_VECTOR(7 downto 0) := x"C4"; -- ? 196
constant simb197: STD_LOGIC_VECTOR(7 downto 0) := x"C5"; -- ? 197
constant simb198: STD_LOGIC_VECTOR(7 downto 0) := x"C6"; -- � 198
constant simb199: STD_LOGIC_VECTOR(7 downto 0) := x"C7"; -- � 199
constant simb200: STD_LOGIC_VECTOR(7 downto 0) := x"C8"; -- ? 200
constant simb201: STD_LOGIC_VECTOR(7 downto 0) := x"C9"; -- ? 201
constant simb202: STD_LOGIC_VECTOR(7 downto 0) := x"CA"; -- ? 202
constant simb203: STD_LOGIC_VECTOR(7 downto 0) := x"CB"; -- ? 203
constant simb204: STD_LOGIC_VECTOR(7 downto 0) := x"CC"; -- ? 204
constant simb205: STD_LOGIC_VECTOR(7 downto 0) := x"CD"; -- ? 205
constant simb206: STD_LOGIC_VECTOR(7 downto 0) := x"CE"; -- ? 206
constant simb207: STD_LOGIC_VECTOR(7 downto 0) := x"CF"; -- � 207

constant simb208: STD_LOGIC_VECTOR(7 downto 0) := x"D0"; -- � 208
constant simb209: STD_LOGIC_VECTOR(7 downto 0) := x"D1"; -- � 209
constant simb210: STD_LOGIC_VECTOR(7 downto 0) := x"D2"; -- � 210
constant simb211: STD_LOGIC_VECTOR(7 downto 0) := x"D3"; -- � 211
constant simb212: STD_LOGIC_VECTOR(7 downto 0) := x"D4"; -- � 212
constant simb213: STD_LOGIC_VECTOR(7 downto 0) := x"D5"; -- ? 213
constant simb214: STD_LOGIC_VECTOR(7 downto 0) := x"D6"; -- � 214
constant simb215: STD_LOGIC_VECTOR(7 downto 0) := x"D7"; -- � 215
constant simb216: STD_LOGIC_VECTOR(7 downto 0) := x"D8"; -- � 216
constant simb217: STD_LOGIC_VECTOR(7 downto 0) := x"D9"; -- ? 217
constant simb218: STD_LOGIC_VECTOR(7 downto 0) := x"DA"; -- ? 218
constant simb219: STD_LOGIC_VECTOR(7 downto 0) := x"DB"; -- ? 219
constant simb220: STD_LOGIC_VECTOR(7 downto 0) := x"DC"; -- ? 220
constant simb221: STD_LOGIC_VECTOR(7 downto 0) := x"DD"; -- � 221
constant simb222: STD_LOGIC_VECTOR(7 downto 0) := x"DE"; -- � 222
constant simb223: STD_LOGIC_VECTOR(7 downto 0) := x"DF"; -- ? 223
constant simb224: STD_LOGIC_VECTOR(7 downto 0) := x"E0"; -- � 224
constant simb225: STD_LOGIC_VECTOR(7 downto 0) := x"E1"; -- � 225
constant simb226: STD_LOGIC_VECTOR(7 downto 0) := x"E2"; -- � 226
constant simb227: STD_LOGIC_VECTOR(7 downto 0) := x"E3"; -- � 227
constant simb228: STD_LOGIC_VECTOR(7 downto 0) := x"E4"; -- � 228
constant simb229: STD_LOGIC_VECTOR(7 downto 0) := x"E5"; -- � 229
constant simb230: STD_LOGIC_VECTOR(7 downto 0) := x"E6"; -- ? 230
constant simb231: STD_LOGIC_VECTOR(7 downto 0) := x"E7"; -- � 231
constant simb232: STD_LOGIC_VECTOR(7 downto 0) := x"E8"; -- � 232
constant simb233: STD_LOGIC_VECTOR(7 downto 0) := x"E9"; -- � 233
constant simb234: STD_LOGIC_VECTOR(7 downto 0) := x"EA"; -- � 234
constant simb235: STD_LOGIC_VECTOR(7 downto 0) := x"EB"; -- � 235
constant simb236: STD_LOGIC_VECTOR(7 downto 0) := x"EC"; -- � 236
constant simb237: STD_LOGIC_VECTOR(7 downto 0) := x"ED"; -- � 237
constant simb238: STD_LOGIC_VECTOR(7 downto 0) := x"EE"; --  ? 238
constant simb239: STD_LOGIC_VECTOR(7 downto 0) := x"EF"; --  ? 239
constant simb240: STD_LOGIC_VECTOR(7 downto 0) := x"F0"; -- -- 240
constant simb241: STD_LOGIC_VECTOR(7 downto 0) := x"F1"; -- � 241
constant simb242: STD_LOGIC_VECTOR(7 downto 0) := x"F2"; --  ? 242
constant simb243: STD_LOGIC_VECTOR(7 downto 0) := x"F3"; -- 3?4 243
constant simb244: STD_LOGIC_VECTOR(7 downto 0) := x"F4"; -- � 244
constant simb245: STD_LOGIC_VECTOR(7 downto 0) := x"F5"; -- � 245
constant simb246: STD_LOGIC_VECTOR(7 downto 0) := x"F6"; -- � 246
constant simb247: STD_LOGIC_VECTOR(7 downto 0) := x"F7"; --  ? 247
constant simb248: STD_LOGIC_VECTOR(7 downto 0) := x"F8"; -- � 248
constant simb249: STD_LOGIC_VECTOR(7 downto 0) := x"F9"; --  ? 249
constant simb250: STD_LOGIC_VECTOR(7 downto 0) := x"FA"; -- � 250
constant simb251: STD_LOGIC_VECTOR(7 downto 0) := x"FB"; -- 1 251
constant simb252: STD_LOGIC_VECTOR(7 downto 0) := x"FC"; -- 3 252
constant simb253: STD_LOGIC_VECTOR(7 downto 0) := x"FD"; -- 2 253
constant simb254: STD_LOGIC_VECTOR(7 downto 0) := x"FE"; -- ? 254
constant simb255: STD_LOGIC_VECTOR(7 downto 0) := x"FF"; -- 255

--constantes de tiempos
constant T1: STD_LOGIC_VECTOR(23 downto 0) := x"001FFE"; -- espera de 81.9us = 8.l90 
-- @ write > 37us (3700 = E74)

-------------------

begin

-------------------------------------------------------------------
--Contador de Retardos CONT1--

process(CLOCK,RESET)

begin
    if RESET = '1' then
        CONT1 <= (others => '0');
    elsif (CLOCK'event and CLOCK='1') then
        CONT1 <= CONT1 + 1;
    end if;
end process;

-------------------------------------------------------------------
--Contador para Secuencias CONT2--

process(CLOCK,READY)

begin

if (CLOCK = '1' and CLOCK'event) then
    if READY = '1' then 
        CONT2 <= CONT2 + 1;
    else CONT2 <= "000000";
    end if;
end if;

end process;

-------------------------------------------------------------------
--Actualizaci�n de estados--

process (CLOCK, Next_State)

begin

    if (CLOCK = '1' and CLOCK'event) then 
        State <= Next_State;
    end if;

end process;

-------------------------------------------------------------------
--FSM--

process(CONT1,CONT2,State,CLOCK,REINI)
    begin
    if REINI = '1' THEN 
        Next_State <= RST;
    elsif (CLOCK = '0' and CLOCK'event) then
        case State is
            when RST => -- Estado de reset
            
                if CONT1 = X"0000000"then --0s
                    LCD_RS <= '0';
                    LCD_RW <= '0';
                    LCD_E <= '0';
                    DATA <= X"00";
                    Next_State <= ST0; -- 

                else

                    Next_State <= ST0;

                end if;

----------------- Estado 0 -----------------

            when ST0 => --Primer estado de espera por 25ms

                        --(20 ms = x 1E 8480 = 2.000.000)(15 ms = x 16 E360 = 1.500.000)

                if CONT1 = X"02625A0" then -- 2.500.000 = 25ms
                    READY <= '1';
                    DATA <= X"38"; -- FUNCTION SET 8BITS, 2 LINE, 5X7
                    Next_State <= ST0;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';

                elsif CONT2 = "0011011" then
                    
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= ST1;

                else

                    Next_State <= ST0;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

----------------- Estado 1 -----------------

            when ST1 => --Segundo estado de espera por 100us (10.000 = x 2710)
                
                if CONT1 = X"0006BD0" then -- 27.600 = 276us

                    READY <= '1';
                    DATA <= uno; -- FUNCTION SET
                    Next_State <= ST1;

               elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= FSET;

                else

                    Next_State <= ST1;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

----------------- Estado FSET -----------------

            when FSET => --FUNCTION SET 0x38 (8bits, 2 lineas, 5x7dots)

                if CONT1 = X"0009C40" then --espera por 40.000 = 40us

                    READY <= '1';
                    DATA <= X"38"; --001DL-N-F-XX
                    Next_State <= FSET;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= EMSET;

                else

                    Next_State <= FSET;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

----------------- Estado EMSET -----------------

            when EMSET => --ENTRY MODE SET 0x06 (1 right-moving cursor and address increment)

                if CONT1 = X"0009C40" then --estado de espera por 40us

                    READY <= '1';
                    DATA <= X"06"; --000001-I/D-SH
                    Next_State <= EMSET;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= DO;

                else

                    Next_State <= EMSET;

                end if;

            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

----------------- Estado D0 -----------------

            when DO => --DISPLAY ON/OFF 0x0C (DISPLAY-CURSOR-BLINKING on-off)

                if CONT1 = X"0009C40" then -- estado de espera por 40us

                    READY <= '1';
                    DATA <= X"0C"; --00001-D-C-B,display on, cursor on
                    Next_State <= DO;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= CLD;

                else

                    Next_State <= DO;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

----------------- Estado CLD -----------------

            when CLD => --CLEAR DISPLAY 0x01
            
                if CONT1 = X"0009C40" then -- estado de espera por 40us

                    READY <= '1';
                    DATA <= X"01"; --00000001
                    Next_State <= CLD;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= RETH;

                else

                    Next_State <= CLD;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

------ return cursor home
----------------- Estado RETH -----------------

            when RETH => --RETURN CURSOR HOME
                if CONT1 = X"0009C40" then -- estado de espera por 40us

                    READY <= '1';
                    DATA <= X"02"; --0000001X
                    Next_State <= RETH;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= SDDRAMA;

                else

                    Next_State <= RETH;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

------ ------
----------------- Estado SDDRAMA -----------------

            when SDDRAMA => --SET DD RAM ADDRESS posici�n del display del rengl�n 1 columna 4

                if CONT1 = X"00280A0" then -- estado de espera por 1.64ms = 164.000

                    READY <= '1';
                    DATA <= X"80"; --1-AC6-AC0, 80(R=1,C=1) 84(R=1,C=5)
                    Next_State <= SDDRAMA;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE1;

                else

                    Next_State <= SDDRAMA;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

---------------------------------------------------------------------------------------
--DATOS--DATOS--DATOS--DATOS--DATOS--DATOS--DATOS--DATOS--DATOS--DATOS--DATOS--
---------------------------------------------------------------------------------------
            when WRITE1 => --Write Data in DD RAM (S 53)
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <=  '1';
                    LCD_RS <= '1';
                    
                    if SW = '1' then
                        DATA <= M_A; 
                    ELSE
                        DATA <= M_D;
                    END IF;
                        
                    Next_State <= WRITE1;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';

                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE2;

                else

                    Next_State <= WRITE1;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
                    
            when WRITE2 => --Write Data in DD RAM (i 69, � A1)
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= fila1_2; --DATA<=x"69";
                    Next_State <= WRITE2;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State  <=  WRITE3;

                else

                    Next_State <= WRITE2;

                end if;

            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE3 => --Write Data in DD RAM (m 6D)
    
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= fila1_3; --DATA<=x"6D"; 
                    Next_State <= WRITE3;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E<='1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE4;

                else

                    Next_State <= WRITE3;

                end if;
            
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE4 => --Write Data in DD RAM (b 62)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= fila1_4; --DATA<=x"62";
                    Next_State <= WRITE4;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE5;
                    
                else

                    Next_State<=WRITE4;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE5 => --Write Data in DD RAM (o 6F)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= fila1_5; --DATA<=x"6F";
                    Next_State <= WRITE5;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';

                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE6;

                else

                    Next_State <= WRITE5;

                end if;
            
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE6 => --Write Data in DD RAM (l 6C)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= fila1_6; --DATA<=x"6C";
                    Next_State <= WRITE6;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE7;

                else

                    Next_State <= WRITE6;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE7 => --Write Data in DD RAM (o 6F)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= fila1_7; --DATA<=x"6F";
                    Next_State <= WRITE7;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';

                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE8;

                else

                    Next_State <= WRITE7;

                end if;
                
                    RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
                    
            when WRITE8 => --Write Data in DD RAM (s 73)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= fila1_8; --DATA<=x"73";
                    Next_State <= WRITE8;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
    
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE9;

                else

                    Next_State <= WRITE8;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE9 => --Write Data in DD RAM (space 20)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= fila1_9; --DATA<=x"20";
                    Next_State <= WRITE9;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE10;

                else

                Next_State<=WRITE9;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE10 => --Write Data in DD RAM (A 41)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= fila1_10; --DATA<=X"41";
                    Next_State <= WRITE10;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E<='1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE11;

                else

                    Next_State <=   WRITE10;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE11 => --Write Data in DD RAM (S 53)
                
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= fila1_11; --DATA<=X"53";
                    Next_State <= WRITE11;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
            
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE12;

                else

                    Next_State <= WRITE11;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE12 => --Write Data in DD RAM (C 43)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= fila1_12; --DATA<=X"43";
                    Next_State <= WRITE12;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE13;

                else

                    Next_State <= WRITE12;

                end if;
                
                RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE13 => --Write Data in DD RAM (I 49)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= fila1_13; --DATA<=X"49";
                    Next_State <= WRITE13;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                
                elsif CONT2 = "0011011" then
                    
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE14;

                else

                    Next_State <= WRITE13;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE14 => --Write Data in DD RAM (I 49)
                
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= fila1_14; --DATA<=X"49";
                    Next_State <= WRITE14;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= SDDRAMA2;

                else

                    Next_State <= WRITE14;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

------ ------
            when SDDRAMA2 => --SET DD RAM ADDRESS posici�n del display rengl�n 2 columna 1

                if CONT1 = X"014050" then -- estado de espera por 1.64ms

                    READY <= '1';
                    LCD_RS <= '0';
                    DATA <= X"C0"; --1-AC6 - AC0, C0(R=2,C=1)
                    Next_State <= SDDRAMA2;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE15; --para brincar al segundo rengl�n

                else

                    Next_State <= SDDRAMA2;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

------ ------
            when WRITE15 => --Write Data in DD RAM (corchetA [ 5B)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space; --DATA<=X"5B";
                    Next_State <= WRITE15;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then    
                    
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE16;

                else

                    Next_State <= WRITE15;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE16 => --Write Data in DD RAM (S 53)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space; --DATA<=X"53";
                    Next_State <= WRITE16;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';

                elsif CONT2 = "0011011" then
                    
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE17;

                else

                    Next_State <= WRITE16;

                end if;
                
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE17 => --Write Data in DD RAM (W 57)
                
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space; --DATA<=X"57";

                    Next_State <= WRITE17;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE18;

                else

                    Next_State <= WRITE17;

                end if;

            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE18 => --Write Data in DD RAM (7 37)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space; --DATA<=X"37";
                    Next_State <= WRITE18;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE19;

                else

                    Next_State <= WRITE18;

                end if;
            
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE19 => --Write Data in DD RAM (- 2D)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space; --DATA<=X"2D";
                    Next_State <= WRITE19;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE20;
    
                else

                    Next_State <= WRITE19;

                end if;

            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE20 => --Write Data in DD RAM (S 53)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space; --DATA<=X"53";
                    Next_State <= WRITE20;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE21;

                else

                    Next_State <= WRITE20;

                end if;

            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE21 => --Write Data in DD RAM (W 57)

                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space; --DATA<=X"57";
                    Next_State <= WRITE21;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE22;

                else

                    Next_State <= WRITE21;

                end if;
            RESET<= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0
            
            when WRITE22 => --Write Data in DD RAM (0 30)
                
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space; --DATA<=X"30";
                    Next_State <= WRITE22;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE23;

                else

                    Next_State <= WRITE22;

                end if;
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

            when WRITE23 => --Write Data in DD RAM (corchetC] 5D)
            
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space; --DATA<=X"5D";
                    Next_State <= WRITE23;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= SDDRAMA3;

                else

                    Next_State <= WRITE23;

                end if;
                
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

------ ------
            when SDDRAMA3 => --SET DD RAM ADDRESS posici�n del display del segundo rengl�n
--columna 16;

                if CONT1 = X"0280A0" then -- estado de espera por 1.64ms

                    READY <= '1';
                    LCD_RS <= '0';
                    DATA <= X"CB"; --1-AC6 - AC0, CB(R=2,C=12)
                    Next_State <= SDDRAMA3;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then

                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= WRITE24;

                else

                    Next_State <= SDDRAMA3;

                end if;
            
            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

------ ------
            when WRITE24 => --Write Data in DD RAM (dataascii)
                
                if CONT1 = T1 then -- estado de espera por 0.335s X"FFFFFF"=750,000

                    READY <= '1';
                    LCD_RS <= '1';
                    DATA <= space; --DATA<=X"";--ascii data
                    Next_State <= WRITE24;

                elsif (CONT2 > "0000001" and CONT2 < "0011010") then--rango de 24*10ns=240ns

                    LCD_E <= '1';
                    
                elsif CONT2 = "0011011" then
                
                    READY <= '0';
                    LCD_E <= '0';
                    Next_State <= SDDRAMA3;

                else

                    Next_State <= WRITE24;

                end if;

            RESET <= CONT2(0)and CONT2(1)and CONT2(2)and CONT2(3); -- CONT1 = 0

        end case;
    end if;
end process; --FIN DEL PROCESO DE LA M�QUINA DE ESTADOS



-------------------------------- Mux --------------------------------------------

fila1_1 <= M_A WHEN SW = '1' ELSE --
           M_D WHEN SW = '0' ELSE --
           space;

fila1_2 <= M_C WHEN SW = '1' ELSE --
           M_E WHEN SW = '0' ELSE --
           space;
           
fila1_3 <= M_T WHEN SW = '1' ELSE --
           M_S WHEN SW = '0' ELSE --
           space;
 
fila1_4 <= M_I WHEN SW = '1' ELSE --
           M_A WHEN SW = '0' ELSE --
           space;
   
fila1_5 <= M_V WHEN SW = '1' ELSE --
           M_C WHEN SW = '0' ELSE --
           space;                      

fila1_6 <= M_A WHEN SW = '1' ELSE --
           M_T WHEN SW = '0' ELSE --
           space;
           
fila1_7 <= M_D WHEN SW = '1' ELSE --
           M_I WHEN SW = '0' ELSE --
           space;
           
fila1_8 <= M_O WHEN SW = '1' ELSE --
           M_V WHEN SW = '0' ELSE --
           space;
           
fila1_9 <= space WHEN SW = '1' ELSE --
           M_A WHEN SW = '0' ELSE --
           space;           
           
fila1_10<= dosptos WHEN SW = '1' ELSE --
           M_D WHEN SW = '0' ELSE --
           space;
                      
fila1_11<= parentC WHEN SW = '1' ELSE --
           M_O WHEN SW = '0' ELSE --
           space;           
           
fila1_12<= space WHEN SW = '1' ELSE --
           space WHEN SW = '0' ELSE --
           space;           
           
fila1_13<= space WHEN SW = '1' ELSE --
           dosptos WHEN SW = '0' ELSE --
           space;           
           
fila1_14<= space WHEN SW = '1' ELSE --
           parentA WHEN SW = '0' ELSE --
           space;   
end;














