library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity fsm_complete is
    Port(
        -- LCD        
        CLK         : IN  STD_LOGIC;
        RS          : OUT STD_LOGIC;							
	    RW		    : OUT STD_LOGIC;							
	    ENA 	    : OUT STD_LOGIC;						
	    DATA_LCD    : OUT STD_LOGIC_VECTOR(7 DOWNTO 0); 
	    SEG         : IN INTEGER RANGE 0 TO 15;
        
        -- TECLADO
        col         : in  std_logic_vector(3 downto 0);
        filas       : out std_logic_vector(3 downto 0); 
        Segmentos   : out std_logic_vector(6 downto 0);
        common      : out std_logic_vector(7 downto 0);
         
        -- MATRIZ SPI 
        rst           : in  std_logic;
        start         : in  std_logic;
        sck, cs, mosi : out std_logic;
        
        -- Servomotor
        led, led_rst, pulse: out std_logic;
        
        -- Buzzer
        sonido         : out std_logic
        
    );
end fsm_complete;

architecture Behavioral of fsm_complete is

SIGNAL NUM_ESTADO: INTEGER RANGE 0 TO 15 := 0;
SIGNAL STD_STATE: std_logic;
SIGNAL STD_STATEC: std_logic_vector(1 downto 0);
SIGNAL SERVO: std_logic_vector(1 downto 0);

begin

-- LCD
pantalla:entity work.LIB_LCD_INTESC_REVD
port map(
    CLK       => CLK,        
    RS        => RS,     							
	RW        => RW,		  							
	ENA       => ENA, 	  						     	
	DATA_LCD  => DATA_LCD, 
	SEG       => NUM_ESTADO
);

-- TECLADO
teclado:entity work.Teclado
port map(
    Reloj      => CLK,
    col        => col,
    filas      => filas, 
    Segmentos  => Segmentos,
    common     => common,
    tecla_num  => NUM_ESTADO,
    STD_ESTADO => STD_STATE,
    MOTOR      => SERVO
);

-- Matriz
matriz:entity work.top_spi
port map(
    clk     => CLK,
    rst     => rst,
    start   => start,
    mem_sel => STD_STATEC,
    sck     => sck,
    cs      => cs,
    mosi    => mosi
);

-- Servomotor
servomotor:entity work.ex_div
port map(
    clk      => CLK,
    rst      => rst,
    pulsador => SERVO,
    led      => led,
    led_rst  => led_rst,
    pulse    => pulse
);

-- Buzzer
buzzer:entity work.fsm
port map(
    clk      => CLK,
    rst      => rst,
    start    => start,
    sonido   => sonido
);

STD_STATEC <= STD_STATE & '1';

end Behavioral;
